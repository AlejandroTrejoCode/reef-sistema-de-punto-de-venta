﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Reef___Punto_de_Venta
{
    class MyConnection
    {
        public MySqlConnection conexion;
        public void Crear_conexion()
        {
            string conec = "Server = 127.0.0.1;uid=root;database=point-of-sell;pwd=admin;Port=3307;";
            conexion = new MySqlConnection(conec);
            conexion.Open();
        }
        public void Cerrar_conexion()
        {
            conexion.Close();
        }
        public MySqlConnection getConexion()
        {
            return conexion;
        }
    }
}
