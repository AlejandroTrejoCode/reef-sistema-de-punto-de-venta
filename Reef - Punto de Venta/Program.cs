﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reef___Punto_de_Venta
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            /*
            try
            {
                MyConnection m = new MyConnection();
                m.Crear_conexion();
                MessageBox.Show("Conexion establecida");
            }
            catch (Exception)
            {
                MessageBox.Show("Error no se puede conectar a la base de datos");
            }
            */
            Application.Run(new Splash());
        }
    }
}
