﻿namespace Reef___Punto_de_Venta
{
    partial class controlProductos
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextBoxDescripcion = new System.Windows.Forms.TextBox();
            this.LabelDescripcion = new System.Windows.Forms.Label();
            this.TextBoxClave = new System.Windows.Forms.TextBox();
            this.LabelClave = new System.Windows.Forms.Label();
            this.Cancelar = new System.Windows.Forms.Button();
            this.Editar = new System.Windows.Forms.Button();
            this.Buscar = new System.Windows.Forms.Button();
            this.ContedorControles = new System.Windows.Forms.Panel();
            this.Remover = new System.Windows.Forms.Button();
            this.ContenedorControles = new System.Windows.Forms.Panel();
            this.ContenedorDescripcion = new System.Windows.Forms.PictureBox();
            this.ContenedorClave = new System.Windows.Forms.PictureBox();
            this.ImagenProducto = new System.Windows.Forms.PictureBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.LabelResumen = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.HeaderVender = new System.Windows.Forms.Panel();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ContedorControles.SuspendLayout();
            this.ContenedorControles.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorDescripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorClave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImagenProducto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.HeaderVender.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // TextBoxDescripcion
            // 
            this.TextBoxDescripcion.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.TextBoxDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextBoxDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxDescripcion.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.TextBoxDescripcion.HideSelection = false;
            this.TextBoxDescripcion.Location = new System.Drawing.Point(136, 101);
            this.TextBoxDescripcion.MaxLength = 100;
            this.TextBoxDescripcion.Name = "TextBoxDescripcion";
            this.TextBoxDescripcion.Size = new System.Drawing.Size(326, 19);
            this.TextBoxDescripcion.TabIndex = 12;
            this.TextBoxDescripcion.TabStop = false;
            this.TextBoxDescripcion.WordWrap = false;
            // 
            // LabelDescripcion
            // 
            this.LabelDescripcion.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelDescripcion.ForeColor = System.Drawing.Color.DimGray;
            this.LabelDescripcion.Location = new System.Drawing.Point(133, 67);
            this.LabelDescripcion.Name = "LabelDescripcion";
            this.LabelDescripcion.Size = new System.Drawing.Size(129, 25);
            this.LabelDescripcion.TabIndex = 10;
            this.LabelDescripcion.Text = "Descripción:";
            this.LabelDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TextBoxClave
            // 
            this.TextBoxClave.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.TextBoxClave.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextBoxClave.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxClave.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.TextBoxClave.HideSelection = false;
            this.TextBoxClave.Location = new System.Drawing.Point(136, 36);
            this.TextBoxClave.MaxLength = 100;
            this.TextBoxClave.Name = "TextBoxClave";
            this.TextBoxClave.Size = new System.Drawing.Size(215, 22);
            this.TextBoxClave.TabIndex = 9;
            this.TextBoxClave.TabStop = false;
            this.TextBoxClave.WordWrap = false;
            // 
            // LabelClave
            // 
            this.LabelClave.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelClave.ForeColor = System.Drawing.Color.DimGray;
            this.LabelClave.Location = new System.Drawing.Point(133, 4);
            this.LabelClave.Name = "LabelClave";
            this.LabelClave.Size = new System.Drawing.Size(98, 25);
            this.LabelClave.TabIndex = 4;
            this.LabelClave.Text = "Clave: ";
            this.LabelClave.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Cancelar
            // 
            this.Cancelar.BackColor = System.Drawing.Color.White;
            this.Cancelar.FlatAppearance.BorderSize = 0;
            this.Cancelar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.Cancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cancelar.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cancelar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(197)))), ((int)(((byte)(65)))));
            this.Cancelar.Location = new System.Drawing.Point(114, 69);
            this.Cancelar.Name = "Cancelar";
            this.Cancelar.Size = new System.Drawing.Size(85, 66);
            this.Cancelar.TabIndex = 10;
            this.Cancelar.Text = "Cancelar";
            this.Cancelar.UseVisualStyleBackColor = false;
            // 
            // Editar
            // 
            this.Editar.BackColor = System.Drawing.Color.White;
            this.Editar.FlatAppearance.BorderSize = 0;
            this.Editar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.Editar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Editar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Editar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(197)))), ((int)(((byte)(65)))));
            this.Editar.Location = new System.Drawing.Point(23, 69);
            this.Editar.Name = "Editar";
            this.Editar.Size = new System.Drawing.Size(85, 66);
            this.Editar.TabIndex = 5;
            this.Editar.Text = "Editar";
            this.Editar.UseVisualStyleBackColor = false;
            // 
            // Buscar
            // 
            this.Buscar.BackColor = System.Drawing.Color.White;
            this.Buscar.FlatAppearance.BorderSize = 0;
            this.Buscar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.Buscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Buscar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Buscar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(197)))), ((int)(((byte)(65)))));
            this.Buscar.Location = new System.Drawing.Point(23, 2);
            this.Buscar.Name = "Buscar";
            this.Buscar.Size = new System.Drawing.Size(85, 66);
            this.Buscar.TabIndex = 4;
            this.Buscar.Text = "Buscar";
            this.Buscar.UseVisualStyleBackColor = false;
            // 
            // ContedorControles
            // 
            this.ContedorControles.Controls.Add(this.Cancelar);
            this.ContedorControles.Controls.Add(this.Remover);
            this.ContedorControles.Controls.Add(this.Editar);
            this.ContedorControles.Controls.Add(this.Buscar);
            this.ContedorControles.Location = new System.Drawing.Point(468, 3);
            this.ContedorControles.Name = "ContedorControles";
            this.ContedorControles.Size = new System.Drawing.Size(226, 135);
            this.ContedorControles.TabIndex = 13;
            // 
            // Remover
            // 
            this.Remover.BackColor = System.Drawing.Color.White;
            this.Remover.FlatAppearance.BorderSize = 0;
            this.Remover.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.Remover.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Remover.Font = new System.Drawing.Font("Century Gothic", 10.5F, System.Drawing.FontStyle.Bold);
            this.Remover.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(197)))), ((int)(((byte)(65)))));
            this.Remover.Location = new System.Drawing.Point(114, 2);
            this.Remover.Name = "Remover";
            this.Remover.Size = new System.Drawing.Size(85, 66);
            this.Remover.TabIndex = 6;
            this.Remover.Text = "Remover";
            this.Remover.UseVisualStyleBackColor = false;
            // 
            // ContenedorControles
            // 
            this.ContenedorControles.BackColor = System.Drawing.SystemColors.Control;
            this.ContenedorControles.Controls.Add(this.ContedorControles);
            this.ContenedorControles.Controls.Add(this.TextBoxDescripcion);
            this.ContenedorControles.Controls.Add(this.ContenedorDescripcion);
            this.ContenedorControles.Controls.Add(this.LabelDescripcion);
            this.ContenedorControles.Controls.Add(this.TextBoxClave);
            this.ContenedorControles.Controls.Add(this.ContenedorClave);
            this.ContenedorControles.Controls.Add(this.LabelClave);
            this.ContenedorControles.Controls.Add(this.ImagenProducto);
            this.ContenedorControles.Location = new System.Drawing.Point(0, 56);
            this.ContenedorControles.Name = "ContenedorControles";
            this.ContenedorControles.Size = new System.Drawing.Size(1210, 140);
            this.ContenedorControles.TabIndex = 52;
            // 
            // ContenedorDescripcion
            // 
            this.ContenedorDescripcion.Image = global::Reef___Punto_de_Venta.Properties.Resources.BuscarContenedor;
            this.ContenedorDescripcion.Location = new System.Drawing.Point(133, 95);
            this.ContenedorDescripcion.Name = "ContenedorDescripcion";
            this.ContenedorDescripcion.Size = new System.Drawing.Size(329, 32);
            this.ContenedorDescripcion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ContenedorDescripcion.TabIndex = 11;
            this.ContenedorDescripcion.TabStop = false;
            // 
            // ContenedorClave
            // 
            this.ContenedorClave.Image = global::Reef___Punto_de_Venta.Properties.Resources.BuscarContenedor;
            this.ContenedorClave.Location = new System.Drawing.Point(133, 32);
            this.ContenedorClave.Name = "ContenedorClave";
            this.ContenedorClave.Size = new System.Drawing.Size(218, 32);
            this.ContenedorClave.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ContenedorClave.TabIndex = 8;
            this.ContenedorClave.TabStop = false;
            // 
            // ImagenProducto
            // 
            this.ImagenProducto.Location = new System.Drawing.Point(3, 4);
            this.ImagenProducto.Name = "ImagenProducto";
            this.ImagenProducto.Size = new System.Drawing.Size(120, 134);
            this.ImagenProducto.TabIndex = 0;
            this.ImagenProducto.TabStop = false;
            // 
            // textBox6
            // 
            this.textBox6.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox6.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.textBox6.HideSelection = false;
            this.textBox6.Location = new System.Drawing.Point(1008, 559);
            this.textBox6.MaxLength = 100;
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(104, 22);
            this.textBox6.TabIndex = 51;
            this.textBox6.TabStop = false;
            this.textBox6.WordWrap = false;
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.textBox5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox5.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.textBox5.HideSelection = false;
            this.textBox5.Location = new System.Drawing.Point(883, 559);
            this.textBox5.MaxLength = 100;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(104, 22);
            this.textBox5.TabIndex = 50;
            this.textBox5.TabStop = false;
            this.textBox5.WordWrap = false;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.DimGray;
            this.label7.Location = new System.Drawing.Point(997, 494);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 46);
            this.label7.TabIndex = 47;
            this.label7.Text = "Precio\r\nVenta";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LabelResumen
            // 
            this.LabelResumen.AutoSize = true;
            this.LabelResumen.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelResumen.ForeColor = System.Drawing.Color.DimGray;
            this.LabelResumen.Location = new System.Drawing.Point(12, 7);
            this.LabelResumen.Name = "LabelResumen";
            this.LabelResumen.Size = new System.Drawing.Size(311, 39);
            this.LabelResumen.TabIndex = 1;
            this.LabelResumen.Text = "Inicio > Productos";
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.White;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(197)))), ((int)(((byte)(65)))));
            this.button7.Location = new System.Drawing.Point(883, 607);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(229, 66);
            this.button7.TabIndex = 33;
            this.button7.Text = "Agregar Producto";
            this.button7.UseVisualStyleBackColor = false;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.DimGray;
            this.label6.Location = new System.Drawing.Point(876, 494);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 46);
            this.label6.TabIndex = 46;
            this.label6.Text = "Precio\r\nCompra";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox4.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.textBox4.HideSelection = false;
            this.textBox4.Location = new System.Drawing.Point(883, 463);
            this.textBox4.MaxLength = 100;
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(215, 22);
            this.textBox4.TabIndex = 45;
            this.textBox4.TabStop = false;
            this.textBox4.WordWrap = false;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.DimGray;
            this.label5.Location = new System.Drawing.Point(876, 435);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(129, 25);
            this.label5.TabIndex = 43;
            this.label5.Text = "Stock:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.DimGray;
            this.label4.Location = new System.Drawing.Point(879, 293);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 25);
            this.label4.TabIndex = 42;
            this.label4.Text = "Nombre:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.textBox3.HideSelection = false;
            this.textBox3.Location = new System.Drawing.Point(883, 325);
            this.textBox3.MaxLength = 100;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(215, 22);
            this.textBox3.TabIndex = 41;
            this.textBox3.TabStop = false;
            this.textBox3.WordWrap = false;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.DimGray;
            this.label3.Location = new System.Drawing.Point(879, 356);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 25);
            this.label3.TabIndex = 36;
            this.label3.Text = "Descripción:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.textBox2.HideSelection = false;
            this.textBox2.Location = new System.Drawing.Point(881, 394);
            this.textBox2.MaxLength = 100;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(294, 19);
            this.textBox2.TabIndex = 38;
            this.textBox2.TabStop = false;
            this.textBox2.WordWrap = false;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DimGray;
            this.label2.Location = new System.Drawing.Point(879, 220);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 25);
            this.label2.TabIndex = 39;
            this.label2.Text = "Clave: ";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.textBox1.HideSelection = false;
            this.textBox1.Location = new System.Drawing.Point(883, 252);
            this.textBox1.MaxLength = 100;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(215, 22);
            this.textBox1.TabIndex = 37;
            this.textBox1.TabStop = false;
            this.textBox1.WordWrap = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(3, 199);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(867, 508);
            this.dataGridView1.TabIndex = 32;
            // 
            // HeaderVender
            // 
            this.HeaderVender.BackColor = System.Drawing.Color.White;
            this.HeaderVender.Controls.Add(this.LabelResumen);
            this.HeaderVender.Location = new System.Drawing.Point(0, 6);
            this.HeaderVender.Name = "HeaderVender";
            this.HeaderVender.Size = new System.Drawing.Size(1210, 50);
            this.HeaderVender.TabIndex = 31;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::Reef___Punto_de_Venta.Properties.Resources.BuscarContenedor;
            this.pictureBox6.Location = new System.Drawing.Point(1001, 555);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(117, 32);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 49;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::Reef___Punto_de_Venta.Properties.Resources.BuscarContenedor;
            this.pictureBox5.Location = new System.Drawing.Point(878, 555);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(115, 32);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 48;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Reef___Punto_de_Venta.Properties.Resources.BuscarContenedor;
            this.pictureBox4.Location = new System.Drawing.Point(880, 459);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(218, 32);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 44;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Reef___Punto_de_Venta.Properties.Resources.BuscarContenedor;
            this.pictureBox3.Location = new System.Drawing.Point(880, 321);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(218, 32);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 40;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Reef___Punto_de_Venta.Properties.Resources.BuscarContenedor;
            this.pictureBox2.Location = new System.Drawing.Point(878, 388);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(305, 32);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 35;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Reef___Punto_de_Venta.Properties.Resources.BuscarContenedor;
            this.pictureBox1.Location = new System.Drawing.Point(880, 248);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(218, 32);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 34;
            this.pictureBox1.TabStop = false;
            // 
            // controlProductos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Controls.Add(this.ContenedorControles);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.HeaderVender);
            this.Name = "controlProductos";
            this.Size = new System.Drawing.Size(1210, 710);
            this.ContedorControles.ResumeLayout(false);
            this.ContenedorControles.ResumeLayout(false);
            this.ContenedorControles.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorDescripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorClave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImagenProducto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.HeaderVender.ResumeLayout(false);
            this.HeaderVender.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox TextBoxDescripcion;
        private System.Windows.Forms.PictureBox ContenedorDescripcion;
        private System.Windows.Forms.Label LabelDescripcion;
        private System.Windows.Forms.TextBox TextBoxClave;
        private System.Windows.Forms.PictureBox ContenedorClave;
        private System.Windows.Forms.Label LabelClave;
        private System.Windows.Forms.PictureBox ImagenProducto;
        private System.Windows.Forms.Button Cancelar;
        private System.Windows.Forms.Button Editar;
        private System.Windows.Forms.Button Buscar;
        private System.Windows.Forms.Panel ContedorControles;
        private System.Windows.Forms.Button Remover;
        private System.Windows.Forms.Panel ContenedorControles;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label LabelResumen;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel HeaderVender;
    }
}
