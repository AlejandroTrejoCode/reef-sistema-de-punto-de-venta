﻿namespace Reef___Punto_de_Venta
{
    partial class controlVender
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextBoxClave = new System.Windows.Forms.TextBox();
            this.BotonModificar = new System.Windows.Forms.Button();
            this.LabelPorcentajeDesc = new System.Windows.Forms.Label();
            this.ImagenSeleccionada = new System.Windows.Forms.PictureBox();
            this.LabelDescuento = new System.Windows.Forms.Label();
            this.TextBoxDescuento = new System.Windows.Forms.TextBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.LabelPrecio = new System.Windows.Forms.Label();
            this.TextBoxPrecio = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.LabelProductos = new System.Windows.Forms.Label();
            this.ImagenProducto = new System.Windows.Forms.PictureBox();
            this.PanelBusqueda = new System.Windows.Forms.Panel();
            this.Limpiar = new System.Windows.Forms.Button();
            this.Aceptar = new System.Windows.Forms.Button();
            this.BusquedaCategoria = new System.Windows.Forms.TextBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.BusquedaDescripcion = new System.Windows.Forms.TextBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.BusquedaClave = new System.Windows.Forms.TextBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.CerrarBusqueda = new System.Windows.Forms.Button();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.BotonModificarCanelar = new System.Windows.Forms.Button();
            this.LabelProductoEditar = new System.Windows.Forms.Label();
            this.LabelCantidad = new System.Windows.Forms.Label();
            this.TextBoxCantidad = new System.Windows.Forms.TextBox();
            this.ContenedorClave = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.PanelEditar = new System.Windows.Forms.Panel();
            this.LabelIVA = new System.Windows.Forms.Label();
            this.LabelUSD = new System.Windows.Forms.Label();
            this.LabelTotalPrecio = new System.Windows.Forms.Label();
            this.LabelTotal = new System.Windows.Forms.Label();
            this.BotonVender = new System.Windows.Forms.Button();
            this.LabelIVAPrecio = new System.Windows.Forms.Label();
            this.ContedorControles = new System.Windows.Forms.Panel();
            this.LabelUSDPrecio = new System.Windows.Forms.Label();
            this.Cancelar = new System.Windows.Forms.Button();
            this.Recarga = new System.Windows.Forms.Button();
            this.Vascula = new System.Windows.Forms.Button();
            this.Descuento = new System.Windows.Forms.Button();
            this.Remover = new System.Windows.Forms.Button();
            this.Editar = new System.Windows.Forms.Button();
            this.Buscar = new System.Windows.Forms.Button();
            this.ContenedorControles = new System.Windows.Forms.Panel();
            this.TextBoxDescripcion = new System.Windows.Forms.TextBox();
            this.ContenedorDescripcion = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Minimizar = new System.Windows.Forms.Button();
            this.LabelResumen = new System.Windows.Forms.Label();
            this.HeaderVender = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.ImagenSeleccionada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImagenProducto)).BeginInit();
            this.PanelBusqueda.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorClave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.PanelEditar.SuspendLayout();
            this.ContedorControles.SuspendLayout();
            this.ContenedorControles.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorDescripcion)).BeginInit();
            this.HeaderVender.SuspendLayout();
            this.SuspendLayout();
            // 
            // TextBoxClave
            // 
            this.TextBoxClave.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.TextBoxClave.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextBoxClave.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxClave.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.TextBoxClave.HideSelection = false;
            this.TextBoxClave.Location = new System.Drawing.Point(136, 35);
            this.TextBoxClave.MaxLength = 100;
            this.TextBoxClave.Name = "TextBoxClave";
            this.TextBoxClave.Size = new System.Drawing.Size(210, 24);
            this.TextBoxClave.TabIndex = 1;
            this.TextBoxClave.WordWrap = false;
            // 
            // BotonModificar
            // 
            this.BotonModificar.BackColor = System.Drawing.Color.Transparent;
            this.BotonModificar.FlatAppearance.BorderSize = 0;
            this.BotonModificar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.BotonModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BotonModificar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotonModificar.ForeColor = System.Drawing.Color.DimGray;
            this.BotonModificar.Image = global::Reef___Punto_de_Venta.Properties.Resources.ProductosEditar1;
            this.BotonModificar.Location = new System.Drawing.Point(128, 223);
            this.BotonModificar.Name = "BotonModificar";
            this.BotonModificar.Size = new System.Drawing.Size(90, 66);
            this.BotonModificar.TabIndex = 17;
            this.BotonModificar.TabStop = false;
            this.BotonModificar.Text = "Modificar";
            this.BotonModificar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.BotonModificar.UseVisualStyleBackColor = false;
            // 
            // LabelPorcentajeDesc
            // 
            this.LabelPorcentajeDesc.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelPorcentajeDesc.ForeColor = System.Drawing.Color.DimGray;
            this.LabelPorcentajeDesc.Location = new System.Drawing.Point(357, 185);
            this.LabelPorcentajeDesc.Name = "LabelPorcentajeDesc";
            this.LabelPorcentajeDesc.Size = new System.Drawing.Size(64, 25);
            this.LabelPorcentajeDesc.TabIndex = 22;
            this.LabelPorcentajeDesc.Text = "100%";
            this.LabelPorcentajeDesc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ImagenSeleccionada
            // 
            this.ImagenSeleccionada.Location = new System.Drawing.Point(354, 67);
            this.ImagenSeleccionada.Name = "ImagenSeleccionada";
            this.ImagenSeleccionada.Size = new System.Drawing.Size(130, 115);
            this.ImagenSeleccionada.TabIndex = 14;
            this.ImagenSeleccionada.TabStop = false;
            // 
            // LabelDescuento
            // 
            this.LabelDescuento.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelDescuento.ForeColor = System.Drawing.Color.DimGray;
            this.LabelDescuento.Location = new System.Drawing.Point(6, 183);
            this.LabelDescuento.Name = "LabelDescuento";
            this.LabelDescuento.Size = new System.Drawing.Size(116, 25);
            this.LabelDescuento.TabIndex = 21;
            this.LabelDescuento.Text = "Descuento:";
            this.LabelDescuento.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TextBoxDescuento
            // 
            this.TextBoxDescuento.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.TextBoxDescuento.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextBoxDescuento.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxDescuento.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.TextBoxDescuento.HideSelection = false;
            this.TextBoxDescuento.Location = new System.Drawing.Point(130, 184);
            this.TextBoxDescuento.MaxLength = 100;
            this.TextBoxDescuento.Name = "TextBoxDescuento";
            this.TextBoxDescuento.Size = new System.Drawing.Size(198, 24);
            this.TextBoxDescuento.TabIndex = 5;
            this.TextBoxDescuento.WordWrap = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Reef___Punto_de_Venta.Properties.Resources.BuscarContenedor;
            this.pictureBox3.Location = new System.Drawing.Point(128, 180);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(200, 32);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 19;
            this.pictureBox3.TabStop = false;
            // 
            // LabelPrecio
            // 
            this.LabelPrecio.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelPrecio.ForeColor = System.Drawing.Color.DimGray;
            this.LabelPrecio.Location = new System.Drawing.Point(6, 127);
            this.LabelPrecio.Name = "LabelPrecio";
            this.LabelPrecio.Size = new System.Drawing.Size(107, 25);
            this.LabelPrecio.TabIndex = 18;
            this.LabelPrecio.Text = "Precio:";
            this.LabelPrecio.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TextBoxPrecio
            // 
            this.TextBoxPrecio.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.TextBoxPrecio.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextBoxPrecio.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxPrecio.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.TextBoxPrecio.HideSelection = false;
            this.TextBoxPrecio.Location = new System.Drawing.Point(130, 128);
            this.TextBoxPrecio.MaxLength = 100;
            this.TextBoxPrecio.Name = "TextBoxPrecio";
            this.TextBoxPrecio.Size = new System.Drawing.Size(198, 24);
            this.TextBoxPrecio.TabIndex = 4;
            this.TextBoxPrecio.WordWrap = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Reef___Punto_de_Venta.Properties.Resources.BuscarContenedor;
            this.pictureBox1.Location = new System.Drawing.Point(128, 124);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(200, 32);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            // 
            // LabelProductos
            // 
            this.LabelProductos.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelProductos.ForeColor = System.Drawing.Color.DimGray;
            this.LabelProductos.Location = new System.Drawing.Point(133, 3);
            this.LabelProductos.Name = "LabelProductos";
            this.LabelProductos.Size = new System.Drawing.Size(98, 25);
            this.LabelProductos.TabIndex = 4;
            this.LabelProductos.Text = "Clave: ";
            this.LabelProductos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ImagenProducto
            // 
            this.ImagenProducto.Image = global::Reef___Punto_de_Venta.Properties.Resources.ImageNull;
            this.ImagenProducto.Location = new System.Drawing.Point(1, 3);
            this.ImagenProducto.Name = "ImagenProducto";
            this.ImagenProducto.Size = new System.Drawing.Size(130, 134);
            this.ImagenProducto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ImagenProducto.TabIndex = 0;
            this.ImagenProducto.TabStop = false;
            // 
            // PanelBusqueda
            // 
            this.PanelBusqueda.BackColor = System.Drawing.SystemColors.Control;
            this.PanelBusqueda.Controls.Add(this.Limpiar);
            this.PanelBusqueda.Controls.Add(this.Aceptar);
            this.PanelBusqueda.Controls.Add(this.BusquedaCategoria);
            this.PanelBusqueda.Controls.Add(this.pictureBox6);
            this.PanelBusqueda.Controls.Add(this.label5);
            this.PanelBusqueda.Controls.Add(this.BusquedaDescripcion);
            this.PanelBusqueda.Controls.Add(this.pictureBox4);
            this.PanelBusqueda.Controls.Add(this.label3);
            this.PanelBusqueda.Controls.Add(this.BusquedaClave);
            this.PanelBusqueda.Controls.Add(this.pictureBox5);
            this.PanelBusqueda.Controls.Add(this.label4);
            this.PanelBusqueda.Controls.Add(this.CerrarBusqueda);
            this.PanelBusqueda.Controls.Add(this.dataGridView2);
            this.PanelBusqueda.Controls.Add(this.label2);
            this.PanelBusqueda.Location = new System.Drawing.Point(3, 200);
            this.PanelBusqueda.Name = "PanelBusqueda";
            this.PanelBusqueda.Size = new System.Drawing.Size(1204, 500);
            this.PanelBusqueda.TabIndex = 10;
            // 
            // Limpiar
            // 
            this.Limpiar.BackColor = System.Drawing.Color.White;
            this.Limpiar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.Limpiar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Limpiar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Limpiar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(200)))), ((int)(((byte)(163)))));
            this.Limpiar.Location = new System.Drawing.Point(1076, 302);
            this.Limpiar.Name = "Limpiar";
            this.Limpiar.Size = new System.Drawing.Size(120, 66);
            this.Limpiar.TabIndex = 22;
            this.Limpiar.TabStop = false;
            this.Limpiar.Text = "Limpiar";
            this.Limpiar.UseVisualStyleBackColor = false;
            // 
            // Aceptar
            // 
            this.Aceptar.BackColor = System.Drawing.Color.White;
            this.Aceptar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.Aceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Aceptar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Aceptar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(200)))), ((int)(((byte)(163)))));
            this.Aceptar.Location = new System.Drawing.Point(935, 302);
            this.Aceptar.Name = "Aceptar";
            this.Aceptar.Size = new System.Drawing.Size(120, 66);
            this.Aceptar.TabIndex = 21;
            this.Aceptar.TabStop = false;
            this.Aceptar.Text = "Aceptar";
            this.Aceptar.UseVisualStyleBackColor = false;
            // 
            // BusquedaCategoria
            // 
            this.BusquedaCategoria.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.BusquedaCategoria.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.BusquedaCategoria.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BusquedaCategoria.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.BusquedaCategoria.HideSelection = false;
            this.BusquedaCategoria.Location = new System.Drawing.Point(934, 249);
            this.BusquedaCategoria.MaxLength = 100;
            this.BusquedaCategoria.Name = "BusquedaCategoria";
            this.BusquedaCategoria.Size = new System.Drawing.Size(210, 24);
            this.BusquedaCategoria.TabIndex = 18;
            this.BusquedaCategoria.WordWrap = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::Reef___Punto_de_Venta.Properties.Resources.BuscarContenedor;
            this.pictureBox6.Location = new System.Drawing.Point(931, 245);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(220, 32);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 20;
            this.pictureBox6.TabStop = false;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.DimGray;
            this.label5.Location = new System.Drawing.Point(931, 217);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 25);
            this.label5.TabIndex = 19;
            this.label5.Text = "Categoria:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // BusquedaDescripcion
            // 
            this.BusquedaDescripcion.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.BusquedaDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.BusquedaDescripcion.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BusquedaDescripcion.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.BusquedaDescripcion.HideSelection = false;
            this.BusquedaDescripcion.Location = new System.Drawing.Point(933, 183);
            this.BusquedaDescripcion.MaxLength = 100;
            this.BusquedaDescripcion.Name = "BusquedaDescripcion";
            this.BusquedaDescripcion.Size = new System.Drawing.Size(250, 20);
            this.BusquedaDescripcion.TabIndex = 13;
            this.BusquedaDescripcion.WordWrap = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Reef___Punto_de_Venta.Properties.Resources.BuscarContenedor;
            this.pictureBox4.Location = new System.Drawing.Point(930, 177);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(260, 32);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 17;
            this.pictureBox4.TabStop = false;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.DimGray;
            this.label3.Location = new System.Drawing.Point(930, 149);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 25);
            this.label3.TabIndex = 16;
            this.label3.Text = "Descripción:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // BusquedaClave
            // 
            this.BusquedaClave.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.BusquedaClave.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.BusquedaClave.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BusquedaClave.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.BusquedaClave.HideSelection = false;
            this.BusquedaClave.Location = new System.Drawing.Point(933, 118);
            this.BusquedaClave.MaxLength = 100;
            this.BusquedaClave.Name = "BusquedaClave";
            this.BusquedaClave.Size = new System.Drawing.Size(210, 24);
            this.BusquedaClave.TabIndex = 12;
            this.BusquedaClave.WordWrap = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::Reef___Punto_de_Venta.Properties.Resources.BuscarContenedor;
            this.pictureBox5.Location = new System.Drawing.Point(930, 114);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(220, 32);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 15;
            this.pictureBox5.TabStop = false;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.DimGray;
            this.label4.Location = new System.Drawing.Point(930, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 25);
            this.label4.TabIndex = 14;
            this.label4.Text = "Clave: ";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // CerrarBusqueda
            // 
            this.CerrarBusqueda.BackgroundImage = global::Reef___Punto_de_Venta.Properties.Resources.Cerrar1;
            this.CerrarBusqueda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CerrarBusqueda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CerrarBusqueda.ForeColor = System.Drawing.Color.PowderBlue;
            this.CerrarBusqueda.Location = new System.Drawing.Point(1177, 3);
            this.CerrarBusqueda.Name = "CerrarBusqueda";
            this.CerrarBusqueda.Size = new System.Drawing.Size(24, 26);
            this.CerrarBusqueda.TabIndex = 3;
            this.CerrarBusqueda.TabStop = false;
            this.CerrarBusqueda.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(3, 5);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(921, 492);
            this.dataGridView2.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DimGray;
            this.label2.Location = new System.Drawing.Point(965, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(186, 41);
            this.label2.TabIndex = 4;
            this.label2.Text = "Busqueda";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(1, 203);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1204, 500);
            this.dataGridView1.TabIndex = 9;
            // 
            // BotonModificarCanelar
            // 
            this.BotonModificarCanelar.BackColor = System.Drawing.Color.Transparent;
            this.BotonModificarCanelar.FlatAppearance.BorderSize = 0;
            this.BotonModificarCanelar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.BotonModificarCanelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BotonModificarCanelar.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotonModificarCanelar.ForeColor = System.Drawing.Color.DimGray;
            this.BotonModificarCanelar.Image = global::Reef___Punto_de_Venta.Properties.Resources.ProductosCancelar;
            this.BotonModificarCanelar.Location = new System.Drawing.Point(243, 220);
            this.BotonModificarCanelar.Name = "BotonModificarCanelar";
            this.BotonModificarCanelar.Size = new System.Drawing.Size(87, 66);
            this.BotonModificarCanelar.TabIndex = 17;
            this.BotonModificarCanelar.TabStop = false;
            this.BotonModificarCanelar.Text = "Cancelar";
            this.BotonModificarCanelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.BotonModificarCanelar.UseVisualStyleBackColor = false;
            // 
            // LabelProductoEditar
            // 
            this.LabelProductoEditar.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelProductoEditar.ForeColor = System.Drawing.Color.DimGray;
            this.LabelProductoEditar.Location = new System.Drawing.Point(3, 14);
            this.LabelProductoEditar.Name = "LabelProductoEditar";
            this.LabelProductoEditar.Size = new System.Drawing.Size(498, 38);
            this.LabelProductoEditar.TabIndex = 3;
            this.LabelProductoEditar.Text = "Nombre Producto Selec.";
            this.LabelProductoEditar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LabelCantidad
            // 
            this.LabelCantidad.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelCantidad.ForeColor = System.Drawing.Color.DimGray;
            this.LabelCantidad.Location = new System.Drawing.Point(6, 70);
            this.LabelCantidad.Name = "LabelCantidad";
            this.LabelCantidad.Size = new System.Drawing.Size(107, 25);
            this.LabelCantidad.TabIndex = 15;
            this.LabelCantidad.Text = "Cantidad:";
            this.LabelCantidad.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TextBoxCantidad
            // 
            this.TextBoxCantidad.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.TextBoxCantidad.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextBoxCantidad.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxCantidad.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.TextBoxCantidad.HideSelection = false;
            this.TextBoxCantidad.Location = new System.Drawing.Point(130, 71);
            this.TextBoxCantidad.MaxLength = 100;
            this.TextBoxCantidad.Name = "TextBoxCantidad";
            this.TextBoxCantidad.Size = new System.Drawing.Size(198, 24);
            this.TextBoxCantidad.TabIndex = 3;
            this.TextBoxCantidad.WordWrap = false;
            // 
            // ContenedorClave
            // 
            this.ContenedorClave.Image = global::Reef___Punto_de_Venta.Properties.Resources.BuscarContenedor;
            this.ContenedorClave.Location = new System.Drawing.Point(133, 31);
            this.ContenedorClave.Name = "ContenedorClave";
            this.ContenedorClave.Size = new System.Drawing.Size(220, 32);
            this.ContenedorClave.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ContenedorClave.TabIndex = 8;
            this.ContenedorClave.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Reef___Punto_de_Venta.Properties.Resources.BuscarContenedor;
            this.pictureBox2.Location = new System.Drawing.Point(128, 67);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(200, 32);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 14;
            this.pictureBox2.TabStop = false;
            // 
            // PanelEditar
            // 
            this.PanelEditar.BackColor = System.Drawing.Color.White;
            this.PanelEditar.Controls.Add(this.BotonModificarCanelar);
            this.PanelEditar.Controls.Add(this.BotonModificar);
            this.PanelEditar.Controls.Add(this.LabelPorcentajeDesc);
            this.PanelEditar.Controls.Add(this.ImagenSeleccionada);
            this.PanelEditar.Controls.Add(this.LabelDescuento);
            this.PanelEditar.Controls.Add(this.TextBoxDescuento);
            this.PanelEditar.Controls.Add(this.pictureBox3);
            this.PanelEditar.Controls.Add(this.LabelPrecio);
            this.PanelEditar.Controls.Add(this.TextBoxPrecio);
            this.PanelEditar.Controls.Add(this.pictureBox1);
            this.PanelEditar.Controls.Add(this.LabelProductoEditar);
            this.PanelEditar.Controls.Add(this.LabelCantidad);
            this.PanelEditar.Controls.Add(this.TextBoxCantidad);
            this.PanelEditar.Controls.Add(this.pictureBox2);
            this.PanelEditar.Location = new System.Drawing.Point(372, 286);
            this.PanelEditar.Name = "PanelEditar";
            this.PanelEditar.Size = new System.Drawing.Size(504, 300);
            this.PanelEditar.TabIndex = 11;
            // 
            // LabelIVA
            // 
            this.LabelIVA.AutoSize = true;
            this.LabelIVA.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelIVA.ForeColor = System.Drawing.Color.DimGray;
            this.LabelIVA.Location = new System.Drawing.Point(455, 106);
            this.LabelIVA.Name = "LabelIVA";
            this.LabelIVA.Size = new System.Drawing.Size(50, 24);
            this.LabelIVA.TabIndex = 14;
            this.LabelIVA.Text = "IVA:";
            // 
            // LabelUSD
            // 
            this.LabelUSD.AutoSize = true;
            this.LabelUSD.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelUSD.ForeColor = System.Drawing.Color.DimGray;
            this.LabelUSD.Location = new System.Drawing.Point(455, 71);
            this.LabelUSD.Name = "LabelUSD";
            this.LabelUSD.Size = new System.Drawing.Size(54, 24);
            this.LabelUSD.TabIndex = 13;
            this.LabelUSD.Text = "USD:";
            // 
            // LabelTotalPrecio
            // 
            this.LabelTotalPrecio.AutoSize = true;
            this.LabelTotalPrecio.Font = new System.Drawing.Font("Century Gothic", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelTotalPrecio.ForeColor = System.Drawing.Color.DimGray;
            this.LabelTotalPrecio.Location = new System.Drawing.Point(510, 4);
            this.LabelTotalPrecio.Name = "LabelTotalPrecio";
            this.LabelTotalPrecio.Size = new System.Drawing.Size(145, 56);
            this.LabelTotalPrecio.TabIndex = 12;
            this.LabelTotalPrecio.Text = "$ 450";
            // 
            // LabelTotal
            // 
            this.LabelTotal.AutoSize = true;
            this.LabelTotal.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelTotal.ForeColor = System.Drawing.Color.DimGray;
            this.LabelTotal.Location = new System.Drawing.Point(410, 19);
            this.LabelTotal.Name = "LabelTotal";
            this.LabelTotal.Size = new System.Drawing.Size(98, 38);
            this.LabelTotal.TabIndex = 3;
            this.LabelTotal.Text = "Total:";
            // 
            // BotonVender
            // 
            this.BotonVender.BackColor = System.Drawing.Color.Transparent;
            this.BotonVender.FlatAppearance.BorderSize = 0;
            this.BotonVender.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.BotonVender.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BotonVender.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotonVender.ForeColor = System.Drawing.Color.DimGray;
            this.BotonVender.Image = global::Reef___Punto_de_Venta.Properties.Resources.VenderVender1;
            this.BotonVender.Location = new System.Drawing.Point(705, 4);
            this.BotonVender.Name = "BotonVender";
            this.BotonVender.Size = new System.Drawing.Size(102, 128);
            this.BotonVender.TabIndex = 11;
            this.BotonVender.TabStop = false;
            this.BotonVender.Text = "Vender (F5)";
            this.BotonVender.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.BotonVender.UseVisualStyleBackColor = false;
            // 
            // LabelIVAPrecio
            // 
            this.LabelIVAPrecio.AutoSize = true;
            this.LabelIVAPrecio.Font = new System.Drawing.Font("Century Gothic", 18.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelIVAPrecio.ForeColor = System.Drawing.Color.DimGray;
            this.LabelIVAPrecio.Location = new System.Drawing.Point(546, 100);
            this.LabelIVAPrecio.Name = "LabelIVAPrecio";
            this.LabelIVAPrecio.Size = new System.Drawing.Size(42, 31);
            this.LabelIVAPrecio.TabIndex = 16;
            this.LabelIVAPrecio.Text = "45";
            // 
            // ContedorControles
            // 
            this.ContedorControles.Controls.Add(this.LabelIVAPrecio);
            this.ContedorControles.Controls.Add(this.LabelUSDPrecio);
            this.ContedorControles.Controls.Add(this.LabelIVA);
            this.ContedorControles.Controls.Add(this.LabelUSD);
            this.ContedorControles.Controls.Add(this.LabelTotalPrecio);
            this.ContedorControles.Controls.Add(this.LabelTotal);
            this.ContedorControles.Controls.Add(this.BotonVender);
            this.ContedorControles.Controls.Add(this.Cancelar);
            this.ContedorControles.Controls.Add(this.Recarga);
            this.ContedorControles.Controls.Add(this.Vascula);
            this.ContedorControles.Controls.Add(this.Descuento);
            this.ContedorControles.Controls.Add(this.Remover);
            this.ContedorControles.Controls.Add(this.Editar);
            this.ContedorControles.Controls.Add(this.Buscar);
            this.ContedorControles.Location = new System.Drawing.Point(397, 2);
            this.ContedorControles.Name = "ContedorControles";
            this.ContedorControles.Size = new System.Drawing.Size(810, 135);
            this.ContedorControles.TabIndex = 13;
            // 
            // LabelUSDPrecio
            // 
            this.LabelUSDPrecio.AutoSize = true;
            this.LabelUSDPrecio.Font = new System.Drawing.Font("Century Gothic", 18.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelUSDPrecio.ForeColor = System.Drawing.Color.DimGray;
            this.LabelUSDPrecio.Location = new System.Drawing.Point(546, 65);
            this.LabelUSDPrecio.Name = "LabelUSDPrecio";
            this.LabelUSDPrecio.Size = new System.Drawing.Size(42, 31);
            this.LabelUSDPrecio.TabIndex = 15;
            this.LabelUSDPrecio.Text = "36";
            // 
            // Cancelar
            // 
            this.Cancelar.BackColor = System.Drawing.Color.Transparent;
            this.Cancelar.FlatAppearance.BorderSize = 0;
            this.Cancelar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.Cancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cancelar.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cancelar.ForeColor = System.Drawing.Color.DimGray;
            this.Cancelar.Image = global::Reef___Punto_de_Venta.Properties.Resources.ProductosCancelar;
            this.Cancelar.Location = new System.Drawing.Point(183, 68);
            this.Cancelar.Name = "Cancelar";
            this.Cancelar.Size = new System.Drawing.Size(85, 66);
            this.Cancelar.TabIndex = 9;
            this.Cancelar.TabStop = false;
            this.Cancelar.Text = "Cancelar";
            this.Cancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Cancelar.UseVisualStyleBackColor = false;
            // 
            // Recarga
            // 
            this.Recarga.BackColor = System.Drawing.Color.Transparent;
            this.Recarga.FlatAppearance.BorderSize = 0;
            this.Recarga.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.Recarga.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Recarga.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Recarga.ForeColor = System.Drawing.Color.DimGray;
            this.Recarga.Image = global::Reef___Punto_de_Venta.Properties.Resources.VenderRecarga;
            this.Recarga.Location = new System.Drawing.Point(183, 68);
            this.Recarga.Name = "Recarga";
            this.Recarga.Size = new System.Drawing.Size(85, 66);
            this.Recarga.TabIndex = 9;
            this.Recarga.Text = "Recarga";
            this.Recarga.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Recarga.UseVisualStyleBackColor = false;
            // 
            // Vascula
            // 
            this.Vascula.BackColor = System.Drawing.Color.Transparent;
            this.Vascula.FlatAppearance.BorderSize = 0;
            this.Vascula.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.Vascula.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Vascula.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Vascula.ForeColor = System.Drawing.Color.DimGray;
            this.Vascula.Image = global::Reef___Punto_de_Venta.Properties.Resources.VenderVascula;
            this.Vascula.Location = new System.Drawing.Point(183, 1);
            this.Vascula.Name = "Vascula";
            this.Vascula.Size = new System.Drawing.Size(85, 66);
            this.Vascula.TabIndex = 5;
            this.Vascula.TabStop = false;
            this.Vascula.Text = "Vascula";
            this.Vascula.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Vascula.UseVisualStyleBackColor = false;
            // 
            // Descuento
            // 
            this.Descuento.BackColor = System.Drawing.Color.Transparent;
            this.Descuento.FlatAppearance.BorderSize = 0;
            this.Descuento.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.Descuento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Descuento.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Descuento.ForeColor = System.Drawing.Color.DimGray;
            this.Descuento.Image = global::Reef___Punto_de_Venta.Properties.Resources.VenderDescuento;
            this.Descuento.Location = new System.Drawing.Point(92, 68);
            this.Descuento.Name = "Descuento";
            this.Descuento.Size = new System.Drawing.Size(85, 66);
            this.Descuento.TabIndex = 7;
            this.Descuento.TabStop = false;
            this.Descuento.Text = "Descuento";
            this.Descuento.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Descuento.UseVisualStyleBackColor = false;
            // 
            // Remover
            // 
            this.Remover.BackColor = System.Drawing.Color.Transparent;
            this.Remover.FlatAppearance.BorderSize = 0;
            this.Remover.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.Remover.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Remover.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Remover.ForeColor = System.Drawing.Color.DimGray;
            this.Remover.Image = global::Reef___Punto_de_Venta.Properties.Resources.ProductosRemover;
            this.Remover.Location = new System.Drawing.Point(92, 1);
            this.Remover.Name = "Remover";
            this.Remover.Size = new System.Drawing.Size(85, 66);
            this.Remover.TabIndex = 4;
            this.Remover.TabStop = false;
            this.Remover.Text = "Remover";
            this.Remover.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Remover.UseVisualStyleBackColor = false;
            // 
            // Editar
            // 
            this.Editar.BackColor = System.Drawing.Color.Transparent;
            this.Editar.FlatAppearance.BorderSize = 0;
            this.Editar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.Editar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Editar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Editar.ForeColor = System.Drawing.Color.DimGray;
            this.Editar.Image = global::Reef___Punto_de_Venta.Properties.Resources.ProductosEditar1;
            this.Editar.Location = new System.Drawing.Point(1, 68);
            this.Editar.Name = "Editar";
            this.Editar.Size = new System.Drawing.Size(85, 66);
            this.Editar.TabIndex = 6;
            this.Editar.TabStop = false;
            this.Editar.Text = "Editar";
            this.Editar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Editar.UseVisualStyleBackColor = false;
            // 
            // Buscar
            // 
            this.Buscar.BackColor = System.Drawing.Color.Transparent;
            this.Buscar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Buscar.FlatAppearance.BorderSize = 0;
            this.Buscar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.Buscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Buscar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Buscar.ForeColor = System.Drawing.Color.DimGray;
            this.Buscar.Image = global::Reef___Punto_de_Venta.Properties.Resources.ProductosBuscar;
            this.Buscar.Location = new System.Drawing.Point(1, 1);
            this.Buscar.Name = "Buscar";
            this.Buscar.Size = new System.Drawing.Size(85, 66);
            this.Buscar.TabIndex = 3;
            this.Buscar.TabStop = false;
            this.Buscar.Text = "Buscar";
            this.Buscar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Buscar.UseVisualStyleBackColor = false;
            // 
            // ContenedorControles
            // 
            this.ContenedorControles.BackColor = System.Drawing.Color.White;
            this.ContenedorControles.Controls.Add(this.ContedorControles);
            this.ContenedorControles.Controls.Add(this.TextBoxDescripcion);
            this.ContenedorControles.Controls.Add(this.ContenedorDescripcion);
            this.ContenedorControles.Controls.Add(this.label1);
            this.ContenedorControles.Controls.Add(this.TextBoxClave);
            this.ContenedorControles.Controls.Add(this.ContenedorClave);
            this.ContenedorControles.Controls.Add(this.LabelProductos);
            this.ContenedorControles.Controls.Add(this.ImagenProducto);
            this.ContenedorControles.Location = new System.Drawing.Point(0, 57);
            this.ContenedorControles.Name = "ContenedorControles";
            this.ContenedorControles.Size = new System.Drawing.Size(1210, 142);
            this.ContenedorControles.TabIndex = 8;
            // 
            // TextBoxDescripcion
            // 
            this.TextBoxDescripcion.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.TextBoxDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextBoxDescripcion.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxDescripcion.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.TextBoxDescripcion.HideSelection = false;
            this.TextBoxDescripcion.Location = new System.Drawing.Point(136, 100);
            this.TextBoxDescripcion.MaxLength = 100;
            this.TextBoxDescripcion.Name = "TextBoxDescripcion";
            this.TextBoxDescripcion.Size = new System.Drawing.Size(250, 20);
            this.TextBoxDescripcion.TabIndex = 2;
            this.TextBoxDescripcion.WordWrap = false;
            // 
            // ContenedorDescripcion
            // 
            this.ContenedorDescripcion.Image = global::Reef___Punto_de_Venta.Properties.Resources.BuscarContenedor;
            this.ContenedorDescripcion.Location = new System.Drawing.Point(133, 94);
            this.ContenedorDescripcion.Name = "ContenedorDescripcion";
            this.ContenedorDescripcion.Size = new System.Drawing.Size(260, 32);
            this.ContenedorDescripcion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ContenedorDescripcion.TabIndex = 11;
            this.ContenedorDescripcion.TabStop = false;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DimGray;
            this.label1.Location = new System.Drawing.Point(133, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 25);
            this.label1.TabIndex = 10;
            this.label1.Text = "Descripción:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Minimizar
            // 
            this.Minimizar.BackgroundImage = global::Reef___Punto_de_Venta.Properties.Resources.Cerrar1;
            this.Minimizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Minimizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Minimizar.ForeColor = System.Drawing.Color.PowderBlue;
            this.Minimizar.Location = new System.Drawing.Point(1190, 0);
            this.Minimizar.Name = "Minimizar";
            this.Minimizar.Size = new System.Drawing.Size(20, 20);
            this.Minimizar.TabIndex = 2;
            this.Minimizar.TabStop = false;
            this.Minimizar.UseVisualStyleBackColor = true;
            // 
            // LabelResumen
            // 
            this.LabelResumen.AutoSize = true;
            this.LabelResumen.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelResumen.ForeColor = System.Drawing.Color.DimGray;
            this.LabelResumen.Location = new System.Drawing.Point(12, 6);
            this.LabelResumen.Name = "LabelResumen";
            this.LabelResumen.Size = new System.Drawing.Size(250, 38);
            this.LabelResumen.TabIndex = 1;
            this.LabelResumen.Text = "Inicio > Vender";
            // 
            // HeaderVender
            // 
            this.HeaderVender.BackColor = System.Drawing.Color.White;
            this.HeaderVender.Controls.Add(this.Minimizar);
            this.HeaderVender.Controls.Add(this.LabelResumen);
            this.HeaderVender.Location = new System.Drawing.Point(0, 7);
            this.HeaderVender.Name = "HeaderVender";
            this.HeaderVender.Size = new System.Drawing.Size(1210, 50);
            this.HeaderVender.TabIndex = 7;
            // 
            // controlVender
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(131)))), ((int)(((byte)(149)))));
            this.Controls.Add(this.PanelBusqueda);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.PanelEditar);
            this.Controls.Add(this.ContenedorControles);
            this.Controls.Add(this.HeaderVender);
            this.Name = "controlVender";
            this.Size = new System.Drawing.Size(1210, 710);
            ((System.ComponentModel.ISupportInitialize)(this.ImagenSeleccionada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImagenProducto)).EndInit();
            this.PanelBusqueda.ResumeLayout(false);
            this.PanelBusqueda.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorClave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.PanelEditar.ResumeLayout(false);
            this.PanelEditar.PerformLayout();
            this.ContedorControles.ResumeLayout(false);
            this.ContedorControles.PerformLayout();
            this.ContenedorControles.ResumeLayout(false);
            this.ContenedorControles.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorDescripcion)).EndInit();
            this.HeaderVender.ResumeLayout(false);
            this.HeaderVender.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox TextBoxClave;
        private System.Windows.Forms.Button BotonModificar;
        private System.Windows.Forms.Label LabelPorcentajeDesc;
        private System.Windows.Forms.PictureBox ImagenSeleccionada;
        private System.Windows.Forms.Label LabelDescuento;
        private System.Windows.Forms.TextBox TextBoxDescuento;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label LabelPrecio;
        private System.Windows.Forms.TextBox TextBoxPrecio;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label LabelProductos;
        private System.Windows.Forms.PictureBox ImagenProducto;
        private System.Windows.Forms.Panel PanelBusqueda;
        private System.Windows.Forms.Button Limpiar;
        private System.Windows.Forms.Button Aceptar;
        private System.Windows.Forms.TextBox BusquedaCategoria;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox BusquedaDescripcion;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox BusquedaClave;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button CerrarBusqueda;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button BotonModificarCanelar;
        private System.Windows.Forms.Label LabelProductoEditar;
        private System.Windows.Forms.Label LabelCantidad;
        private System.Windows.Forms.TextBox TextBoxCantidad;
        private System.Windows.Forms.PictureBox ContenedorClave;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel PanelEditar;
        private System.Windows.Forms.Label LabelIVA;
        private System.Windows.Forms.Label LabelUSD;
        private System.Windows.Forms.Label LabelTotalPrecio;
        private System.Windows.Forms.Label LabelTotal;
        private System.Windows.Forms.Button BotonVender;
        private System.Windows.Forms.Label LabelIVAPrecio;
        private System.Windows.Forms.Panel ContedorControles;
        private System.Windows.Forms.Label LabelUSDPrecio;
        private System.Windows.Forms.Button Cancelar;
        private System.Windows.Forms.Button Recarga;
        private System.Windows.Forms.Button Vascula;
        private System.Windows.Forms.Button Descuento;
        private System.Windows.Forms.Button Remover;
        private System.Windows.Forms.Button Editar;
        private System.Windows.Forms.Button Buscar;
        private System.Windows.Forms.Panel ContenedorControles;
        private System.Windows.Forms.TextBox TextBoxDescripcion;
        private System.Windows.Forms.PictureBox ContenedorDescripcion;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button Minimizar;
        private System.Windows.Forms.Label LabelResumen;
        private System.Windows.Forms.Panel HeaderVender;
    }
}
