﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reef___Punto_de_Venta
{
    public partial class Splash : Form
    {
        double count = 0;
        Login ardilla;

        public Splash()
        {
            InitializeComponent();
        }

        private void Splash_Load(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            count += 0.05;
            if (count < 1)
            {
                this.Opacity += count;
            }
            else 
            {
                if (count > 1.2)
                {
                    this.Opacity -= 0.05;
                    if (count > 2.2)
                    {
                        timer1.Enabled = false;
                        ardilla = new Login();
                        ardilla.Show();

                        this.Hide();
                    }
                }
            }
        }
    }
}
