﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Reef___Punto_de_Venta
{
    public partial class Login : Form
    {
        string userType, userName, userId;
        public static Inicio ini;

        public Login()
        {
            InitializeComponent();
        }

        private void Login_Load(object sender, EventArgs e)
        {
            MyConnection con = new MyConnection();
            con.Crear_conexion();

            string query = "select Nom_us from login;";

            MySqlCommand cmd = new MySqlCommand(query, con.getConexion());
            MySqlDataAdapter da1 = new MySqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da1.Fill(dt);

            Usuarios.ValueMember = "Nom_us";
            Usuarios.DataSource = dt;
        }

        private void pictureBox4_Click_1(object sender, EventArgs e)
        {
            MyConnection con = new MyConnection();
            MySqlCommand cmd;
            MySqlDataReader da1;
            string query, pass;

            try
            {
                con.Crear_conexion();
                query = string.Format("select pass, T_user, Nom_us, id_user from login where Nom_us = '{0}'", Usuarios.Text);
                cmd = new MySqlCommand(query, con.getConexion());
                da1 = cmd.ExecuteReader();
                if (da1.Read())
                {
                    pass = da1.GetString("pass");
                    userType = da1.GetString("Nom_us");
                    userName = da1.GetString("T_user");
                    userId = da1.GetString("id_user");

                    if (Contrasena.Text == pass)
                    {
                        MessageBox.Show("Acceso Concedido");
                        this.Hide();

                        ini = new Inicio(userType, userName,userId);
                        ini.Show();
                    }

                }
                else
                {
                    MessageBox.Show("Contraseña Incorrecta");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Contrasena_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == Convert.ToChar(Keys.Enter)) { pictureBox4_Click_1(pictureBox4, null); }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
