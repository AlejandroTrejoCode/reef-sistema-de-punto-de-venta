﻿namespace Reef___Punto_de_Venta
{
    partial class controlDashboard
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.DashRow1 = new System.Windows.Forms.Panel();
            this.Tarjeta4 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.Gmes = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.Tarjeta3 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.Gsemana = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.Tarjeta2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.Gdia = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Tarjeta1 = new System.Windows.Forms.Panel();
            this.LabelFecha = new System.Windows.Forms.Label();
            this.LabelHora = new System.Windows.Forms.Label();
            this.DashRow2 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Tarjeta8 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Locacion = new System.Windows.Forms.Label();
            this.Tarjeta7 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.Tarjeta6 = new System.Windows.Forms.Panel();
            this.LabelMayor = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.Tarjeta5 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.DashHeader = new System.Windows.Forms.Panel();
            this.LabelNombreTienda = new System.Windows.Forms.Label();
            this.LabelDireccion = new System.Windows.Forms.Label();
            this.UbicacionImg = new System.Windows.Forms.PictureBox();
            this.LabelResumen = new System.Windows.Forms.Label();
            this.TimerHora = new System.Windows.Forms.Timer(this.components);
            this.DashRow1.SuspendLayout();
            this.Tarjeta4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Gmes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.Tarjeta3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Gsemana)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.Tarjeta2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Gdia)).BeginInit();
            this.Tarjeta1.SuspendLayout();
            this.DashRow2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.Tarjeta8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.Tarjeta7.SuspendLayout();
            this.Tarjeta6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            this.Tarjeta5.SuspendLayout();
            this.DashHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UbicacionImg)).BeginInit();
            this.SuspendLayout();
            // 
            // DashRow1
            // 
            this.DashRow1.Controls.Add(this.Tarjeta4);
            this.DashRow1.Controls.Add(this.Tarjeta3);
            this.DashRow1.Controls.Add(this.Tarjeta2);
            this.DashRow1.Controls.Add(this.Tarjeta1);
            this.DashRow1.Location = new System.Drawing.Point(0, 120);
            this.DashRow1.Name = "DashRow1";
            this.DashRow1.Size = new System.Drawing.Size(1210, 300);
            this.DashRow1.TabIndex = 4;
            // 
            // Tarjeta4
            // 
            this.Tarjeta4.BackColor = System.Drawing.Color.White;
            this.Tarjeta4.Controls.Add(this.label3);
            this.Tarjeta4.Controls.Add(this.Gmes);
            this.Tarjeta4.Controls.Add(this.dataGridView3);
            this.Tarjeta4.Location = new System.Drawing.Point(910, 10);
            this.Tarjeta4.Name = "Tarjeta4";
            this.Tarjeta4.Size = new System.Drawing.Size(295, 280);
            this.Tarjeta4.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.DimGray;
            this.label3.Location = new System.Drawing.Point(11, 2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(273, 36);
            this.label3.TabIndex = 7;
            this.label3.Text = "Ventas Mensuales\r\n";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Gmes
            // 
            this.Gmes.BackColor = System.Drawing.Color.Transparent;
            chartArea4.AxisX.LineColor = System.Drawing.Color.Gray;
            chartArea4.AxisX.LineWidth = 2;
            chartArea4.AxisX.MajorGrid.LineColor = System.Drawing.Color.Transparent;
            chartArea4.AxisX.MajorTickMark.LineColor = System.Drawing.Color.Transparent;
            chartArea4.AxisY.LineColor = System.Drawing.Color.Gray;
            chartArea4.AxisY.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea4.AxisY.LineWidth = 2;
            chartArea4.AxisY.MajorGrid.LineColor = System.Drawing.Color.Transparent;
            chartArea4.Name = "ChartArea1";
            this.Gmes.ChartAreas.Add(chartArea4);
            legend4.Enabled = false;
            legend4.Name = "Legend1";
            this.Gmes.Legends.Add(legend4);
            this.Gmes.Location = new System.Drawing.Point(2, 39);
            this.Gmes.Name = "Gmes";
            this.Gmes.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this.Gmes.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(200)))), ((int)(((byte)(163)))))};
            series4.ChartArea = "ChartArea1";
            series4.CustomProperties = "PointWidth=0.9";
            series4.Legend = "Legend1";
            series4.Name = "Series1";
            this.Gmes.Series.Add(series4);
            this.Gmes.Size = new System.Drawing.Size(291, 240);
            this.Gmes.TabIndex = 2;
            this.Gmes.Text = "chart3";
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(28, 46);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(240, 150);
            this.dataGridView3.TabIndex = 8;
            this.dataGridView3.Visible = false;
            // 
            // Tarjeta3
            // 
            this.Tarjeta3.BackColor = System.Drawing.Color.White;
            this.Tarjeta3.Controls.Add(this.label2);
            this.Tarjeta3.Controls.Add(this.Gsemana);
            this.Tarjeta3.Controls.Add(this.dataGridView2);
            this.Tarjeta3.Location = new System.Drawing.Point(608, 10);
            this.Tarjeta3.Name = "Tarjeta3";
            this.Tarjeta3.Size = new System.Drawing.Size(295, 280);
            this.Tarjeta3.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DimGray;
            this.label2.Location = new System.Drawing.Point(9, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(277, 36);
            this.label2.TabIndex = 6;
            this.label2.Text = "Ventas Semanales\r\n";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Gsemana
            // 
            this.Gsemana.BackColor = System.Drawing.Color.Transparent;
            chartArea5.AxisX.LineColor = System.Drawing.Color.Gray;
            chartArea5.AxisX.LineWidth = 2;
            chartArea5.AxisX.MajorGrid.LineColor = System.Drawing.Color.Transparent;
            chartArea5.AxisX.MajorTickMark.LineColor = System.Drawing.Color.Transparent;
            chartArea5.AxisY.LineColor = System.Drawing.Color.DimGray;
            chartArea5.AxisY.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea5.AxisY.LineWidth = 2;
            chartArea5.AxisY.MajorGrid.LineColor = System.Drawing.Color.Transparent;
            chartArea5.Name = "ChartArea1";
            this.Gsemana.ChartAreas.Add(chartArea5);
            legend5.Enabled = false;
            legend5.Name = "Legend1";
            this.Gsemana.Legends.Add(legend5);
            this.Gsemana.Location = new System.Drawing.Point(1, 39);
            this.Gsemana.Name = "Gsemana";
            this.Gsemana.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this.Gsemana.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(200)))), ((int)(((byte)(163)))))};
            series5.ChartArea = "ChartArea1";
            series5.CustomProperties = "PointWidth=0.9";
            series5.Legend = "Legend1";
            series5.Name = "Series1";
            this.Gsemana.Series.Add(series5);
            this.Gsemana.Size = new System.Drawing.Size(293, 240);
            this.Gsemana.TabIndex = 2;
            this.Gsemana.Text = "chart2";
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(27, 56);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(240, 150);
            this.dataGridView2.TabIndex = 7;
            this.dataGridView2.Visible = false;
            // 
            // Tarjeta2
            // 
            this.Tarjeta2.BackColor = System.Drawing.Color.White;
            this.Tarjeta2.Controls.Add(this.label1);
            this.Tarjeta2.Controls.Add(this.Gdia);
            this.Tarjeta2.Location = new System.Drawing.Point(306, 10);
            this.Tarjeta2.Name = "Tarjeta2";
            this.Tarjeta2.Size = new System.Drawing.Size(295, 280);
            this.Tarjeta2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DimGray;
            this.label1.Location = new System.Drawing.Point(39, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(218, 36);
            this.label1.TabIndex = 7;
            this.label1.Text = "Ventas Diarias";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Gdia
            // 
            this.Gdia.BackColor = System.Drawing.Color.Transparent;
            this.Gdia.BorderlineWidth = 0;
            chartArea6.Area3DStyle.Inclination = 1;
            chartArea6.AxisX.LineColor = System.Drawing.Color.Gray;
            chartArea6.AxisX.LineWidth = 2;
            chartArea6.AxisX.MajorGrid.LineColor = System.Drawing.Color.Transparent;
            chartArea6.AxisX.MajorTickMark.LineColor = System.Drawing.Color.Transparent;
            chartArea6.AxisY.LineColor = System.Drawing.Color.Gray;
            chartArea6.AxisY.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea6.AxisY.LineWidth = 2;
            chartArea6.AxisY.MajorGrid.LineColor = System.Drawing.Color.Transparent;
            chartArea6.BorderColor = System.Drawing.Color.DarkRed;
            chartArea6.Name = "ChartArea1";
            this.Gdia.ChartAreas.Add(chartArea6);
            legend6.Alignment = System.Drawing.StringAlignment.Center;
            legend6.BorderWidth = 0;
            legend6.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend6.Enabled = false;
            legend6.Font = new System.Drawing.Font("Century Gothic", 1.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend6.ForeColor = System.Drawing.Color.DimGray;
            legend6.HeaderSeparatorColor = System.Drawing.Color.DimGray;
            legend6.IsTextAutoFit = false;
            legend6.ItemColumnSeparatorColor = System.Drawing.Color.DimGray;
            legend6.Name = "Legend1";
            legend6.ShadowColor = System.Drawing.Color.White;
            legend6.Title = "Ventas Diarias";
            legend6.TitleFont = new System.Drawing.Font("Century Gothic", 17.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend6.TitleForeColor = System.Drawing.Color.DimGray;
            legend6.TitleSeparator = System.Windows.Forms.DataVisualization.Charting.LegendSeparatorStyle.GradientLine;
            legend6.TitleSeparatorColor = System.Drawing.Color.DimGray;
            this.Gdia.Legends.Add(legend6);
            this.Gdia.Location = new System.Drawing.Point(2, 39);
            this.Gdia.Name = "Gdia";
            this.Gdia.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this.Gdia.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(200)))), ((int)(((byte)(163)))))};
            series6.ChartArea = "ChartArea1";
            series6.CustomProperties = "PointWidth=0.9";
            series6.EmptyPointStyle.LabelForeColor = System.Drawing.Color.DimGray;
            series6.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series6.LabelBorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            series6.LabelBorderWidth = 0;
            series6.LabelForeColor = System.Drawing.Color.DimGray;
            series6.Legend = "Legend1";
            series6.MarkerBorderWidth = 0;
            series6.Name = "Series1";
            series6.ShadowColor = System.Drawing.Color.Red;
            series6.SmartLabelStyle.AllowOutsidePlotArea = System.Windows.Forms.DataVisualization.Charting.LabelOutsidePlotAreaStyle.No;
            series6.SmartLabelStyle.CalloutLineColor = System.Drawing.Color.White;
            series6.SmartLabelStyle.CalloutLineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            series6.SmartLabelStyle.CalloutLineWidth = 0;
            series6.SmartLabelStyle.CalloutStyle = System.Windows.Forms.DataVisualization.Charting.LabelCalloutStyle.None;
            series6.SmartLabelStyle.IsOverlappedHidden = false;
            series6.YValuesPerPoint = 4;
            this.Gdia.Series.Add(series6);
            this.Gdia.Size = new System.Drawing.Size(293, 240);
            this.Gdia.TabIndex = 1;
            // 
            // Tarjeta1
            // 
            this.Tarjeta1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(200)))), ((int)(((byte)(163)))));
            this.Tarjeta1.Controls.Add(this.LabelFecha);
            this.Tarjeta1.Controls.Add(this.LabelHora);
            this.Tarjeta1.Location = new System.Drawing.Point(4, 10);
            this.Tarjeta1.Name = "Tarjeta1";
            this.Tarjeta1.Size = new System.Drawing.Size(295, 280);
            this.Tarjeta1.TabIndex = 0;
            // 
            // LabelFecha
            // 
            this.LabelFecha.Font = new System.Drawing.Font("Century Gothic", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelFecha.ForeColor = System.Drawing.Color.White;
            this.LabelFecha.Location = new System.Drawing.Point(1, 150);
            this.LabelFecha.Name = "LabelFecha";
            this.LabelFecha.Size = new System.Drawing.Size(290, 56);
            this.LabelFecha.TabIndex = 6;
            this.LabelFecha.Text = "LUN 22, JUL 16";
            this.LabelFecha.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LabelHora
            // 
            this.LabelHora.AutoSize = true;
            this.LabelHora.Font = new System.Drawing.Font("Century Gothic", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelHora.ForeColor = System.Drawing.Color.White;
            this.LabelHora.Location = new System.Drawing.Point(1, 40);
            this.LabelHora.Name = "LabelHora";
            this.LabelHora.Size = new System.Drawing.Size(290, 112);
            this.LabelHora.TabIndex = 5;
            this.LabelHora.Text = "13:26";
            // 
            // DashRow2
            // 
            this.DashRow2.Controls.Add(this.dataGridView1);
            this.DashRow2.Controls.Add(this.Tarjeta8);
            this.DashRow2.Controls.Add(this.Tarjeta7);
            this.DashRow2.Controls.Add(this.Tarjeta6);
            this.DashRow2.Controls.Add(this.Tarjeta5);
            this.DashRow2.Location = new System.Drawing.Point(0, 420);
            this.DashRow2.Name = "DashRow2";
            this.DashRow2.Size = new System.Drawing.Size(1211, 290);
            this.DashRow2.TabIndex = 5;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(1120, 277);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(74, 10);
            this.dataGridView1.TabIndex = 4;
            this.dataGridView1.Visible = false;
            // 
            // Tarjeta8
            // 
            this.Tarjeta8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(191)))), ((int)(((byte)(217)))));
            this.Tarjeta8.Controls.Add(this.pictureBox1);
            this.Tarjeta8.Controls.Add(this.label5);
            this.Tarjeta8.Controls.Add(this.label4);
            this.Tarjeta8.Controls.Add(this.Locacion);
            this.Tarjeta8.Location = new System.Drawing.Point(910, 6);
            this.Tarjeta8.Name = "Tarjeta8";
            this.Tarjeta8.Size = new System.Drawing.Size(295, 265);
            this.Tarjeta8.TabIndex = 2;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Reef___Punto_de_Venta.Properties.Resources.partly_cloudy;
            this.pictureBox1.Location = new System.Drawing.Point(60, 147);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(60, 66);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(187, 147);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 25);
            this.label5.TabIndex = 8;
            this.label5.Text = "°C";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(114, 147);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 66);
            this.label4.TabIndex = 7;
            this.label4.Text = "30";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Locacion
            // 
            this.Locacion.AutoSize = true;
            this.Locacion.Font = new System.Drawing.Font("Century Gothic", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Locacion.ForeColor = System.Drawing.Color.White;
            this.Locacion.Location = new System.Drawing.Point(35, 19);
            this.Locacion.Name = "Locacion";
            this.Locacion.Size = new System.Drawing.Size(224, 112);
            this.Locacion.TabIndex = 6;
            this.Locacion.Text = "Cancún \r\nQ. Roo";
            this.Locacion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Tarjeta7
            // 
            this.Tarjeta7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(225)))), ((int)(((byte)(94)))));
            this.Tarjeta7.Controls.Add(this.label11);
            this.Tarjeta7.Controls.Add(this.label10);
            this.Tarjeta7.Location = new System.Drawing.Point(608, 6);
            this.Tarjeta7.Name = "Tarjeta7";
            this.Tarjeta7.Size = new System.Drawing.Size(295, 265);
            this.Tarjeta7.TabIndex = 3;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(9, 49);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(283, 182);
            this.label11.TabIndex = 14;
            this.label11.Text = "-Verificar ventas\r\n- Revisar ofertas\r\n-El clima favorece a los chocolates\r\n";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(10, 4);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(277, 44);
            this.label10.TabIndex = 13;
            this.label10.Text = "Notificaciones";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Tarjeta6
            // 
            this.Tarjeta6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(187)))), ((int)(((byte)(228)))));
            this.Tarjeta6.Controls.Add(this.LabelMayor);
            this.Tarjeta6.Controls.Add(this.label8);
            this.Tarjeta6.Controls.Add(this.dataGridView4);
            this.Tarjeta6.Location = new System.Drawing.Point(306, 6);
            this.Tarjeta6.Name = "Tarjeta6";
            this.Tarjeta6.Size = new System.Drawing.Size(295, 265);
            this.Tarjeta6.TabIndex = 2;
            // 
            // LabelMayor
            // 
            this.LabelMayor.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelMayor.ForeColor = System.Drawing.Color.White;
            this.LabelMayor.Location = new System.Drawing.Point(2, 100);
            this.LabelMayor.Name = "LabelMayor";
            this.LabelMayor.Size = new System.Drawing.Size(290, 72);
            this.LabelMayor.TabIndex = 7;
            this.LabelMayor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 29.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(14, 5);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(265, 45);
            this.label8.TabIndex = 12;
            this.label8.Text = "Más vendido";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dataGridView4
            // 
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Location = new System.Drawing.Point(17, 60);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.Size = new System.Drawing.Size(262, 171);
            this.dataGridView4.TabIndex = 16;
            this.dataGridView4.Visible = false;
            // 
            // Tarjeta5
            // 
            this.Tarjeta5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(123)))), ((int)(((byte)(174)))));
            this.Tarjeta5.Controls.Add(this.label7);
            this.Tarjeta5.Controls.Add(this.label6);
            this.Tarjeta5.Location = new System.Drawing.Point(4, 6);
            this.Tarjeta5.Name = "Tarjeta5";
            this.Tarjeta5.Size = new System.Drawing.Size(295, 265);
            this.Tarjeta5.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(3, 78);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(286, 171);
            this.label7.TabIndex = 11;
            this.label7.Text = "Aún faltan más ventas";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 33.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(5, 1);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(284, 53);
            this.label6.TabIndex = 10;
            this.label6.Text = "¡Promoción!";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DashHeader
            // 
            this.DashHeader.BackColor = System.Drawing.Color.White;
            this.DashHeader.Controls.Add(this.LabelNombreTienda);
            this.DashHeader.Controls.Add(this.LabelDireccion);
            this.DashHeader.Controls.Add(this.UbicacionImg);
            this.DashHeader.Controls.Add(this.LabelResumen);
            this.DashHeader.ForeColor = System.Drawing.Color.Transparent;
            this.DashHeader.Location = new System.Drawing.Point(0, 1);
            this.DashHeader.Name = "DashHeader";
            this.DashHeader.Size = new System.Drawing.Size(1208, 123);
            this.DashHeader.TabIndex = 6;
            this.DashHeader.Paint += new System.Windows.Forms.PaintEventHandler(this.DashHeader_Paint_1);
            // 
            // LabelNombreTienda
            // 
            this.LabelNombreTienda.AutoSize = true;
            this.LabelNombreTienda.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelNombreTienda.ForeColor = System.Drawing.Color.DimGray;
            this.LabelNombreTienda.Location = new System.Drawing.Point(21, 50);
            this.LabelNombreTienda.Name = "LabelNombreTienda";
            this.LabelNombreTienda.Size = new System.Drawing.Size(115, 15);
            this.LabelNombreTienda.TabIndex = 1;
            this.LabelNombreTienda.Text = "Abarrotes: Guzmán";
            // 
            // LabelDireccion
            // 
            this.LabelDireccion.AutoSize = true;
            this.LabelDireccion.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelDireccion.ForeColor = System.Drawing.Color.DimGray;
            this.LabelDireccion.Location = new System.Drawing.Point(20, 66);
            this.LabelDireccion.Name = "LabelDireccion";
            this.LabelDireccion.Size = new System.Drawing.Size(189, 30);
            this.LabelDireccion.TabIndex = 1;
            this.LabelDireccion.Text = "Reg. 230, Calle 62, Mzna: 12, Lt: 14\r\nCancún, Quintana Roo";
            // 
            // UbicacionImg
            // 
            this.UbicacionImg.Image = global::Reef___Punto_de_Venta.Properties.Resources.Ubicacion_fw;
            this.UbicacionImg.Location = new System.Drawing.Point(2, 60);
            this.UbicacionImg.Name = "UbicacionImg";
            this.UbicacionImg.Size = new System.Drawing.Size(20, 25);
            this.UbicacionImg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.UbicacionImg.TabIndex = 2;
            this.UbicacionImg.TabStop = false;
            // 
            // LabelResumen
            // 
            this.LabelResumen.AutoSize = true;
            this.LabelResumen.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelResumen.ForeColor = System.Drawing.Color.DimGray;
            this.LabelResumen.Location = new System.Drawing.Point(8, 14);
            this.LabelResumen.Name = "LabelResumen";
            this.LabelResumen.Size = new System.Drawing.Size(259, 39);
            this.LabelResumen.TabIndex = 0;
            this.LabelResumen.Text = "Resumen Diario";
            // 
            // TimerHora
            // 
            this.TimerHora.Enabled = true;
            this.TimerHora.Tick += new System.EventHandler(this.TimerHora_Tick);
            // 
            // controlDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.DashHeader);
            this.Controls.Add(this.DashRow2);
            this.Controls.Add(this.DashRow1);
            this.Name = "controlDashboard";
            this.Size = new System.Drawing.Size(1210, 710);
            this.Load += new System.EventHandler(this.controlDashboard_Load);
            this.DashRow1.ResumeLayout(false);
            this.Tarjeta4.ResumeLayout(false);
            this.Tarjeta4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Gmes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.Tarjeta3.ResumeLayout(false);
            this.Tarjeta3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Gsemana)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.Tarjeta2.ResumeLayout(false);
            this.Tarjeta2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Gdia)).EndInit();
            this.Tarjeta1.ResumeLayout(false);
            this.Tarjeta1.PerformLayout();
            this.DashRow2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.Tarjeta8.ResumeLayout(false);
            this.Tarjeta8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.Tarjeta7.ResumeLayout(false);
            this.Tarjeta7.PerformLayout();
            this.Tarjeta6.ResumeLayout(false);
            this.Tarjeta6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            this.Tarjeta5.ResumeLayout(false);
            this.Tarjeta5.PerformLayout();
            this.DashHeader.ResumeLayout(false);
            this.DashHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UbicacionImg)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel DashRow1;
        public System.Windows.Forms.Panel Tarjeta4;
        public System.Windows.Forms.Panel Tarjeta3;
        public System.Windows.Forms.Panel Tarjeta2;
        public System.Windows.Forms.Panel Tarjeta1;
        private System.Windows.Forms.Label LabelFecha;
        private System.Windows.Forms.Label LabelHora;
        private System.Windows.Forms.Panel DashRow2;
        public System.Windows.Forms.Panel Tarjeta8;
        public System.Windows.Forms.Panel Tarjeta7;
        public System.Windows.Forms.Panel Tarjeta6;
        public System.Windows.Forms.Panel Tarjeta5;
        private System.Windows.Forms.Panel DashHeader;
        public System.Windows.Forms.Label LabelDireccion;
        private System.Windows.Forms.PictureBox UbicacionImg;
        private System.Windows.Forms.Label LabelResumen;
        private System.Windows.Forms.Timer TimerHora;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataVisualization.Charting.Chart Gmes;
        private System.Windows.Forms.DataVisualization.Charting.Chart Gsemana;
        private System.Windows.Forms.DataVisualization.Charting.Chart Gdia;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label LabelNombreTienda;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Label Locacion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label LabelMayor;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.Label label11;
    }
}
