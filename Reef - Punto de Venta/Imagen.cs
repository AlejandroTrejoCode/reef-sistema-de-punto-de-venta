﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

namespace Reef___Punto_de_Venta
{
    class Imagen
    {
        private static MyConnection conecta = new MyConnection();
        public static void ActualziarImagen(Image imagen, string tabla, string campoid, string id)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                imagen.Save(ms, ImageFormat.Gif);
                byte[] imgArr = ms.ToArray();
                MyConnection m = new MyConnection();
                m.Crear_conexion();
                using (MySqlConnection conn = m.getConexion())
                {
                    using (MySqlCommand cmd = new MySqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandText = "UPDATE " + tabla + " set img = @imgArr where " + campoid + " = @id";
                        cmd.Parameters.AddWithValue("@imgArr", imgArr);
                        cmd.Parameters.AddWithValue("@id", id);
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("insercion exitosa");
                    }
                }
            }
        }
        public static Image CargarImagen(string idcampo, string idvalue)
        {
            MyConnection m = new MyConnection();
            m.Crear_conexion();
            using (MySqlConnection conn = m.getConexion())
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "SELECT img FROM productos where " + idcampo + " = @id";
                    cmd.Parameters.AddWithValue("@id", idvalue);
                    byte[] imgArr = (byte[])cmd.ExecuteScalar();
                    imgArr = (byte[])cmd.ExecuteScalar();
                    using (var stream = new MemoryStream(imgArr))
                    {
                        Image img = Image.FromStream(stream);
                        return img;
                    }
                }
            }
        }
        public static void llenar(DataGridView d, string tabla, string[] campos)
        {
            StringBuilder s = new StringBuilder();
            s.Append("select ");
            for (int i = 0; i < campos.Length - 1; i++)
            {
                s.Append(campos[i]);
                s.Append(",");
            }
            s.Append(campos[campos.Length - 1]);
            s.Append(" from ");
            s.Append(tabla);
            s.Append(";");
            conecta.Crear_conexion();
            MySqlCommand buscarproductos = new MySqlCommand(s.ToString(), conecta.getConexion());
            MySqlDataAdapter cmc = new MySqlDataAdapter(buscarproductos);
            DataSet tht = new DataSet();
            cmc.Fill(tht, tabla);
            d.DataSource = tht.Tables[tabla].DefaultView;
        }
        public static Bitmap ResizeImage(Image image)
        {
            var destRect = new Rectangle(0, 0, 130, 134);
            var destImage = new Bitmap(130, 134);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }
            return destImage;
        }
    }
}
