﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reef___Punto_de_Venta
{
    public partial class controlDashboard : UserControl
    {
        public controlDashboard()
        {
            InitializeComponent();
        }        

        private void TimerHora_Tick(object sender, EventArgs e)
        {
            string HorasyMinutos = DateTime.Now.ToString("HH:mm");
            LabelHora.Text = HorasyMinutos;
            string Fecha = DateTime.Now.ToString("ddd d, MMM").ToUpper();
            LabelFecha.Text = Fecha;
        }
               
        private void DashHeader_Paint_1(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics, this.DashHeader.ClientRectangle, Color.FromArgb(165, 182, 195), ButtonBorderStyle.Solid);
        }

        int[] dias = { 0, 7, 30 };//Nuevo Luis
        private void controlDashboard_Load(object sender, EventArgs e)
        {

        /*
         *viejo
         * 
            //Para seguir graficando
            try
            {
                string f = DateTime.Now.ToString("yyMMdd"), i = DateTime.Now.AddDays(-5).ToString("yyMMdd");
                Sentencias.grafica(i, f, dataGridView1, "ventas");
                Gdia.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
                for (int p = 0; p < dataGridView1.RowCount; p++)
                {
                    string x = dataGridView1.Rows[p].Cells["descripcion"].Value.ToString();
                    string y = dataGridView1.Rows[p].Cells["total"].Value.ToString();
                    Gdia.Series[0].Points.AddXY(x, y);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " +ex);
            }     
        *
        */ 
        

            grafica(dias[0], 1);


        }

        private void grafica(int dias, int s)
        {
            string[] c = { "detalle_venta.id_producto", "productos.descripcion", "ventas.fecha_ven" };
            string[] v = { "ventas", "detalle_venta", "productos" };
            string condition = "and (detalle_venta.id_venta=ventas.id_venta) and detalle_venta.id_producto=productos.id_producto";
            string g = "detalle_venta.id_producto";
            string f = DateTime.Now.ToString("yyMMdd"), i = DateTime.Now.AddDays(-dias).ToString("yyMMdd");
            if (s == 1)
            {
                Sentencias.grafica(i, f, dataGridView1, "ventas", c, "detalle_venta.cantidad", "total", v, condition, g);
            }
            else if (s == 2)
            {
                Sentencias.grafica(i, f, dataGridView2, "ventas1", c, "detalle_venta.cantidad", "total", v, condition, g);
            }
            else if (s == 3)
            {
                Sentencias.grafica(i, f, dataGridView3, "ventas", c, "detalle_venta.cantidad", "total", v, condition, g);
            }
            sgrafica(s);
        }

        //n
        private void sgrafica(int s)
        {
            switch (s)
            {
                case 1:
                    Gdia.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
                    if (dataGridView1.RowCount > 0)
                    {
                        int c = 0;
                        while (c < 10 & c < dataGridView1.RowCount - 1)
                        {
                            string x = dataGridView1.Rows[c].Cells["descripcion"].Value.ToString();
                            string y = dataGridView1.Rows[c].Cells["total"].Value.ToString();
                            Gdia.Series[0].Points.AddXY(x, y);
                            c++;
                        }
                        //for (int p = 0; p < dataGridView1.RowCount - 1; p++)
                        //{
                        //    string x = dataGridView1.Rows[p].Cells["descripcion"].Value.ToString();
                        //    string y = dataGridView1.Rows[p].Cells["total"].Value.ToString();
                        //    Gdia.Series[0].Points.AddXY(x, y);
                        //}
                    }
                    grafica(5, 2);
                    break;
                case 2:
                    Gsemana.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
                    if (dataGridView2.RowCount > 0)
                    {
                        int c = 0;
                        while (c < 10 & c < dataGridView2.RowCount - 1)
                        {
                            string x = dataGridView2.Rows[c].Cells["descripcion"].Value.ToString();
                            string y = dataGridView2.Rows[c].Cells["total"].Value.ToString();
                            Gsemana.Series[0].Points.AddXY(x, y);
                            c++;
                        }
                        //for (int p = 0; p < dataGridView2.RowCount - 1; p++)
                        //{
                        //    string x1 = dataGridView2.Rows[p].Cells["descripcion"].Value.ToString();
                        //    string y1 = dataGridView2.Rows[p].Cells["total"].Value.ToString();
                        //    Gsemana.Series[0].Points.AddXY(x1, y1);
                        //}
                    }
                    grafica(dias[1], 3);
                    break;
                case 3:
                    Gmes.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
                    if (dataGridView3.RowCount > 0)
                    {
                        int c = 0;
                        while (c < 10 & c < dataGridView3.RowCount - 1)
                        {
                            string x = dataGridView3.Rows[c].Cells["descripcion"].Value.ToString();
                            string y = dataGridView3.Rows[c].Cells["total"].Value.ToString();
                            Gmes.Series[0].Points.AddXY(x, y);
                            c++;
                        }
                        mayor();
                        //for (int p = 0; p < dataGridView3.RowCount - 1; p++)
                        //{
                        //    string x2 = dataGridView3.Rows[p].Cells["descripcion"].Value.ToString();
                        //    string y2 = dataGridView3.Rows[p].Cells["total"].Value.ToString();
                        //    Gmes.Series[0].Points.AddXY(x2, y2);
                        //}
                    }
                    break;
            }
        }

        private void mayor()
        {
            try
            {
                string[] c = { "detalle_venta.id_producto", "productos.descripcion", "ventas.fecha_ven" };
                string[] v = { "ventas", "detalle_venta", "productos" };
                string condition = "and (detalle_venta.id_venta=ventas.id_venta) and detalle_venta.id_producto=productos.id_producto";
                string g = "total desc";
                string f = DateTime.Now.ToString("yyMMdd"), i = DateTime.Now.AddDays(-0).ToString("yyMMdd");
                Sentencias.grafica(i, f, dataGridView4, "ventas", c, "detalle_venta.cantidad", "total", v, condition, g);
                LabelMayor.Text = dataGridView4.Rows[0].Cells[1].Value.ToString();
                //LabelMenor.Text = dataGridView4.Rows[dataGridView4.RowCount - 1].Cells[1].Value.ToString();
            }
            catch (Exception)
            {

            }
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {
            
        }
    }
}
