﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;
using Reef___Punto_de_Venta.Properties;


namespace Reef___Punto_de_Venta
{
    class Sentencias
    {
        private static MyConnection conecta = new MyConnection();
        private static DataSet tht = new DataSet();
        private static BindingSource binding = new BindingSource(); //Enlaces para los controles
        public static string[] productos = { "id_producto", "categoria", "descripcion", "stock", "precio_compra", "precio_venta", "img" };
        public static string[] detalle_ventas = { "id_detalle", "id_venta", "id_producto", "precio_un", "cantidad", "descuento", "importe" };
        public static string[] ventas = { "id_venta", "id_empleado", "fecha_ven", "total" };

        public static void insertar(string tabla, string[] campos, string[] valores)
        {
            try
            {
                conecta.Crear_conexion();
                string inserta = "";
                if (campos.Length == valores.Length)
                {
                    inserta = "INSERT INTO " + tabla + "(";
                    for (int i = 0; i < campos.Length - 1; i++)
                    {
                        inserta += (campos[i] + ",");
                    }
                    inserta += (campos[campos.Length - 1] + ") Values (");
                    for (int i = 0; i < valores.Length - 1; i++)
                    {
                        inserta += (valores[i] + ",");
                    }
                    inserta += (valores[valores.Length - 1] + ");");
                    MySqlCommand pro = new MySqlCommand(inserta);
                    pro.Connection = conecta.getConexion();
                    pro.ExecuteNonQuery(); //Ejecuta la insercción del registro
                    conecta.Cerrar_conexion();
                }
                else
                { MessageBox.Show("No concuerda la cantidad de datos"); }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error algun campo debe contener datos erroneos " + ex.ToString());
            }
        }
        public static void insertar(string tabla, string[] campos, string[] valores, bool bind)
        {
            try
            {
                conecta.Crear_conexion();
                string inserta = "";
                if (campos.Length - 1 == valores.Length)
                {
                    inserta = "INSERT INTO " + tabla + "(";
                    for (int i = 0; i < valores.Length - 1; i++)
                    {
                        inserta += (campos[i] + ",");
                    }
                    inserta += (campos[valores.Length - 1] + ") Values (");
                    for (int i = 0; i < valores.Length - 1; i++)
                    {
                        inserta += (valores[i] + ",");
                    }
                    inserta += (valores[valores.Length - 1] + ");");
                    MySqlCommand pro = new MySqlCommand(inserta);
                    pro.Connection = conecta.getConexion();
                    pro.ExecuteNonQuery(); //Ejecuta la insercción del registro
                    conecta.Cerrar_conexion();
                }
                else
                { MessageBox.Show("No concuerda la cantidad de datos"); }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error algun campo debe contener datos erroneos " + ex.ToString());
            }
        }
        public static void llenartextbox(TextBox[] textboxs, DataGridView d, Button b)
        {
            if (textboxs.Length == d.SelectedRows[0].Cells.Count)
            {
                for (int i = 0; i < textboxs.Length; i++)
                {
                    textboxs[i].Text = d.SelectedRows[0].Cells[i].Value.ToString();
                }
                b.Text = "Terminar Editar";
            }
        }
        public static void llenartextbox(TextBox[] textboxs, DataGridView d, Button b, bool bindin)
        {
            var drv = d.SelectedRows[0].DataBoundItem as DataRowView;
            var row = drv.Row as DataRow;
            if (textboxs.Length == row.ItemArray.Length-1)
            {
                
                for (int i = 0; i < textboxs.Length; i++)
                {
                    textboxs[i].Text = row[i].ToString();
                }
                b.Text = "Terminar Editar";
            }
        }
        public static void modificar(string tabla, string[] campos, string[] valores)
        {
            try
            {
                conecta.Crear_conexion();
                StringBuilder s = new StringBuilder();
                s.Append("update "); s.Append(tabla); s.Append(" set ");
                for (int i = 1; i < valores.Length; i++)
                {
                    s.Append(campos[i]); s.Append("="); s.Append(valores[i]); s.Append(", ");
                }
                s.Append(campos[campos.Length - 1]); s.Append("="); s.Append(valores[valores.Length - 1]);
                s.Append(" where "); s.Append(campos[0]); s.Append("="); s.Append(valores[0]); s.Append(";");
                MySqlCommand revisa = new MySqlCommand(s.ToString());
                revisa.Connection = conecta.getConexion();
                revisa.ExecuteNonQuery();
                MessageBox.Show("Registro  Modificado", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            { }
        }
        public static void modificar(string tabla, string[] campos, string[] valores, bool bindin)
        {
            try
            {
                conecta.Crear_conexion();
                StringBuilder s = new StringBuilder();
                s.Append("update "); s.Append(tabla); s.Append(" set ");
                for (int i = 1; i < valores.Length; i++)
                {
                    s.Append(campos[i]); s.Append("="); s.Append(valores[i]); s.Append(", ");
                }
                s.Append(campos[campos.Length - 2]); s.Append("="); s.Append(valores[valores.Length - 1]);
                s.Append(" where "); s.Append(campos[0]); s.Append("="); s.Append(valores[0]); s.Append(";");
                MySqlCommand revisa = new MySqlCommand(s.ToString());
                revisa.Connection = conecta.getConexion();
                revisa.ExecuteNonQuery();
                MessageBox.Show("Registro  Modificado", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            { }
        }
        public static void eliminar(DataGridView d, string tabla, string[] campos)
        {
            if (d.SelectedRows.Count > 0)
            {
                conecta.Crear_conexion();
                string eliminacion = "DELETE FROM " + tabla + " WHERE " + campos[0] + "=" + d.SelectedRows[0].Cells[0].Value.ToString() + ";";
                MySqlCommand chec = new MySqlCommand(eliminacion, conecta.getConexion());
                chec.Connection = conecta.getConexion();
                chec.ExecuteNonQuery();
                conecta.Cerrar_conexion();
                MessageBox.Show("Registro eliminado", "Elminación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Error primero debe seleccionar un registro");
            }
        }
        public static void llenar(DataGridView d,PictureBox p, string tabla, string[] campos,bool bindin, string bind, string[] header)
        {
            d.DataBindings.Clear();
            p.DataBindings.Clear();
            StringBuilder s = new StringBuilder();
            s.Append("select ");
            for (int i = 0; i < campos.Length - 1; i++)
            {
                s.Append(campos[i]);
                s.Append(" as '");
                s.Append(header[i]);
                s.Append("',");
            }
            s.Append(campos[campos.Length - 1]);
            s.Append(" from ");
            s.Append(tabla);
            s.Append(";");
            conecta.Crear_conexion();
            MySqlCommand buscarproductos = new MySqlCommand(s.ToString(), conecta.getConexion());
            MySqlDataAdapter cmc = new MySqlDataAdapter(buscarproductos);
            DataSet tht = new DataSet();
            cmc.Fill(tht, tabla);
            if (bindin == true)
            {
                binding.DataSource = tht.Tables[tabla].DefaultView;
                //Enlaza el datagrind
                d.DataSource = binding;
                d.Columns[bind].Visible = false;
                //Enlaza los textbox con los campos
                p.DataBindings.Add("Image", binding, bind, true);
                conecta.Cerrar_conexion();
            }
            
        }
        public static void llenar(DataGridView d, string tabla, string[] campos)
        {
            StringBuilder s = new StringBuilder();
            s.Append("select ");
            for (int i = 0; i < campos.Length - 1; i++)
            {
                s.Append(campos[i]);
                s.Append(",");
            }
            s.Append(campos[campos.Length - 1]);
            s.Append(" from ");
            s.Append(tabla);
            s.Append(";");
            conecta.Crear_conexion();
            MySqlCommand buscarproductos = new MySqlCommand(s.ToString(), conecta.getConexion());
            MySqlDataAdapter cmc = new MySqlDataAdapter(buscarproductos);
            DataSet tht = new DataSet();
            cmc.Fill(tht, tabla);
            d.DataSource = tht.Tables[tabla].DefaultView;
        }
        public static void llenar(DataGridView d, string tabla, string[] campos,string[] As)
        {
            StringBuilder s = new StringBuilder();
            s.Append("select ");
            for (int i = 0; i < campos.Length - 1; i++)
            {
                s.Append(campos[i]);
                s.Append(" as ");
                s.Append(As[i]);
                s.Append(",");
            }
            s.Append(campos[campos.Length - 1]);
            s.Append(" as ");
            s.Append(As[campos.Length-1]);
            s.Append(" from ");
            s.Append(tabla);
            s.Append(";");
            conecta.Crear_conexion();
            MySqlCommand buscarproductos = new MySqlCommand(s.ToString(), conecta.getConexion());
            MySqlDataAdapter cmc = new MySqlDataAdapter(buscarproductos);
            DataSet tht = new DataSet();
            cmc.Fill(tht, tabla);
            d.DataSource = tht.Tables[tabla].DefaultView;
        }
        public static void limpiar(TextBox[] textboxs)
        {
            foreach (TextBox t in textboxs)
            {
                t.Text = "";
            }
        }
        public static void buscar(string tabla, DataGridView d, string[] campos, string[] camposbusca, string[] valores)
        {
            try
            {
                //Buscar registros
                int contar_filas;
                conecta.Crear_conexion();
                string search3 = "select ";
                for (int i = 0; i < campos.Length - 1; i++)
                {
                    search3 += (campos[i] + ",");
                }
                search3 += campos[campos.Length - 1];
                search3 += (" from " + tabla + " where ");
                for (int i = 0; i < camposbusca.Length - 1; i++)
                {
                    search3 += (camposbusca[i] + " like " + "'%" + valores[i] + "%' and ");
                }
                search3 += (camposbusca[camposbusca.Length - 1] + " like " + "'%" + valores[valores.Length - 1] + "%';");
                MySqlCommand busca = new MySqlCommand(search3, conecta.getConexion());
                MySqlDataAdapter cmc = new MySqlDataAdapter(busca);
                DataSet tht = new DataSet();
                busca.Connection = conecta.getConexion();
                cmc.Fill(tht, tabla);
                d.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
                d.DataSource = tht.Tables[tabla].DefaultView;
                contar_filas = d.RowCount;
                if (contar_filas > 1)
                {
                    MessageBox.Show("Búsqueda exitosa");
                    MySqlDataReader leer = busca.ExecuteReader();
                }
                else
                {
                    MessageBox.Show("No existe el registro solicitado");
                }
            }
            catch (Exception)
            {

                MessageBox.Show("No se cupo completar la acción, re-intente, bye...");
            }
        }
        public static void buscar(string tabla, DataGridView d,PictureBox p, string[] campos, string[] camposbusca, string[] valores, bool bindin, string bind)
        {
            try
            {
                d.DataBindings.Clear();
                p.DataBindings.Clear();
                //Buscar registros
                int contar_filas;
                conecta.Crear_conexion();
                string search3 = "select ";
                for (int i = 0; i < campos.Length - 1; i++)
                {
                    search3 += (campos[i] + ",");
                }
                search3 += campos[campos.Length - 1];
                search3 += (" from " + tabla + " where ");
                for (int i = 0; i < camposbusca.Length - 1; i++)
                {
                    search3 += (camposbusca[i] + " like " + "'%" + valores[i] + "%' and ");
                }
                search3 += (camposbusca[camposbusca.Length - 1] + " like " + "'%" + valores[valores.Length - 1] + "%';");
                MySqlCommand busca = new MySqlCommand(search3, conecta.getConexion());
                MySqlDataAdapter cmc = new MySqlDataAdapter(busca);
                DataSet tht = new DataSet();
                busca.Connection = conecta.getConexion();
                cmc.Fill(tht, tabla);
                if (bindin == true)
                {
                    binding.DataSource = tht.Tables[tabla].DefaultView;
                    //Enlaza el datagrind
                    d.DataSource = binding;
                    d.Columns[bind].Visible = false;
                    //Enlaza los textbox con los campos
                    p.DataBindings.Add("Image", binding, bind, true);
                    conecta.Cerrar_conexion();
                }
                contar_filas = d.RowCount;
                if (contar_filas > 0)
                {
                    MessageBox.Show("Búsqueda exitosa");
                }
                else
                {
                    MessageBox.Show("No existe el registro solicitado");
                    p.Image = Resources.ImageNull;
                }
            }
            catch (Exception)
            {

                MessageBox.Show("No se pudo realizar la busqueda debe haber un error en sus campos");
            }
        }
        public static string max(string tabla, string campo)
        {
            try
            {
                StringBuilder s = new StringBuilder();
                s.Append("select max(");
                s.Append(campo);
                s.Append(") from ");
                s.Append(tabla);
                s.Append(";");
                conecta.Crear_conexion();
                MySqlCommand buscarproductos = new MySqlCommand(s.ToString(), conecta.getConexion());
                string valor = buscarproductos.ExecuteScalar().ToString();
                if (valor == null || valor == "")
                {
                    return "0";
                }
                return valor;
            }
            catch(Exception ex) { return "0"; }
        }
        public static void Vender(string tabla, string campo1, string valor1,string campocom,string valorcom)
        {
            try
            {
                conecta.Crear_conexion();
                string s = string.Format("update {0} set {1}=({1}-{2}) where {3}={4};",tabla,campo1,valor1,campocom,valorcom);
                MySqlCommand revisa = new MySqlCommand(s.ToString());
                revisa.Connection = conecta.getConexion();
                revisa.ExecuteNonQuery();
                MessageBox.Show("Registro  Modificado", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            { }
        }
                
        public static void grafica(string i, string f, DataGridView d, string tabla, string[] campos, string camposum, string nomres, string[] tablas, string condition, string group)
        {
            StringBuilder c = new StringBuilder();
            int u = 0;
            c.Append("select ");
            for (int j = 0; j < campos.Length; j++)
            {
                c.Append(campos[j]);
                c.Append(", ");
            }
            c.Append("sum(");
            c.Append(camposum);
            c.Append(") as ");
            c.Append(nomres);
            c.Append(" from ");
            for (int j = 0; j < tablas.Length - 1; j++)
            {
                c.Append(tablas[j]);
                c.Append(", ");
                u = j;
            }
            c.Append(tablas[u + 1]);
            c.Append(" where (ventas.fecha_ven between ");
            c.Append(i);
            c.Append(" and ");
            c.Append(f);
            c.Append(") ");
            c.Append(condition);
            c.Append(" group by ");
            c.Append(group);
            c.Append(";");            
            conecta.Crear_conexion();
            MySqlCommand buscarproductos = new MySqlCommand(c.ToString(), conecta.getConexion());
            MySqlDataAdapter cmc = new MySqlDataAdapter(buscarproductos);
            DataSet tht = new DataSet();
            cmc.Fill(tht, tabla);
            d.DataSource = tht.Tables[tabla].DefaultView;
        }

        //Luis
        public static void sentenciax(string fi, string ff, string tabla, string[] campos, string[] tablas, string condition, DataGridView d)
        {
            StringBuilder c = new StringBuilder();
            int u = 0;
            c.Append("select ");
            for (int j = 0; j < campos.Length - 1; j++)
            {
                c.Append(campos[j]);
                c.Append(", ");
            }
            c.Append(campos[campos.Length - 1]);
            c.Append(" from ");
            for (int j = 0; j < tablas.Length - 1; j++)
            {
                c.Append(tablas[j]);
                c.Append(", ");
                u = j;
            }
            c.Append(tablas[u + 1]);
            c.Append(" where (ventas.fecha_ven between ");
            c.Append(fi);
            c.Append(" and ");
            c.Append(ff);
            c.Append(") ");
            c.Append(condition);
            c.Append(";");
            conecta.Crear_conexion();
            MySqlCommand buscarproductos = new MySqlCommand(c.ToString(), conecta.getConexion());
            MySqlDataAdapter cmc = new MySqlDataAdapter(buscarproductos);
            DataSet tht = new DataSet();
            cmc.Fill(tht, tabla);
            d.DataSource = tht.Tables[tabla].DefaultView;
        }
        public static void buscano(string tabla, DataGridView d, string condicion)
        {
            StringBuilder c = new StringBuilder();
            c.Append("select * from ");
            c.Append(tabla);
            c.Append(" " + condicion);
            conecta.Crear_conexion();
            MySqlCommand buscarproductos = new MySqlCommand(c.ToString(), conecta.getConexion());
            MySqlDataAdapter cmc = new MySqlDataAdapter(buscarproductos);
            DataSet tht = new DataSet();
            cmc.Fill(tht, tabla);
            d.DataSource = tht.Tables[tabla].DefaultView;
        }
    }
}
