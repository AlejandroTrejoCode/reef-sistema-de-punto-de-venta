﻿namespace Reef___Punto_de_Venta.Vistas
{
    partial class Analisis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Analisis));
            this.HeaderVender = new System.Windows.Forms.Panel();
            this.Minimiza = new System.Windows.Forms.Button();
            this.LabelResumen = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Vdia = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.VSemanal = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.VMensual = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.PMensual = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Psemanal = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Pdiario = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.dataGridView5 = new System.Windows.Forms.DataGridView();
            this.dataGridView6 = new System.Windows.Forms.DataGridView();
            this.label7 = new System.Windows.Forms.Label();
            this.HeaderVender.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Vdia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VSemanal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VMensual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PMensual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Psemanal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Pdiario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).BeginInit();
            this.SuspendLayout();
            // 
            // HeaderVender
            // 
            this.HeaderVender.BackColor = System.Drawing.Color.White;
            this.HeaderVender.Controls.Add(this.Minimiza);
            this.HeaderVender.Controls.Add(this.LabelResumen);
            this.HeaderVender.Location = new System.Drawing.Point(0, 0);
            this.HeaderVender.Name = "HeaderVender";
            this.HeaderVender.Size = new System.Drawing.Size(1210, 50);
            this.HeaderVender.TabIndex = 3;
            this.HeaderVender.Paint += new System.Windows.Forms.PaintEventHandler(this.HeaderVender_Paint);
            // 
            // Minimiza
            // 
            this.Minimiza.BackgroundImage = global::Reef___Punto_de_Venta.Properties.Resources.Cerrar1;
            this.Minimiza.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Minimiza.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Minimiza.ForeColor = System.Drawing.Color.PowderBlue;
            this.Minimiza.Location = new System.Drawing.Point(1187, 0);
            this.Minimiza.Name = "Minimiza";
            this.Minimiza.Size = new System.Drawing.Size(20, 20);
            this.Minimiza.TabIndex = 3;
            this.Minimiza.UseVisualStyleBackColor = true;
            this.Minimiza.Click += new System.EventHandler(this.Minimiza_Click);
            // 
            // LabelResumen
            // 
            this.LabelResumen.AutoSize = true;
            this.LabelResumen.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelResumen.ForeColor = System.Drawing.Color.DimGray;
            this.LabelResumen.Location = new System.Drawing.Point(12, 6);
            this.LabelResumen.Name = "LabelResumen";
            this.LabelResumen.Size = new System.Drawing.Size(254, 38);
            this.LabelResumen.TabIndex = 1;
            this.LabelResumen.Text = "Inicio > Analisis";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DimGray;
            this.label1.Location = new System.Drawing.Point(87, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(245, 38);
            this.label1.TabIndex = 27;
            this.label1.Text = "Ventas - Diario";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DimGray;
            this.label2.Location = new System.Drawing.Point(505, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(243, 38);
            this.label2.TabIndex = 28;
            this.label2.Text = "Ventas - Semanal";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.DimGray;
            this.label3.Location = new System.Drawing.Point(912, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(243, 38);
            this.label3.TabIndex = 29;
            this.label3.Text = "Ventas - Mensual";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.DimGray;
            this.label4.Location = new System.Drawing.Point(14, 452);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(229, 38);
            this.label4.TabIndex = 30;
            this.label4.Text = "Productos - Diario";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.DimGray;
            this.label5.Location = new System.Drawing.Point(472, 607);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(262, 38);
            this.label5.TabIndex = 31;
            this.label5.Text = "Productos - Semanal";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.DimGray;
            this.label6.Location = new System.Drawing.Point(893, 607);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(262, 38);
            this.label6.TabIndex = 32;
            this.label6.Text = "Productos - Mensual";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Vdia
            // 
            this.Vdia.BackColor = System.Drawing.Color.Transparent;
            this.Vdia.BorderlineWidth = 0;
            chartArea1.Area3DStyle.Inclination = 1;
            chartArea1.AxisX.LineColor = System.Drawing.Color.Gray;
            chartArea1.AxisX.LineWidth = 2;
            chartArea1.AxisX.MajorGrid.LineColor = System.Drawing.Color.Transparent;
            chartArea1.AxisX.MajorTickMark.LineColor = System.Drawing.Color.Transparent;
            chartArea1.AxisY.LineColor = System.Drawing.Color.Gray;
            chartArea1.AxisY.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea1.AxisY.LineWidth = 2;
            chartArea1.AxisY.MajorGrid.LineColor = System.Drawing.Color.Transparent;
            chartArea1.BorderColor = System.Drawing.Color.DarkRed;
            chartArea1.Name = "ChartArea1";
            this.Vdia.ChartAreas.Add(chartArea1);
            legend1.Alignment = System.Drawing.StringAlignment.Center;
            legend1.BorderWidth = 0;
            legend1.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend1.Enabled = false;
            legend1.Font = new System.Drawing.Font("Century Gothic", 1.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend1.ForeColor = System.Drawing.Color.DimGray;
            legend1.HeaderSeparatorColor = System.Drawing.Color.DimGray;
            legend1.IsTextAutoFit = false;
            legend1.ItemColumnSeparatorColor = System.Drawing.Color.DimGray;
            legend1.Name = "Legend1";
            legend1.ShadowColor = System.Drawing.Color.White;
            legend1.Title = "Ventas Diarias";
            legend1.TitleFont = new System.Drawing.Font("Century Gothic", 17.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend1.TitleForeColor = System.Drawing.Color.DimGray;
            legend1.TitleSeparator = System.Windows.Forms.DataVisualization.Charting.LegendSeparatorStyle.GradientLine;
            legend1.TitleSeparatorColor = System.Drawing.Color.DimGray;
            this.Vdia.Legends.Add(legend1);
            this.Vdia.Location = new System.Drawing.Point(12, 374);
            this.Vdia.Name = "Vdia";
            this.Vdia.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this.Vdia.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(200)))), ((int)(((byte)(163)))))};
            series1.ChartArea = "ChartArea1";
            series1.CustomProperties = "PointWidth=0.9";
            series1.EmptyPointStyle.LabelForeColor = System.Drawing.Color.DimGray;
            series1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series1.LabelBorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            series1.LabelBorderWidth = 0;
            series1.LabelForeColor = System.Drawing.Color.DimGray;
            series1.Legend = "Legend1";
            series1.MarkerBorderWidth = 0;
            series1.Name = "Series1";
            series1.ShadowColor = System.Drawing.Color.Red;
            series1.SmartLabelStyle.AllowOutsidePlotArea = System.Windows.Forms.DataVisualization.Charting.LabelOutsidePlotAreaStyle.No;
            series1.SmartLabelStyle.CalloutLineColor = System.Drawing.Color.White;
            series1.SmartLabelStyle.CalloutLineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            series1.SmartLabelStyle.CalloutLineWidth = 0;
            series1.SmartLabelStyle.CalloutStyle = System.Windows.Forms.DataVisualization.Charting.LabelCalloutStyle.None;
            series1.SmartLabelStyle.IsOverlappedHidden = false;
            series1.YValuesPerPoint = 4;
            this.Vdia.Series.Add(series1);
            this.Vdia.Size = new System.Drawing.Size(391, 240);
            this.Vdia.TabIndex = 33;
            // 
            // VSemanal
            // 
            this.VSemanal.BackColor = System.Drawing.Color.Transparent;
            this.VSemanal.BorderlineWidth = 0;
            chartArea2.Area3DStyle.Inclination = 1;
            chartArea2.AxisX.LineColor = System.Drawing.Color.Gray;
            chartArea2.AxisX.LineWidth = 2;
            chartArea2.AxisX.MajorGrid.LineColor = System.Drawing.Color.Transparent;
            chartArea2.AxisX.MajorTickMark.LineColor = System.Drawing.Color.Transparent;
            chartArea2.AxisY.LineColor = System.Drawing.Color.Gray;
            chartArea2.AxisY.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea2.AxisY.LineWidth = 2;
            chartArea2.AxisY.MajorGrid.LineColor = System.Drawing.Color.Transparent;
            chartArea2.BorderColor = System.Drawing.Color.DarkRed;
            chartArea2.Name = "ChartArea1";
            this.VSemanal.ChartAreas.Add(chartArea2);
            legend2.Alignment = System.Drawing.StringAlignment.Center;
            legend2.BorderWidth = 0;
            legend2.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend2.Enabled = false;
            legend2.Font = new System.Drawing.Font("Century Gothic", 1.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend2.ForeColor = System.Drawing.Color.DimGray;
            legend2.HeaderSeparatorColor = System.Drawing.Color.DimGray;
            legend2.IsTextAutoFit = false;
            legend2.ItemColumnSeparatorColor = System.Drawing.Color.DimGray;
            legend2.Name = "Legend1";
            legend2.ShadowColor = System.Drawing.Color.White;
            legend2.Title = "Ventas Diarias";
            legend2.TitleFont = new System.Drawing.Font("Century Gothic", 17.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend2.TitleForeColor = System.Drawing.Color.DimGray;
            legend2.TitleSeparator = System.Windows.Forms.DataVisualization.Charting.LegendSeparatorStyle.GradientLine;
            legend2.TitleSeparatorColor = System.Drawing.Color.DimGray;
            this.VSemanal.Legends.Add(legend2);
            this.VSemanal.Location = new System.Drawing.Point(422, 374);
            this.VSemanal.Name = "VSemanal";
            this.VSemanal.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this.VSemanal.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(200)))), ((int)(((byte)(163)))))};
            series2.ChartArea = "ChartArea1";
            series2.CustomProperties = "PointWidth=0.9";
            series2.EmptyPointStyle.LabelForeColor = System.Drawing.Color.DimGray;
            series2.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series2.LabelBorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            series2.LabelBorderWidth = 0;
            series2.LabelForeColor = System.Drawing.Color.DimGray;
            series2.Legend = "Legend1";
            series2.MarkerBorderWidth = 0;
            series2.Name = "Series1";
            series2.ShadowColor = System.Drawing.Color.Red;
            series2.SmartLabelStyle.AllowOutsidePlotArea = System.Windows.Forms.DataVisualization.Charting.LabelOutsidePlotAreaStyle.No;
            series2.SmartLabelStyle.CalloutLineColor = System.Drawing.Color.White;
            series2.SmartLabelStyle.CalloutLineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            series2.SmartLabelStyle.CalloutLineWidth = 0;
            series2.SmartLabelStyle.CalloutStyle = System.Windows.Forms.DataVisualization.Charting.LabelCalloutStyle.None;
            series2.SmartLabelStyle.IsOverlappedHidden = false;
            series2.YValuesPerPoint = 4;
            this.VSemanal.Series.Add(series2);
            this.VSemanal.Size = new System.Drawing.Size(395, 240);
            this.VSemanal.TabIndex = 34;
            // 
            // VMensual
            // 
            this.VMensual.BackColor = System.Drawing.Color.Transparent;
            this.VMensual.BorderlineWidth = 0;
            chartArea3.Area3DStyle.Inclination = 1;
            chartArea3.AxisX.LineColor = System.Drawing.Color.Gray;
            chartArea3.AxisX.LineWidth = 2;
            chartArea3.AxisX.MajorGrid.LineColor = System.Drawing.Color.Transparent;
            chartArea3.AxisX.MajorTickMark.LineColor = System.Drawing.Color.Transparent;
            chartArea3.AxisY.LineColor = System.Drawing.Color.Gray;
            chartArea3.AxisY.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea3.AxisY.LineWidth = 2;
            chartArea3.AxisY.MajorGrid.LineColor = System.Drawing.Color.Transparent;
            chartArea3.BorderColor = System.Drawing.Color.DarkRed;
            chartArea3.Name = "ChartArea1";
            this.VMensual.ChartAreas.Add(chartArea3);
            legend3.Alignment = System.Drawing.StringAlignment.Center;
            legend3.BorderWidth = 0;
            legend3.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend3.Enabled = false;
            legend3.Font = new System.Drawing.Font("Century Gothic", 1.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend3.ForeColor = System.Drawing.Color.DimGray;
            legend3.HeaderSeparatorColor = System.Drawing.Color.DimGray;
            legend3.IsTextAutoFit = false;
            legend3.ItemColumnSeparatorColor = System.Drawing.Color.DimGray;
            legend3.Name = "Legend1";
            legend3.ShadowColor = System.Drawing.Color.White;
            legend3.Title = "Ventas Diarias";
            legend3.TitleFont = new System.Drawing.Font("Century Gothic", 17.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend3.TitleForeColor = System.Drawing.Color.DimGray;
            legend3.TitleSeparator = System.Windows.Forms.DataVisualization.Charting.LegendSeparatorStyle.GradientLine;
            legend3.TitleSeparatorColor = System.Drawing.Color.DimGray;
            this.VMensual.Legends.Add(legend3);
            this.VMensual.Location = new System.Drawing.Point(823, 374);
            this.VMensual.Name = "VMensual";
            this.VMensual.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this.VMensual.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(200)))), ((int)(((byte)(163)))))};
            series3.ChartArea = "ChartArea1";
            series3.CustomProperties = "PointWidth=0.9";
            series3.EmptyPointStyle.LabelForeColor = System.Drawing.Color.DimGray;
            series3.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series3.LabelBorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            series3.LabelBorderWidth = 0;
            series3.LabelForeColor = System.Drawing.Color.DimGray;
            series3.Legend = "Legend1";
            series3.MarkerBorderWidth = 0;
            series3.Name = "Series1";
            series3.ShadowColor = System.Drawing.Color.Red;
            series3.SmartLabelStyle.AllowOutsidePlotArea = System.Windows.Forms.DataVisualization.Charting.LabelOutsidePlotAreaStyle.No;
            series3.SmartLabelStyle.CalloutLineColor = System.Drawing.Color.White;
            series3.SmartLabelStyle.CalloutLineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            series3.SmartLabelStyle.CalloutLineWidth = 0;
            series3.SmartLabelStyle.CalloutStyle = System.Windows.Forms.DataVisualization.Charting.LabelCalloutStyle.None;
            series3.SmartLabelStyle.IsOverlappedHidden = false;
            series3.YValuesPerPoint = 4;
            this.VMensual.Series.Add(series3);
            this.VMensual.Size = new System.Drawing.Size(375, 240);
            this.VMensual.TabIndex = 35;
            // 
            // PMensual
            // 
            this.PMensual.BackColor = System.Drawing.Color.Transparent;
            this.PMensual.BorderlineWidth = 0;
            chartArea4.Area3DStyle.Inclination = 1;
            chartArea4.AxisX.LineColor = System.Drawing.Color.Gray;
            chartArea4.AxisX.LineWidth = 2;
            chartArea4.AxisX.MajorGrid.LineColor = System.Drawing.Color.Transparent;
            chartArea4.AxisX.MajorTickMark.LineColor = System.Drawing.Color.Transparent;
            chartArea4.AxisY.LineColor = System.Drawing.Color.Gray;
            chartArea4.AxisY.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea4.AxisY.LineWidth = 2;
            chartArea4.AxisY.MajorGrid.LineColor = System.Drawing.Color.Transparent;
            chartArea4.BorderColor = System.Drawing.Color.DarkRed;
            chartArea4.Name = "ChartArea1";
            this.PMensual.ChartAreas.Add(chartArea4);
            legend4.Alignment = System.Drawing.StringAlignment.Center;
            legend4.BorderWidth = 0;
            legend4.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend4.Enabled = false;
            legend4.Font = new System.Drawing.Font("Century Gothic", 1.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend4.ForeColor = System.Drawing.Color.DimGray;
            legend4.HeaderSeparatorColor = System.Drawing.Color.DimGray;
            legend4.IsTextAutoFit = false;
            legend4.ItemColumnSeparatorColor = System.Drawing.Color.DimGray;
            legend4.Name = "Legend1";
            legend4.ShadowColor = System.Drawing.Color.White;
            legend4.Title = "Ventas Diarias";
            legend4.TitleFont = new System.Drawing.Font("Century Gothic", 17.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend4.TitleForeColor = System.Drawing.Color.DimGray;
            legend4.TitleSeparator = System.Windows.Forms.DataVisualization.Charting.LegendSeparatorStyle.GradientLine;
            legend4.TitleSeparatorColor = System.Drawing.Color.DimGray;
            this.PMensual.Legends.Add(legend4);
            this.PMensual.Location = new System.Drawing.Point(836, 114);
            this.PMensual.Name = "PMensual";
            this.PMensual.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this.PMensual.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(200)))), ((int)(((byte)(163)))))};
            series4.ChartArea = "ChartArea1";
            series4.CustomProperties = "PointWidth=0.9";
            series4.EmptyPointStyle.LabelForeColor = System.Drawing.Color.DimGray;
            series4.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series4.LabelBorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            series4.LabelBorderWidth = 0;
            series4.LabelForeColor = System.Drawing.Color.DimGray;
            series4.Legend = "Legend1";
            series4.MarkerBorderWidth = 0;
            series4.Name = "Series1";
            series4.ShadowColor = System.Drawing.Color.Red;
            series4.SmartLabelStyle.AllowOutsidePlotArea = System.Windows.Forms.DataVisualization.Charting.LabelOutsidePlotAreaStyle.No;
            series4.SmartLabelStyle.CalloutLineColor = System.Drawing.Color.White;
            series4.SmartLabelStyle.CalloutLineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            series4.SmartLabelStyle.CalloutLineWidth = 0;
            series4.SmartLabelStyle.CalloutStyle = System.Windows.Forms.DataVisualization.Charting.LabelCalloutStyle.None;
            series4.SmartLabelStyle.IsOverlappedHidden = false;
            series4.YValuesPerPoint = 4;
            this.PMensual.Series.Add(series4);
            this.PMensual.Size = new System.Drawing.Size(348, 240);
            this.PMensual.TabIndex = 36;
            // 
            // Psemanal
            // 
            this.Psemanal.BackColor = System.Drawing.Color.Transparent;
            this.Psemanal.BorderlineWidth = 0;
            chartArea5.Area3DStyle.Inclination = 1;
            chartArea5.AxisX.LineColor = System.Drawing.Color.Gray;
            chartArea5.AxisX.LineWidth = 2;
            chartArea5.AxisX.MajorGrid.LineColor = System.Drawing.Color.Transparent;
            chartArea5.AxisX.MajorTickMark.LineColor = System.Drawing.Color.Transparent;
            chartArea5.AxisY.LineColor = System.Drawing.Color.Gray;
            chartArea5.AxisY.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea5.AxisY.LineWidth = 2;
            chartArea5.AxisY.MajorGrid.LineColor = System.Drawing.Color.Transparent;
            chartArea5.BorderColor = System.Drawing.Color.DarkRed;
            chartArea5.Name = "ChartArea1";
            this.Psemanal.ChartAreas.Add(chartArea5);
            legend5.Alignment = System.Drawing.StringAlignment.Center;
            legend5.BorderWidth = 0;
            legend5.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend5.Enabled = false;
            legend5.Font = new System.Drawing.Font("Century Gothic", 1.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend5.ForeColor = System.Drawing.Color.DimGray;
            legend5.HeaderSeparatorColor = System.Drawing.Color.DimGray;
            legend5.IsTextAutoFit = false;
            legend5.ItemColumnSeparatorColor = System.Drawing.Color.DimGray;
            legend5.Name = "Legend1";
            legend5.ShadowColor = System.Drawing.Color.White;
            legend5.Title = "Ventas Diarias";
            legend5.TitleFont = new System.Drawing.Font("Century Gothic", 17.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend5.TitleForeColor = System.Drawing.Color.DimGray;
            legend5.TitleSeparator = System.Windows.Forms.DataVisualization.Charting.LegendSeparatorStyle.GradientLine;
            legend5.TitleSeparatorColor = System.Drawing.Color.DimGray;
            this.Psemanal.Legends.Add(legend5);
            this.Psemanal.Location = new System.Drawing.Point(422, 114);
            this.Psemanal.Name = "Psemanal";
            this.Psemanal.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this.Psemanal.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(200)))), ((int)(((byte)(163)))))};
            series5.ChartArea = "ChartArea1";
            series5.CustomProperties = "PointWidth=0.9";
            series5.EmptyPointStyle.LabelForeColor = System.Drawing.Color.DimGray;
            series5.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series5.LabelBorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            series5.LabelBorderWidth = 0;
            series5.LabelForeColor = System.Drawing.Color.DimGray;
            series5.Legend = "Legend1";
            series5.MarkerBorderWidth = 0;
            series5.Name = "Series1";
            series5.ShadowColor = System.Drawing.Color.Red;
            series5.SmartLabelStyle.AllowOutsidePlotArea = System.Windows.Forms.DataVisualization.Charting.LabelOutsidePlotAreaStyle.No;
            series5.SmartLabelStyle.CalloutLineColor = System.Drawing.Color.White;
            series5.SmartLabelStyle.CalloutLineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            series5.SmartLabelStyle.CalloutLineWidth = 0;
            series5.SmartLabelStyle.CalloutStyle = System.Windows.Forms.DataVisualization.Charting.LabelCalloutStyle.None;
            series5.SmartLabelStyle.IsOverlappedHidden = false;
            series5.YValuesPerPoint = 4;
            this.Psemanal.Series.Add(series5);
            this.Psemanal.Size = new System.Drawing.Size(395, 240);
            this.Psemanal.TabIndex = 37;
            // 
            // Pdiario
            // 
            this.Pdiario.BackColor = System.Drawing.Color.Transparent;
            this.Pdiario.BorderlineWidth = 0;
            chartArea6.Area3DStyle.Inclination = 1;
            chartArea6.AxisX.LineColor = System.Drawing.Color.Gray;
            chartArea6.AxisX.LineWidth = 2;
            chartArea6.AxisX.MajorGrid.LineColor = System.Drawing.Color.Transparent;
            chartArea6.AxisX.MajorTickMark.LineColor = System.Drawing.Color.Transparent;
            chartArea6.AxisY.LineColor = System.Drawing.Color.Gray;
            chartArea6.AxisY.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea6.AxisY.LineWidth = 2;
            chartArea6.AxisY.MajorGrid.LineColor = System.Drawing.Color.Transparent;
            chartArea6.BorderColor = System.Drawing.Color.DarkRed;
            chartArea6.Name = "ChartArea1";
            this.Pdiario.ChartAreas.Add(chartArea6);
            legend6.Alignment = System.Drawing.StringAlignment.Center;
            legend6.BorderWidth = 0;
            legend6.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend6.Enabled = false;
            legend6.Font = new System.Drawing.Font("Century Gothic", 1.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend6.ForeColor = System.Drawing.Color.DimGray;
            legend6.HeaderSeparatorColor = System.Drawing.Color.DimGray;
            legend6.IsTextAutoFit = false;
            legend6.ItemColumnSeparatorColor = System.Drawing.Color.DimGray;
            legend6.Name = "Legend1";
            legend6.ShadowColor = System.Drawing.Color.White;
            legend6.Title = "Ventas Diarias";
            legend6.TitleFont = new System.Drawing.Font("Century Gothic", 17.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend6.TitleForeColor = System.Drawing.Color.DimGray;
            legend6.TitleSeparator = System.Windows.Forms.DataVisualization.Charting.LegendSeparatorStyle.GradientLine;
            legend6.TitleSeparatorColor = System.Drawing.Color.DimGray;
            this.Pdiario.Legends.Add(legend6);
            this.Pdiario.Location = new System.Drawing.Point(19, 114);
            this.Pdiario.Name = "Pdiario";
            this.Pdiario.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this.Pdiario.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(200)))), ((int)(((byte)(163)))))};
            series6.ChartArea = "ChartArea1";
            series6.CustomProperties = "PointWidth=0.9";
            series6.EmptyPointStyle.LabelForeColor = System.Drawing.Color.DimGray;
            series6.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series6.LabelBorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            series6.LabelBorderWidth = 0;
            series6.LabelForeColor = System.Drawing.Color.DimGray;
            series6.Legend = "Legend1";
            series6.MarkerBorderWidth = 0;
            series6.Name = "Series1";
            series6.ShadowColor = System.Drawing.Color.Red;
            series6.SmartLabelStyle.AllowOutsidePlotArea = System.Windows.Forms.DataVisualization.Charting.LabelOutsidePlotAreaStyle.No;
            series6.SmartLabelStyle.CalloutLineColor = System.Drawing.Color.White;
            series6.SmartLabelStyle.CalloutLineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            series6.SmartLabelStyle.CalloutLineWidth = 0;
            series6.SmartLabelStyle.CalloutStyle = System.Windows.Forms.DataVisualization.Charting.LabelCalloutStyle.None;
            series6.SmartLabelStyle.IsOverlappedHidden = false;
            series6.YValuesPerPoint = 4;
            this.Pdiario.Series.Add(series6);
            this.Pdiario.Size = new System.Drawing.Size(384, 240);
            this.Pdiario.TabIndex = 38;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(92, 426);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(240, 150);
            this.dataGridView1.TabIndex = 39;
            this.dataGridView1.Visible = false;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(510, 426);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(240, 150);
            this.dataGridView2.TabIndex = 40;
            this.dataGridView2.Visible = false;
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(915, 426);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(240, 150);
            this.dataGridView3.TabIndex = 41;
            this.dataGridView3.Visible = false;
            // 
            // dataGridView4
            // 
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Location = new System.Drawing.Point(92, 165);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.Size = new System.Drawing.Size(240, 150);
            this.dataGridView4.TabIndex = 42;
            this.dataGridView4.Visible = false;
            // 
            // dataGridView5
            // 
            this.dataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView5.Location = new System.Drawing.Point(510, 165);
            this.dataGridView5.Name = "dataGridView5";
            this.dataGridView5.Size = new System.Drawing.Size(240, 150);
            this.dataGridView5.TabIndex = 43;
            this.dataGridView5.Visible = false;
            // 
            // dataGridView6
            // 
            this.dataGridView6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView6.Location = new System.Drawing.Point(915, 165);
            this.dataGridView6.Name = "dataGridView6";
            this.dataGridView6.Size = new System.Drawing.Size(240, 150);
            this.dataGridView6.TabIndex = 44;
            this.dataGridView6.Visible = false;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.DimGray;
            this.label7.Location = new System.Drawing.Point(87, 607);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(262, 38);
            this.label7.TabIndex = 45;
            this.label7.Text = "Productos - Diario";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Analisis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1210, 705);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.dataGridView6);
            this.Controls.Add(this.dataGridView5);
            this.Controls.Add(this.dataGridView4);
            this.Controls.Add(this.dataGridView3);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.Pdiario);
            this.Controls.Add(this.Psemanal);
            this.Controls.Add(this.PMensual);
            this.Controls.Add(this.VMensual);
            this.Controls.Add(this.VSemanal);
            this.Controls.Add(this.Vdia);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.HeaderVender);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(156, 60);
            this.Name = "Analisis";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Analisis";
            this.Load += new System.EventHandler(this.Analisis_Load);
            this.HeaderVender.ResumeLayout(false);
            this.HeaderVender.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Vdia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VSemanal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VMensual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PMensual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Psemanal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Pdiario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel HeaderVender;
        private System.Windows.Forms.Label LabelResumen;
        private System.Windows.Forms.Button Minimiza;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataVisualization.Charting.Chart Vdia;
        private System.Windows.Forms.DataVisualization.Charting.Chart VSemanal;
        private System.Windows.Forms.DataVisualization.Charting.Chart VMensual;
        private System.Windows.Forms.DataVisualization.Charting.Chart PMensual;
        private System.Windows.Forms.DataVisualization.Charting.Chart Psemanal;
        private System.Windows.Forms.DataVisualization.Charting.Chart Pdiario;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.DataGridView dataGridView5;
        private System.Windows.Forms.DataGridView dataGridView6;
        private System.Windows.Forms.Label label7;
    }
}