﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Reef___Punto_de_Venta.Properties;

namespace Reef___Punto_de_Venta.Vistas
{
    public partial class Productos : Form
    {
        TextBox[] textboxs;
        string[] campos=new string[Sentencias.productos.Length];
        string[] header = { "Clave","Linea", "Descripcion", "Cantidad", "Precio Compra", "Precio Venta" };
        DataSet tht = new DataSet();
        BindingSource binding = new BindingSource();

        public Productos()
        {
            InitializeComponent();
        }

        private void Minimizar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        ///////////AQUI ESTA EL LOAD///////////////////////////////
        private void Productos_Load(object sender, EventArgs e)
        {
            Array.Copy(Sentencias.productos, campos, campos.Length);
            textboxs = new TextBox[] { TextBoxClaveA, TextBoxCategoriaA, TextBoxDescripcionA, TextBoxStockA, TextBoxPCA, TextBoxPVA };
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            Sentencias.llenar(dataGridView1, ImagenProducto, "productos", campos, true, "img",header);
            dataGridView1.Columns[0].Width = 100;
            dataGridView1.Columns[1].Width = 100;
            dataGridView1.Columns[2].Width = 329;
            dataGridView1.Columns[3].Width = 100;
            dataGridView1.Columns[4].Width = 100;
            dataGridView1.Columns[5].Width = 100;
        }

        private void ContenedorControles_Paint(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics, this.ContenedorControles.ClientRectangle, Color.FromArgb(165, 182, 195), ButtonBorderStyle.Solid);
        }

        private void Limpiar_Click(object sender, EventArgs e)
        {
            Sentencias.limpiar(textboxs);
        }

        private void ImagenProducto_MouseClick(object sender, MouseEventArgs e)
        {
            if (dataGridView1.RowCount > 1)
            {
                Election elec = new Election(Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[0].Value),this);
                elec.Show();
            }
            else
            {
                MessageBox.Show("Debes seleccionar un objeto valido");
            }
        }

        private void Buscar_Click(object sender, EventArgs e)
        {
            string[] camposbusca = { "id_producto", "descripcion" };
            string[] valores = { TextBoxClave.Text, TextBoxDescripcion.Text };
            Sentencias.buscar("productos", dataGridView1, ImagenProducto, campos, camposbusca, valores, true, "img");
        }

        private void Remover_Click(object sender, EventArgs e)
        {
            Sentencias.eliminar(dataGridView1,"productos",campos);
            Sentencias.llenar(dataGridView1, ImagenProducto, "productos", campos, true, "img", header);
        }

        private void Editar_Click(object sender, EventArgs e)
        {
            Sentencias.llenartextbox(textboxs,dataGridView1,Agregar,true);
        }

        private void Agregar_Click(object sender, EventArgs e)
        {
            string[] valores = { TextBoxClaveA.Text, "\"" + TextBoxCategoriaA.Text + "\"", "\"" + TextBoxDescripcionA.Text + "\"", TextBoxStockA.Text, TextBoxPCA.Text.Replace(',','.'), TextBoxPVA.Text.Replace(',','.') };
            if (Agregar.Text == "Agregar Producto")
            {
                if (areFull())
                {
                    Sentencias.insertar("productos", campos, valores,true);
                    Imagen.ActualziarImagen(Imagen.ResizeImage(Resources.ImageNull),"productos",campos[0],valores[0]);
                }
                else
                {
                    MessageBox.Show("Deben estar llenos todos los campos para agregar un producto");
                }
                Sentencias.llenar(dataGridView1, ImagenProducto, "productos", campos, true, "img",header);
                Sentencias.limpiar(textboxs);
            }
            else
            {
                if (areFull())
                {
                    Sentencias.modificar("productos", campos, valores,true);
                }
                else
                {
                    MessageBox.Show("Deben estar llenos todos los campos para editarr un producto");
                }
                Agregar.Text = "Agregar Producto";
                Sentencias.llenar(dataGridView1, ImagenProducto, "productos", campos, true, "img", header);
                Sentencias.limpiar(textboxs);
            }

        }
        private bool areFull()
        {
            bool lleno = true;
            int a = 0;
            while (lleno != false && a != textboxs.Length)
            {
                if (textboxs[a].Text == "")
                {
                    lleno = false;
                }
                a++;
            }
            return lleno;
        }

        private void Cancelar_Click(object sender, EventArgs e)
        {
            Sentencias.limpiar(textboxs);
            TextBox[] texts = { TextBoxClave, TextBoxDescripcion };
            Sentencias.limpiar(texts);
            Agregar.Text = "Agregar Producto";
            Sentencias.llenar(dataGridView1,ImagenProducto, "productos", campos, true, "img", header);
        }
        private void cargarposicion()
        {
            try
            {
                MyConnection conecta = new MyConnection();
                conecta.Crear_conexion();
                string valores = "select * from productos;";
                MySqlCommand busca = new MySqlCommand(valores, conecta.getConexion());
                //Crea el adaptador
                MySqlDataAdapter cmc = new MySqlDataAdapter(busca);
                //Llena el DataSet
                cmc.Fill(tht, "productos");
                //Enlaza el Binding Source con la tabla
                binding.DataSource = tht.Tables["productos"].DefaultView;
                //Enlaza el datagrind
                dataGridView1.DataSource = binding;
                dataGridView1.Columns["img"].Visible = false;
                //Enlaza los textbox con los campos
                ImagenProducto.DataBindings.Add("Image", binding, "img", true);
                conecta.Cerrar_conexion();
            }
            catch (Exception)
            {
            }
        }
        public void llenar()
        {
            this.Cancelar.PerformClick();
        }

        private void HeaderVender_Paint(object sender, PaintEventArgs e)
        {

        }

        private void ImagenProducto_Click(object sender, EventArgs e)
        {

        }
    }
}
