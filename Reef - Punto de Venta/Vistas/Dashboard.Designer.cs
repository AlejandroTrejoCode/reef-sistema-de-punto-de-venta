﻿namespace Reef___Punto_de_Venta
{
    partial class Inicio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Inicio));
            this.LabelVender = new System.Windows.Forms.Label();
            this.LabelProductos = new System.Windows.Forms.Label();
            this.LabelReportes = new System.Windows.Forms.Label();
            this.LabelInventario = new System.Windows.Forms.Label();
            this.LblCorte = new System.Windows.Forms.Label();
            this.LabelAnalisis = new System.Windows.Forms.Label();
            this.LabelOfertas = new System.Windows.Forms.Label();
            this.LabelAjustes = new System.Windows.Forms.Label();
            this.Header = new System.Windows.Forms.Panel();
            this.AbiertoCerrado = new System.Windows.Forms.Button();
            this.ContenedorBotonAbierto = new System.Windows.Forms.PictureBox();
            this.Buscar = new System.Windows.Forms.PictureBox();
            this.TextBoxBuscar = new System.Windows.Forms.TextBox();
            this.ContenedorBuscar = new System.Windows.Forms.PictureBox();
            this.panelWorkSpace = new System.Windows.Forms.Panel();
            this.Menu = new System.Windows.Forms.Panel();
            this.MenuSelector = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.MenuVender = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.MenuProductos = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.MenuReportes = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.MenuInventario = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.MenuCorte = new System.Windows.Forms.Panel();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.MenuAnalisis = new System.Windows.Forms.Panel();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.MenuOfertas = new System.Windows.Forms.Panel();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.MenuAjustes = new System.Windows.Forms.Panel();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.MenuUsuario = new System.Windows.Forms.Panel();
            this.UserImg = new System.Windows.Forms.PictureBox();
            this.LabelCorte = new System.Windows.Forms.LinkLabel();
            this.labelUser = new System.Windows.Forms.Label();
            this.labelType = new System.Windows.Forms.Label();
            this.TimerHora = new System.Windows.Forms.Timer(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.Header.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorBotonAbierto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Buscar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorBuscar)).BeginInit();
            this.Menu.SuspendLayout();
            this.MenuVender.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.MenuProductos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.MenuReportes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.MenuInventario.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.MenuCorte.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.MenuAnalisis.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.MenuOfertas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.MenuAjustes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.MenuUsuario.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UserImg)).BeginInit();
            this.SuspendLayout();
            // 
            // LabelVender
            // 
            this.LabelVender.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelVender.ForeColor = System.Drawing.Color.White;
            this.LabelVender.Location = new System.Drawing.Point(38, 17);
            this.LabelVender.Name = "LabelVender";
            this.LabelVender.Size = new System.Drawing.Size(100, 30);
            this.LabelVender.TabIndex = 1;
            this.LabelVender.Text = "Vender";
            this.LabelVender.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolTip1.SetToolTip(this.LabelVender, "Realiza ventas de productos.");
            this.LabelVender.Click += new System.EventHandler(this.LabelVender_Click);
            this.LabelVender.MouseEnter += new System.EventHandler(this.MenuVender_MouseEnter);
            this.LabelVender.MouseLeave += new System.EventHandler(this.MenuVender_MouseLeave);
            // 
            // LabelProductos
            // 
            this.LabelProductos.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelProductos.ForeColor = System.Drawing.Color.White;
            this.LabelProductos.Location = new System.Drawing.Point(38, 20);
            this.LabelProductos.Name = "LabelProductos";
            this.LabelProductos.Size = new System.Drawing.Size(102, 30);
            this.LabelProductos.TabIndex = 3;
            this.LabelProductos.Text = "Productos";
            this.LabelProductos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolTip1.SetToolTip(this.LabelProductos, "Agrega, modifica y consulta productos.");
            this.LabelProductos.Click += new System.EventHandler(this.LabelProductos_Click);
            this.LabelProductos.MouseEnter += new System.EventHandler(this.MenuProductos_MouseEnter);
            this.LabelProductos.MouseLeave += new System.EventHandler(this.MenuProductos_MouseLeave);
            // 
            // LabelReportes
            // 
            this.LabelReportes.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelReportes.ForeColor = System.Drawing.Color.White;
            this.LabelReportes.Location = new System.Drawing.Point(38, 17);
            this.LabelReportes.Name = "LabelReportes";
            this.LabelReportes.Size = new System.Drawing.Size(105, 30);
            this.LabelReportes.TabIndex = 3;
            this.LabelReportes.Text = "Reportes";
            this.LabelReportes.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.LabelReportes, "Genera y consulta reportes de ventas y productos.");
            this.LabelReportes.Click += new System.EventHandler(this.LabelReportes_Click);
            this.LabelReportes.MouseEnter += new System.EventHandler(this.MenuReportes_MouseEnter);
            this.LabelReportes.MouseLeave += new System.EventHandler(this.MenuReportes_MouseLeave);
            // 
            // LabelInventario
            // 
            this.LabelInventario.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelInventario.ForeColor = System.Drawing.Color.White;
            this.LabelInventario.Location = new System.Drawing.Point(38, 17);
            this.LabelInventario.Name = "LabelInventario";
            this.LabelInventario.Size = new System.Drawing.Size(107, 30);
            this.LabelInventario.TabIndex = 3;
            this.LabelInventario.Text = "Inventario";
            this.LabelInventario.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.LabelInventario, "Consulta todos los productos existentes.");
            this.LabelInventario.Click += new System.EventHandler(this.LabelInventario_Click);
            this.LabelInventario.MouseEnter += new System.EventHandler(this.MenuInventario_MouseEnter);
            this.LabelInventario.MouseLeave += new System.EventHandler(this.MenuInventario_MouseLeave);
            // 
            // LblCorte
            // 
            this.LblCorte.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCorte.ForeColor = System.Drawing.Color.White;
            this.LblCorte.Location = new System.Drawing.Point(38, 17);
            this.LblCorte.Name = "LblCorte";
            this.LblCorte.Size = new System.Drawing.Size(100, 30);
            this.LblCorte.TabIndex = 5;
            this.LblCorte.Text = "Corte";
            this.LblCorte.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolTip1.SetToolTip(this.LblCorte, "Realiza el corte de caja.");
            this.LblCorte.Click += new System.EventHandler(this.LblCorte_Click);
            this.LblCorte.MouseEnter += new System.EventHandler(this.MenuCorte_MouseEnter);
            this.LblCorte.MouseLeave += new System.EventHandler(this.MenuCorte_MouseLeave);
            // 
            // LabelAnalisis
            // 
            this.LabelAnalisis.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelAnalisis.ForeColor = System.Drawing.Color.White;
            this.LabelAnalisis.Location = new System.Drawing.Point(38, 17);
            this.LabelAnalisis.Name = "LabelAnalisis";
            this.LabelAnalisis.Size = new System.Drawing.Size(100, 30);
            this.LabelAnalisis.TabIndex = 3;
            this.LabelAnalisis.Text = "Análisis";
            this.LabelAnalisis.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolTip1.SetToolTip(this.LabelAnalisis, "Consulta de manera detalla el historial de ventas y productos.");
            this.LabelAnalisis.Click += new System.EventHandler(this.LabelAnalisis_Click);
            this.LabelAnalisis.MouseEnter += new System.EventHandler(this.MenuAnalisis_MouseEnter);
            this.LabelAnalisis.MouseLeave += new System.EventHandler(this.MenuAnalisis_MouseLeave);
            // 
            // LabelOfertas
            // 
            this.LabelOfertas.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelOfertas.ForeColor = System.Drawing.Color.White;
            this.LabelOfertas.Location = new System.Drawing.Point(38, 17);
            this.LabelOfertas.Name = "LabelOfertas";
            this.LabelOfertas.Size = new System.Drawing.Size(107, 30);
            this.LabelOfertas.TabIndex = 5;
            this.LabelOfertas.Text = "Ofertas";
            this.LabelOfertas.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolTip1.SetToolTip(this.LabelOfertas, "Consulta las mejores ofertas para tus productos.");
            this.LabelOfertas.Click += new System.EventHandler(this.LabelOfertas_Click);
            this.LabelOfertas.MouseEnter += new System.EventHandler(this.MenuOfertas_MouseEnter);
            this.LabelOfertas.MouseLeave += new System.EventHandler(this.MenuOfertas_MouseLeave);
            // 
            // LabelAjustes
            // 
            this.LabelAjustes.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelAjustes.ForeColor = System.Drawing.Color.White;
            this.LabelAjustes.Location = new System.Drawing.Point(38, 17);
            this.LabelAjustes.Name = "LabelAjustes";
            this.LabelAjustes.Size = new System.Drawing.Size(102, 30);
            this.LabelAjustes.TabIndex = 7;
            this.LabelAjustes.Text = "Ajustes";
            this.LabelAjustes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolTip1.SetToolTip(this.LabelAjustes, "Configura tu sistema de punto de venta Reef.");
            this.LabelAjustes.Click += new System.EventHandler(this.LabelAjustes_Click);
            this.LabelAjustes.MouseEnter += new System.EventHandler(this.MenuAjustes_MouseEnter);
            this.LabelAjustes.MouseLeave += new System.EventHandler(this.MenuAjustes_MouseLeave);
            // 
            // Header
            // 
            this.Header.BackColor = System.Drawing.Color.White;
            this.Header.Controls.Add(this.AbiertoCerrado);
            this.Header.Controls.Add(this.ContenedorBotonAbierto);
            this.Header.Controls.Add(this.Buscar);
            this.Header.Controls.Add(this.TextBoxBuscar);
            this.Header.Controls.Add(this.ContenedorBuscar);
            this.Header.ForeColor = System.Drawing.Color.White;
            this.Header.Location = new System.Drawing.Point(156, 0);
            this.Header.Name = "Header";
            this.Header.Size = new System.Drawing.Size(1210, 60);
            this.Header.TabIndex = 0;
            this.Header.Paint += new System.Windows.Forms.PaintEventHandler(this.Header_Paint);
            // 
            // AbiertoCerrado
            // 
            this.AbiertoCerrado.BackColor = System.Drawing.Color.White;
            this.AbiertoCerrado.FlatAppearance.BorderSize = 0;
            this.AbiertoCerrado.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.AbiertoCerrado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AbiertoCerrado.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AbiertoCerrado.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(200)))), ((int)(((byte)(163)))));
            this.AbiertoCerrado.Location = new System.Drawing.Point(1093, 15);
            this.AbiertoCerrado.Name = "AbiertoCerrado";
            this.AbiertoCerrado.Size = new System.Drawing.Size(85, 30);
            this.AbiertoCerrado.TabIndex = 8;
            this.AbiertoCerrado.TabStop = false;
            this.AbiertoCerrado.Text = "Abierto";
            this.AbiertoCerrado.UseVisualStyleBackColor = false;
            this.AbiertoCerrado.Click += new System.EventHandler(this.AbiertoCerrado_Click);
            // 
            // ContenedorBotonAbierto
            // 
            this.ContenedorBotonAbierto.Image = global::Reef___Punto_de_Venta.Properties.Resources.ButtonAbierto_fw;
            this.ContenedorBotonAbierto.Location = new System.Drawing.Point(1085, 12);
            this.ContenedorBotonAbierto.Name = "ContenedorBotonAbierto";
            this.ContenedorBotonAbierto.Size = new System.Drawing.Size(100, 36);
            this.ContenedorBotonAbierto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ContenedorBotonAbierto.TabIndex = 9;
            this.ContenedorBotonAbierto.TabStop = false;
            // 
            // Buscar
            // 
            this.Buscar.Image = global::Reef___Punto_de_Venta.Properties.Resources.Buscar1;
            this.Buscar.Location = new System.Drawing.Point(221, 15);
            this.Buscar.Name = "Buscar";
            this.Buscar.Size = new System.Drawing.Size(30, 30);
            this.Buscar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Buscar.TabIndex = 6;
            this.Buscar.TabStop = false;
            this.Buscar.MouseEnter += new System.EventHandler(this.Buscar_MouseEnter);
            this.Buscar.MouseLeave += new System.EventHandler(this.Buscar_MouseLeave);
            // 
            // TextBoxBuscar
            // 
            this.TextBoxBuscar.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.TextBoxBuscar.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextBoxBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxBuscar.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.TextBoxBuscar.HideSelection = false;
            this.TextBoxBuscar.Location = new System.Drawing.Point(18, 19);
            this.TextBoxBuscar.MaxLength = 100;
            this.TextBoxBuscar.Name = "TextBoxBuscar";
            this.TextBoxBuscar.Size = new System.Drawing.Size(198, 22);
            this.TextBoxBuscar.TabIndex = 0;
            this.TextBoxBuscar.TabStop = false;
            this.TextBoxBuscar.WordWrap = false;
            // 
            // ContenedorBuscar
            // 
            this.ContenedorBuscar.Image = global::Reef___Punto_de_Venta.Properties.Resources.BuscarContenedor;
            this.ContenedorBuscar.Location = new System.Drawing.Point(15, 15);
            this.ContenedorBuscar.Name = "ContenedorBuscar";
            this.ContenedorBuscar.Size = new System.Drawing.Size(204, 32);
            this.ContenedorBuscar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ContenedorBuscar.TabIndex = 7;
            this.ContenedorBuscar.TabStop = false;
            // 
            // panelWorkSpace
            // 
            this.panelWorkSpace.BackColor = System.Drawing.Color.Transparent;
            this.panelWorkSpace.Location = new System.Drawing.Point(156, 60);
            this.panelWorkSpace.Name = "panelWorkSpace";
            this.panelWorkSpace.Size = new System.Drawing.Size(1210, 710);
            this.panelWorkSpace.TabIndex = 2;
            // 
            // Menu
            // 
            this.Menu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(49)))), ((int)(((byte)(52)))));
            this.Menu.Controls.Add(this.MenuSelector);
            this.Menu.Controls.Add(this.label8);
            this.Menu.Controls.Add(this.MenuVender);
            this.Menu.Controls.Add(this.MenuProductos);
            this.Menu.Controls.Add(this.MenuReportes);
            this.Menu.Controls.Add(this.MenuInventario);
            this.Menu.Controls.Add(this.MenuCorte);
            this.Menu.Controls.Add(this.MenuAnalisis);
            this.Menu.Controls.Add(this.MenuOfertas);
            this.Menu.Controls.Add(this.MenuAjustes);
            this.Menu.Location = new System.Drawing.Point(0, 250);
            this.Menu.Name = "Menu";
            this.Menu.Size = new System.Drawing.Size(156, 518);
            this.Menu.TabIndex = 1;
            // 
            // MenuSelector
            // 
            this.MenuSelector.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(226)))), ((int)(((byte)(176)))));
            this.MenuSelector.Location = new System.Drawing.Point(0, 0);
            this.MenuSelector.Name = "MenuSelector";
            this.MenuSelector.Size = new System.Drawing.Size(9, 64);
            this.MenuSelector.TabIndex = 0;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(22, 637);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 31);
            this.label8.TabIndex = 7;
            this.label8.Text = "Config";
            // 
            // MenuVender
            // 
            this.MenuVender.Controls.Add(this.pictureBox1);
            this.MenuVender.Controls.Add(this.LabelVender);
            this.MenuVender.Location = new System.Drawing.Point(13, 0);
            this.MenuVender.Name = "MenuVender";
            this.MenuVender.Size = new System.Drawing.Size(140, 64);
            this.MenuVender.TabIndex = 8;
            this.MenuVender.Click += new System.EventHandler(this.LabelVender_Click);
            this.MenuVender.MouseEnter += new System.EventHandler(this.MenuVender_MouseEnter);
            this.MenuVender.MouseLeave += new System.EventHandler(this.MenuVender_MouseLeave);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Reef___Punto_de_Venta.Properties.Resources.VenderBlancor;
            this.pictureBox1.Location = new System.Drawing.Point(2, 17);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(30, 30);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.LabelVender_Click);
            this.pictureBox1.MouseEnter += new System.EventHandler(this.MenuVender_MouseEnter);
            this.pictureBox1.MouseLeave += new System.EventHandler(this.MenuVender_MouseLeave);
            // 
            // MenuProductos
            // 
            this.MenuProductos.Controls.Add(this.pictureBox2);
            this.MenuProductos.Controls.Add(this.LabelProductos);
            this.MenuProductos.Location = new System.Drawing.Point(12, 65);
            this.MenuProductos.Name = "MenuProductos";
            this.MenuProductos.Size = new System.Drawing.Size(144, 64);
            this.MenuProductos.TabIndex = 9;
            this.MenuProductos.Click += new System.EventHandler(this.LabelProductos_Click);
            this.MenuProductos.MouseEnter += new System.EventHandler(this.MenuProductos_MouseEnter);
            this.MenuProductos.MouseLeave += new System.EventHandler(this.MenuProductos_MouseLeave);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Reef___Punto_de_Venta.Properties.Resources.ProductosBlanco1;
            this.pictureBox2.Location = new System.Drawing.Point(2, 17);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(30, 30);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.LabelProductos_Click);
            this.pictureBox2.MouseEnter += new System.EventHandler(this.MenuProductos_MouseEnter);
            this.pictureBox2.MouseLeave += new System.EventHandler(this.MenuProductos_MouseLeave);
            // 
            // MenuReportes
            // 
            this.MenuReportes.Controls.Add(this.pictureBox3);
            this.MenuReportes.Controls.Add(this.LabelReportes);
            this.MenuReportes.Location = new System.Drawing.Point(12, 130);
            this.MenuReportes.Name = "MenuReportes";
            this.MenuReportes.Size = new System.Drawing.Size(144, 64);
            this.MenuReportes.TabIndex = 10;
            this.MenuReportes.Click += new System.EventHandler(this.LabelReportes_Click);
            this.MenuReportes.MouseEnter += new System.EventHandler(this.MenuReportes_MouseEnter);
            this.MenuReportes.MouseLeave += new System.EventHandler(this.MenuReportes_MouseLeave);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Reef___Punto_de_Venta.Properties.Resources.ReporteBlanco;
            this.pictureBox3.Location = new System.Drawing.Point(2, 17);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(30, 30);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.LabelReportes_Click);
            this.pictureBox3.MouseEnter += new System.EventHandler(this.MenuReportes_MouseEnter);
            this.pictureBox3.MouseLeave += new System.EventHandler(this.MenuReportes_MouseLeave);
            // 
            // MenuInventario
            // 
            this.MenuInventario.Controls.Add(this.pictureBox4);
            this.MenuInventario.Controls.Add(this.LabelInventario);
            this.MenuInventario.Location = new System.Drawing.Point(12, 195);
            this.MenuInventario.Name = "MenuInventario";
            this.MenuInventario.Size = new System.Drawing.Size(144, 64);
            this.MenuInventario.TabIndex = 11;
            this.MenuInventario.Click += new System.EventHandler(this.LabelInventario_Click);
            this.MenuInventario.MouseEnter += new System.EventHandler(this.MenuInventario_MouseEnter);
            this.MenuInventario.MouseLeave += new System.EventHandler(this.MenuInventario_MouseLeave);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Reef___Punto_de_Venta.Properties.Resources.InventarioBlanco;
            this.pictureBox4.Location = new System.Drawing.Point(2, 17);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(30, 30);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 2;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.LabelInventario_Click);
            this.pictureBox4.MouseEnter += new System.EventHandler(this.MenuInventario_MouseEnter);
            this.pictureBox4.MouseLeave += new System.EventHandler(this.MenuInventario_MouseLeave);
            // 
            // MenuCorte
            // 
            this.MenuCorte.Controls.Add(this.pictureBox5);
            this.MenuCorte.Controls.Add(this.LblCorte);
            this.MenuCorte.Location = new System.Drawing.Point(12, 260);
            this.MenuCorte.Name = "MenuCorte";
            this.MenuCorte.Size = new System.Drawing.Size(144, 64);
            this.MenuCorte.TabIndex = 12;
            this.MenuCorte.Click += new System.EventHandler(this.LblCorte_Click);
            this.MenuCorte.MouseEnter += new System.EventHandler(this.MenuCorte_MouseEnter);
            this.MenuCorte.MouseLeave += new System.EventHandler(this.MenuCorte_MouseLeave);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::Reef___Punto_de_Venta.Properties.Resources.CorteBlanco;
            this.pictureBox5.Location = new System.Drawing.Point(2, 17);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(30, 30);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 4;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Click += new System.EventHandler(this.LblCorte_Click);
            this.pictureBox5.MouseEnter += new System.EventHandler(this.MenuCorte_MouseEnter);
            this.pictureBox5.MouseLeave += new System.EventHandler(this.MenuCorte_MouseLeave);
            // 
            // MenuAnalisis
            // 
            this.MenuAnalisis.Controls.Add(this.pictureBox6);
            this.MenuAnalisis.Controls.Add(this.LabelAnalisis);
            this.MenuAnalisis.Location = new System.Drawing.Point(12, 325);
            this.MenuAnalisis.Name = "MenuAnalisis";
            this.MenuAnalisis.Size = new System.Drawing.Size(144, 64);
            this.MenuAnalisis.TabIndex = 13;
            this.MenuAnalisis.Click += new System.EventHandler(this.LabelAnalisis_Click);
            this.MenuAnalisis.MouseEnter += new System.EventHandler(this.MenuAnalisis_MouseEnter);
            this.MenuAnalisis.MouseLeave += new System.EventHandler(this.MenuAnalisis_MouseLeave);
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::Reef___Punto_de_Venta.Properties.Resources.EstadisticasBlanco;
            this.pictureBox6.Location = new System.Drawing.Point(2, 17);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(30, 30);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 2;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.Click += new System.EventHandler(this.LabelAnalisis_Click);
            this.pictureBox6.MouseEnter += new System.EventHandler(this.MenuAnalisis_MouseEnter);
            this.pictureBox6.MouseLeave += new System.EventHandler(this.MenuAnalisis_MouseLeave);
            // 
            // MenuOfertas
            // 
            this.MenuOfertas.Controls.Add(this.pictureBox7);
            this.MenuOfertas.Controls.Add(this.LabelOfertas);
            this.MenuOfertas.Location = new System.Drawing.Point(12, 389);
            this.MenuOfertas.Name = "MenuOfertas";
            this.MenuOfertas.Size = new System.Drawing.Size(144, 64);
            this.MenuOfertas.TabIndex = 14;
            this.MenuOfertas.Click += new System.EventHandler(this.LabelOfertas_Click);
            this.MenuOfertas.MouseEnter += new System.EventHandler(this.MenuOfertas_MouseEnter);
            this.MenuOfertas.MouseLeave += new System.EventHandler(this.MenuOfertas_MouseLeave);
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::Reef___Punto_de_Venta.Properties.Resources.RecomendacionesBlanco;
            this.pictureBox7.Location = new System.Drawing.Point(2, 17);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(30, 30);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 4;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.Click += new System.EventHandler(this.LabelOfertas_Click);
            this.pictureBox7.MouseEnter += new System.EventHandler(this.MenuOfertas_MouseEnter);
            this.pictureBox7.MouseLeave += new System.EventHandler(this.MenuOfertas_MouseLeave);
            // 
            // MenuAjustes
            // 
            this.MenuAjustes.Controls.Add(this.pictureBox8);
            this.MenuAjustes.Controls.Add(this.LabelAjustes);
            this.MenuAjustes.Location = new System.Drawing.Point(12, 453);
            this.MenuAjustes.Name = "MenuAjustes";
            this.MenuAjustes.Size = new System.Drawing.Size(144, 64);
            this.MenuAjustes.TabIndex = 15;
            this.MenuAjustes.Click += new System.EventHandler(this.LabelAjustes_Click);
            this.MenuAjustes.MouseEnter += new System.EventHandler(this.MenuAjustes_MouseEnter);
            this.MenuAjustes.MouseLeave += new System.EventHandler(this.MenuAjustes_MouseLeave);
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::Reef___Punto_de_Venta.Properties.Resources.Ajustes;
            this.pictureBox8.Location = new System.Drawing.Point(2, 17);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(30, 30);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 6;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.Click += new System.EventHandler(this.LabelAjustes_Click);
            this.pictureBox8.MouseEnter += new System.EventHandler(this.MenuAjustes_MouseEnter);
            this.pictureBox8.MouseLeave += new System.EventHandler(this.MenuAjustes_MouseLeave);
            // 
            // MenuUsuario
            // 
            this.MenuUsuario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(200)))), ((int)(((byte)(163)))));
            this.MenuUsuario.Controls.Add(this.UserImg);
            this.MenuUsuario.Controls.Add(this.LabelCorte);
            this.MenuUsuario.Controls.Add(this.labelUser);
            this.MenuUsuario.Controls.Add(this.labelType);
            this.MenuUsuario.Location = new System.Drawing.Point(0, 0);
            this.MenuUsuario.Name = "MenuUsuario";
            this.MenuUsuario.Size = new System.Drawing.Size(156, 250);
            this.MenuUsuario.TabIndex = 0;
            // 
            // UserImg
            // 
            this.UserImg.Image = global::Reef___Punto_de_Venta.Properties.Resources.Reynaldo;
            this.UserImg.Location = new System.Drawing.Point(3, 12);
            this.UserImg.Name = "UserImg";
            this.UserImg.Size = new System.Drawing.Size(152, 128);
            this.UserImg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.UserImg.TabIndex = 0;
            this.UserImg.TabStop = false;
            // 
            // LabelCorte
            // 
            this.LabelCorte.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelCorte.LinkColor = System.Drawing.Color.White;
            this.LabelCorte.Location = new System.Drawing.Point(0, 210);
            this.LabelCorte.Name = "LabelCorte";
            this.LabelCorte.Size = new System.Drawing.Size(156, 19);
            this.LabelCorte.TabIndex = 3;
            this.LabelCorte.TabStop = true;
            this.LabelCorte.Text = "Salir";
            this.LabelCorte.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.LabelCorte.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LabelCorte_LinkClicked);
            // 
            // labelUser
            // 
            this.labelUser.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUser.ForeColor = System.Drawing.Color.White;
            this.labelUser.Location = new System.Drawing.Point(1, 143);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new System.Drawing.Size(156, 33);
            this.labelUser.TabIndex = 2;
            this.labelUser.Text = "Paco";
            this.labelUser.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelType
            // 
            this.labelType.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelType.ForeColor = System.Drawing.Color.White;
            this.labelType.Location = new System.Drawing.Point(1, 176);
            this.labelType.Name = "labelType";
            this.labelType.Size = new System.Drawing.Size(156, 34);
            this.labelType.TabIndex = 1;
            this.labelType.Text = "Admin";
            this.labelType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TimerHora
            // 
            this.TimerHora.Enabled = true;
            // 
            // Inicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1366, 768);
            this.Controls.Add(this.panelWorkSpace);
            this.Controls.Add(this.Header);
            this.Controls.Add(this.MenuUsuario);
            this.Controls.Add(this.Menu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Inicio";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "REEF - SPV";
            this.Load += new System.EventHandler(this.Inicio_Load);
            this.Header.ResumeLayout(false);
            this.Header.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorBotonAbierto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Buscar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorBuscar)).EndInit();
            this.Menu.ResumeLayout(false);
            this.Menu.PerformLayout();
            this.MenuVender.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.MenuProductos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.MenuReportes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.MenuInventario.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.MenuCorte.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.MenuAnalisis.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.MenuOfertas.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.MenuAjustes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.MenuUsuario.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.UserImg)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Header;
        private System.Windows.Forms.Panel Menu;
        private System.Windows.Forms.Panel MenuUsuario;
        private System.Windows.Forms.TextBox TextBoxBuscar;
        private System.Windows.Forms.PictureBox UserImg;
        public  System.Windows.Forms.Label labelType;
        public  System.Windows.Forms.Label labelUser;
        private System.Windows.Forms.LinkLabel LabelCorte;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox Buscar;
        private System.Windows.Forms.PictureBox ContenedorBuscar;
        private System.Windows.Forms.Panel MenuVender;
        private System.Windows.Forms.Panel MenuInventario;
        private System.Windows.Forms.Panel MenuReportes;
        private System.Windows.Forms.Panel MenuProductos;
        private System.Windows.Forms.Panel MenuOfertas;
        private System.Windows.Forms.Panel MenuAnalisis;
        private System.Windows.Forms.Panel MenuCorte;
        private System.Windows.Forms.Panel MenuAjustes;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel MenuSelector;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        public System.Windows.Forms.Label LabelVender;
        private System.Windows.Forms.Label LabelProductos;
        private System.Windows.Forms.Label LabelReportes;
        private System.Windows.Forms.Label LabelInventario;
        private System.Windows.Forms.Label LblCorte;
        private System.Windows.Forms.Label LabelAnalisis;
        private System.Windows.Forms.Label LabelOfertas;
        private System.Windows.Forms.Label LabelAjustes;
        private System.Windows.Forms.Timer TimerHora;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Panel panelWorkSpace;
        private System.Windows.Forms.Button AbiertoCerrado;
        private System.Windows.Forms.PictureBox ContenedorBotonAbierto;
        private System.Windows.Forms.ToolTip toolTip2;
        public int IDuser;
    }
}