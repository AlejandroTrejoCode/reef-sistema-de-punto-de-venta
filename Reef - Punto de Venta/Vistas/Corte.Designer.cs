﻿namespace Reef___Punto_de_Venta.Vistas
{
    partial class Corte
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Corte));
            this.HeaderVender = new System.Windows.Forms.Panel();
            this.LabelDia = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Minimiza = new System.Windows.Forms.Button();
            this.LabelResumen = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.PanelCorte = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.LabelGanancias = new System.Windows.Forms.Label();
            this.LabelGanacias = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.HeaderVender.SuspendLayout();
            this.PanelCorte.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // HeaderVender
            // 
            this.HeaderVender.BackColor = System.Drawing.Color.White;
            this.HeaderVender.Controls.Add(this.LabelDia);
            this.HeaderVender.Controls.Add(this.label1);
            this.HeaderVender.Controls.Add(this.Minimiza);
            this.HeaderVender.Controls.Add(this.LabelResumen);
            this.HeaderVender.Location = new System.Drawing.Point(205, 0);
            this.HeaderVender.Name = "HeaderVender";
            this.HeaderVender.Size = new System.Drawing.Size(800, 93);
            this.HeaderVender.TabIndex = 3;
            this.HeaderVender.Paint += new System.Windows.Forms.PaintEventHandler(this.HeaderVender_Paint);
            // 
            // LabelDia
            // 
            this.LabelDia.BackColor = System.Drawing.Color.Transparent;
            this.LabelDia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LabelDia.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelDia.ForeColor = System.Drawing.Color.Coral;
            this.LabelDia.Location = new System.Drawing.Point(197, 53);
            this.LabelDia.Name = "LabelDia";
            this.LabelDia.Size = new System.Drawing.Size(140, 38);
            this.LabelDia.TabIndex = 28;
            this.LabelDia.Text = "06/08/16";
            this.LabelDia.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LabelDia.Click += new System.EventHandler(this.LabelDia_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DimGray;
            this.label1.Location = new System.Drawing.Point(14, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(177, 38);
            this.label1.TabIndex = 27;
            this.label1.Text = "Corte del día:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Minimiza
            // 
            this.Minimiza.BackgroundImage = global::Reef___Punto_de_Venta.Properties.Resources.Cerrar1;
            this.Minimiza.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Minimiza.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Minimiza.ForeColor = System.Drawing.Color.PowderBlue;
            this.Minimiza.Location = new System.Drawing.Point(780, 0);
            this.Minimiza.Name = "Minimiza";
            this.Minimiza.Size = new System.Drawing.Size(20, 20);
            this.Minimiza.TabIndex = 3;
            this.Minimiza.UseVisualStyleBackColor = true;
            this.Minimiza.Click += new System.EventHandler(this.Minimiza_Click);
            // 
            // LabelResumen
            // 
            this.LabelResumen.AutoSize = true;
            this.LabelResumen.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelResumen.ForeColor = System.Drawing.Color.DimGray;
            this.LabelResumen.Location = new System.Drawing.Point(12, 6);
            this.LabelResumen.Name = "LabelResumen";
            this.LabelResumen.Size = new System.Drawing.Size(223, 38);
            this.LabelResumen.TabIndex = 1;
            this.LabelResumen.Text = "Inicio > Corte";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DimGray;
            this.label2.Location = new System.Drawing.Point(80, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(197, 40);
            this.label2.TabIndex = 28;
            this.label2.Text = "Ventas Totales:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.DimGray;
            this.label3.Location = new System.Drawing.Point(503, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(141, 40);
            this.label3.TabIndex = 29;
            this.label3.Text = "Ganacias:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PanelCorte
            // 
            this.PanelCorte.BackColor = System.Drawing.Color.White;
            this.PanelCorte.Controls.Add(this.listBox1);
            this.PanelCorte.Controls.Add(this.dataGridView2);
            this.PanelCorte.Controls.Add(this.label4);
            this.PanelCorte.Controls.Add(this.LabelGanancias);
            this.PanelCorte.Controls.Add(this.LabelGanacias);
            this.PanelCorte.Controls.Add(this.pictureBox3);
            this.PanelCorte.Controls.Add(this.pictureBox2);
            this.PanelCorte.Controls.Add(this.pictureBox1);
            this.PanelCorte.Controls.Add(this.label3);
            this.PanelCorte.Controls.Add(this.label2);
            this.PanelCorte.Controls.Add(this.dataGridView1);
            this.PanelCorte.Location = new System.Drawing.Point(205, 99);
            this.PanelCorte.Name = "PanelCorte";
            this.PanelCorte.Size = new System.Drawing.Size(800, 599);
            this.PanelCorte.TabIndex = 30;
            this.PanelCorte.Paint += new System.Windows.Forms.PaintEventHandler(this.PanelCorte_Paint);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.DimGray;
            this.label4.Location = new System.Drawing.Point(151, 157);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(217, 40);
            this.label4.TabIndex = 35;
            this.label4.Text = "Ventas Resumen";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LabelGanancias
            // 
            this.LabelGanancias.BackColor = System.Drawing.Color.Transparent;
            this.LabelGanancias.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LabelGanancias.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelGanancias.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(200)))), ((int)(((byte)(163)))));
            this.LabelGanancias.Location = new System.Drawing.Point(638, 45);
            this.LabelGanancias.Name = "LabelGanancias";
            this.LabelGanancias.Size = new System.Drawing.Size(132, 40);
            this.LabelGanancias.TabIndex = 34;
            this.LabelGanancias.Text = "$1,000";
            this.LabelGanancias.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LabelGanacias
            // 
            this.LabelGanacias.BackColor = System.Drawing.Color.Transparent;
            this.LabelGanacias.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LabelGanacias.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelGanacias.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(200)))), ((int)(((byte)(163)))));
            this.LabelGanacias.Location = new System.Drawing.Point(283, 45);
            this.LabelGanacias.Name = "LabelGanacias";
            this.LabelGanacias.Size = new System.Drawing.Size(132, 40);
            this.LabelGanacias.TabIndex = 33;
            this.LabelGanacias.Text = "$1,000";
            this.LabelGanacias.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Reef___Punto_de_Venta.Properties.Resources.point_of_service;
            this.pictureBox3.Location = new System.Drawing.Point(85, 151);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(60, 50);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 32;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Reef___Punto_de_Venta.Properties.Resources.rich;
            this.pictureBox2.Location = new System.Drawing.Point(438, 40);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(60, 50);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 31;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Reef___Punto_de_Venta.Properties.Resources.change;
            this.pictureBox1.Location = new System.Drawing.Point(19, 40);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(60, 50);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 30;
            this.pictureBox1.TabStop = false;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(391, 175);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(24, 19);
            this.dataGridView2.TabIndex = 37;
            this.dataGridView2.Visible = false;
            // 
            // listBox1
            // 
            this.listBox1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBox1.ForeColor = System.Drawing.Color.DimGray;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 20;
            this.listBox1.Location = new System.Drawing.Point(152, 207);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(492, 344);
            this.listBox1.TabIndex = 39;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(152, 207);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(492, 344);
            this.dataGridView1.TabIndex = 40;
            this.dataGridView1.Visible = false;
            // 
            // Corte
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1210, 705);
            this.Controls.Add(this.HeaderVender);
            this.Controls.Add(this.PanelCorte);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(156, 60);
            this.Name = "Corte";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Corte";
            this.Load += new System.EventHandler(this.Corte_Load);
            this.HeaderVender.ResumeLayout(false);
            this.HeaderVender.PerformLayout();
            this.PanelCorte.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel HeaderVender;
        private System.Windows.Forms.Label LabelResumen;
        private System.Windows.Forms.Button Minimiza;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LabelDia;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel PanelCorte;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label LabelGanacias;
        private System.Windows.Forms.Label LabelGanancias;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}