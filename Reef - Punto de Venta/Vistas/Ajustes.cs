﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using MySql.Data.MySqlClient;

namespace Reef___Punto_de_Venta.Vistas
{
    public partial class Ajustes : Form
    {
        public Ajustes()
        {
            InitializeComponent();
        }

        BindingSource bindin = new BindingSource();
        MyConnection con = new MyConnection();

        private void HeaderVender_Paint(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics, this.HeaderVender.ClientRectangle, Color.FromArgb(165, 182, 195), ButtonBorderStyle.Solid);
        }

        private void Ajustes_Load(object sender, EventArgs e)
        {
            TextBoxUbicacion.Text = Inicio.vistaDashboard.LabelDireccion.Text;
            TextBoxNombreT.Text = Inicio.vistaDashboard.LabelNombreTienda.Text;

            llenar();
        }

        private void CambiarDatos_Click(object sender, EventArgs e)
        {
            if (TextBoxNombreT.Text.ToUpper() != "APD" && TextBoxUbicacion.Text.ToUpper() != "APD")
            {
                Inicio.vistaDashboard.LabelDireccion.Text = TextBoxUbicacion.Text;
                Inicio.vistaDashboard.LabelNombreTienda.Text = TextBoxNombreT.Text;
                Modificar_DatosTienda();
            }
            else
            {
                ///apd
                PictureBox[] apd = new PictureBox[8];
                for (int i = 0; i < 8; i++)
                {
                    apd[i] = new PictureBox();
                    apd[i].Image = global::Reef___Punto_de_Venta.Properties.Resources.apd;
                    apd[i].SizeMode = PictureBoxSizeMode.AutoSize;
                }
                Inicio.vistaDashboard.Tarjeta1.Controls.Add(apd[0]);
                Inicio.vistaDashboard.Tarjeta2.Controls.Add(apd[1]);
                Inicio.vistaDashboard.Tarjeta3.Controls.Add(apd[2]);
                Inicio.vistaDashboard.Tarjeta4.Controls.Add(apd[3]);
                Inicio.vistaDashboard.Tarjeta5.Controls.Add(apd[4]);
                Inicio.vistaDashboard.Tarjeta6.Controls.Add(apd[5]);
                Inicio.vistaDashboard.Tarjeta7.Controls.Add(apd[6]);
                Inicio.vistaDashboard.Tarjeta8.Controls.Add(apd[7]);
            }
        }

        private void Modificar_DatosTienda()
        {
            string txt = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            txt = txt.Remove(0, 6);//Remueve la parte "file:\"
            txt = txt.Remove(txt.Length - 9, 9);
            txt += "InfoTienda\\";
            //Modifica el nombre de la tienda
            FileStream stream = new FileStream(@txt + "nombre_tienda.txt", FileMode.Create, FileAccess.Write);
            StreamWriter escribe = new StreamWriter(stream);
            escribe.WriteLine(TextBoxNombreT.Text);
            escribe.Close();
            //Modifica la direccion de la tienda
            stream = new FileStream(@txt + "direccion_tienda.txt", FileMode.Create, FileAccess.Write);
            escribe = new StreamWriter(stream);
            escribe.WriteLine(TextBoxUbicacion.Text);
            //Cierra el documento
            escribe.Close();
        }
        private void Minimizar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void llenar()//Para llenar el DataGrid.
        {
            con.Crear_conexion();
            string search = "select*from login;";
            MySqlCommand buscaLogges = new MySqlCommand(search, con.getConexion());
            MySqlDataAdapter cmc = new MySqlDataAdapter(buscaLogges);
            DataSet tht = new DataSet();
            cmc.Fill(tht, "login");
            dataGridView1.DataSource = tht.Tables["login"].DefaultView;
        }

        public void cargarposicion()
        {
            try
            {
                con.Crear_conexion();
                string valores = "select * from login";
                MySqlCommand busca = new MySqlCommand(valores, con.getConexion());
                //crea el adaptador
                MySqlDataAdapter cmc = new MySqlDataAdapter(busca);
                DataSet tht = new DataSet();
                //llama al dataset
                cmc.Fill(tht, "login");
                //enlaza el bindingsource en la tabla
                bindin.DataSource = tht.Tables["login"].DefaultView;
                //enlaza el datagrid con el bindin source
                dataGridView1.DataSource = bindin;
                //enlaza los textbox a los campos
                texbId.DataBindings.Add("Text", bindin, "id_user", true);
                textBox1.DataBindings.Add("Text", bindin, "Nom_us", true);
                pass.DataBindings.Add("Text", bindin, "Pass", true);

                con.Cerrar_conexion();

                AgregarUsuario.Enabled = false;
            }
            catch { }
        }//para llenar el datagrid y llenar texboxs con datos del dgv

        public void limpiartxt()
        {
            texbId.Clear();
            textBox1.Clear();
            pass.Clear();
        }

        private void AgregarUsuario_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Desea añadir usuario?", "Confirme", MessageBoxButtons.OKCancel);

            if (result == DialogResult.OK)
            {
                if (string.IsNullOrEmpty(textBox1.Text) || string.IsNullOrEmpty(pass.Text))
                {
                    MessageBox.Show("Faltan campos");
                }
                else
                {
                    con.Crear_conexion();
                    string inserta = "INSERT INTO login (Nom_us, Pass, T_user) Values(" + "'" + textBox1.Text + "','" + pass.Text + "','" + comboBox1.SelectedItem + "')";
                    MySqlCommand pro = new MySqlCommand(inserta);
                    pro.Connection = con.getConexion();
                    pro.ExecuteNonQuery();
                    con.Cerrar_conexion();
                    MessageBox.Show("Se ha ingresado satisfactoriamente", "Inserción Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    llenar();

                    limpiartxt();
                }
            }
            limpiartxt();
        }

        private void ModificarUsuario_Click_1(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Desea modificar usuario?", "Confirme", MessageBoxButtons.OKCancel);

            if (result == DialogResult.OK)
            {
                try
                {
                    con.Crear_conexion();
                    string actualiza = "UPDATE login SET Nom_us=@C2, Pass=@C3, T_user=@C4 where id_user = " + texbId.Text + "";
                    MySqlCommand revisa = new MySqlCommand(actualiza);
                    revisa.Connection = con.getConexion();
                    revisa.Parameters.AddWithValue("@C2", (textBox1.Text));
                    revisa.Parameters.AddWithValue("@C3", (pass.Text));
                    revisa.Parameters.AddWithValue("@C4", (comboBox1.SelectedItem));
                    MessageBox.Show("Registro Modificado", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    con.getConexion();
                    revisa.ExecuteNonQuery();
                    con.Cerrar_conexion();

                    llenar();
                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
            else
            {
                AgregarUsuario.Enabled = false;
            }
        }

        private void EliminarUsuario_Click_1(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Desea eliminar usuario?", "Confirme", MessageBoxButtons.OKCancel);

            if (result == DialogResult.OK)
            {
                con.Crear_conexion();
                string eliminacion = "DELETE FROM login WHERE id_user=" + texbId.Text + ";";
                MySqlCommand chec = new MySqlCommand(eliminacion, con.getConexion());
                chec.ExecuteNonQuery();
                con.Cerrar_conexion();
                MessageBox.Show("Registro eliminado", "Eliminación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                llenar();

                limpiartxt();
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            cargarposicion();
            AgregarUsuario.Enabled = false;
        }

        private void dataGridView1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Escape))
            {
                limpiartxt();
                llenar();
                AgregarUsuario.Enabled = true;
            }
        }

        private void dataGridView1_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Escape))
            {
                limpiartxt();
                llenar();
                AgregarUsuario.Enabled = true;
            }
        }
    }
}
