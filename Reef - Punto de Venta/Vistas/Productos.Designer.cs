﻿namespace Reef___Punto_de_Venta.Vistas
{
    partial class Productos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Productos));
            this.HeaderVender = new System.Windows.Forms.Panel();
            this.Minimizar = new System.Windows.Forms.Button();
            this.LabelResumen = new System.Windows.Forms.Label();
            this.Agregar = new System.Windows.Forms.Button();
            this.TextBoxClaveA = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.TextBoxStockA = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.TextBoxPCA = new System.Windows.Forms.TextBox();
            this.TextBoxPVA = new System.Windows.Forms.TextBox();
            this.ContenedorControles = new System.Windows.Forms.Panel();
            this.ContedorControles = new System.Windows.Forms.Panel();
            this.Editarbutton = new System.Windows.Forms.Button();
            this.Cancelar = new System.Windows.Forms.Button();
            this.Remover = new System.Windows.Forms.Button();
            this.Buscar = new System.Windows.Forms.Button();
            this.TextBoxDescripcion = new System.Windows.Forms.TextBox();
            this.ContenedorDescripcion = new System.Windows.Forms.PictureBox();
            this.LabelDescripcion = new System.Windows.Forms.Label();
            this.TextBoxClave = new System.Windows.Forms.TextBox();
            this.ContenedorClave = new System.Windows.Forms.PictureBox();
            this.LabelClave = new System.Windows.Forms.Label();
            this.ImagenProducto = new System.Windows.Forms.PictureBox();
            this.PanelAgregar = new System.Windows.Forms.Panel();
            this.ContenedorPrecioCA = new System.Windows.Forms.PictureBox();
            this.ContenedorPVA = new System.Windows.Forms.PictureBox();
            this.TextBoxCategoriaA = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Limpiar = new System.Windows.Forms.Button();
            this.ContenedorCategoriaA = new System.Windows.Forms.PictureBox();
            this.TextBoxDescripcionA = new System.Windows.Forms.TextBox();
            this.ContenedorStockA = new System.Windows.Forms.PictureBox();
            this.ContenedorClaveA = new System.Windows.Forms.PictureBox();
            this.ContenedorDescripcionA = new System.Windows.Forms.PictureBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.HeaderVender.SuspendLayout();
            this.ContenedorControles.SuspendLayout();
            this.ContedorControles.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorDescripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorClave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImagenProducto)).BeginInit();
            this.PanelAgregar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorPrecioCA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorPVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorCategoriaA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorStockA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorClaveA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorDescripcionA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // HeaderVender
            // 
            this.HeaderVender.BackColor = System.Drawing.Color.White;
            this.HeaderVender.Controls.Add(this.Minimizar);
            this.HeaderVender.Controls.Add(this.LabelResumen);
            this.HeaderVender.Location = new System.Drawing.Point(0, 0);
            this.HeaderVender.Name = "HeaderVender";
            this.HeaderVender.Size = new System.Drawing.Size(1210, 50);
            this.HeaderVender.TabIndex = 1;
            this.HeaderVender.Paint += new System.Windows.Forms.PaintEventHandler(this.HeaderVender_Paint);
            // 
            // Minimizar
            // 
            this.Minimizar.BackgroundImage = global::Reef___Punto_de_Venta.Properties.Resources.Cerrar1;
            this.Minimizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Minimizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Minimizar.ForeColor = System.Drawing.Color.PowderBlue;
            this.Minimizar.Location = new System.Drawing.Point(1190, 0);
            this.Minimizar.Name = "Minimizar";
            this.Minimizar.Size = new System.Drawing.Size(20, 20);
            this.Minimizar.TabIndex = 2;
            this.Minimizar.TabStop = false;
            this.Minimizar.UseVisualStyleBackColor = true;
            this.Minimizar.Click += new System.EventHandler(this.Minimizar_Click);
            // 
            // LabelResumen
            // 
            this.LabelResumen.AutoSize = true;
            this.LabelResumen.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelResumen.ForeColor = System.Drawing.Color.DimGray;
            this.LabelResumen.Location = new System.Drawing.Point(12, 6);
            this.LabelResumen.Name = "LabelResumen";
            this.LabelResumen.Size = new System.Drawing.Size(290, 38);
            this.LabelResumen.TabIndex = 1;
            this.LabelResumen.Text = "Inicio > Productos";
            // 
            // Agregar
            // 
            this.Agregar.BackColor = System.Drawing.Color.White;
            this.Agregar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.Agregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Agregar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Agregar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(200)))), ((int)(((byte)(163)))));
            this.Agregar.Location = new System.Drawing.Point(5, 430);
            this.Agregar.Name = "Agregar";
            this.Agregar.Size = new System.Drawing.Size(161, 66);
            this.Agregar.TabIndex = 4;
            this.Agregar.TabStop = false;
            this.Agregar.Text = "Agregar Producto";
            this.Agregar.UseVisualStyleBackColor = false;
            this.Agregar.Click += new System.EventHandler(this.Agregar_Click);
            // 
            // TextBoxClaveA
            // 
            this.TextBoxClaveA.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.TextBoxClaveA.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextBoxClaveA.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxClaveA.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.TextBoxClaveA.HideSelection = false;
            this.TextBoxClaveA.Location = new System.Drawing.Point(26, 53);
            this.TextBoxClaveA.MaxLength = 100;
            this.TextBoxClaveA.Name = "TextBoxClaveA";
            this.TextBoxClaveA.Size = new System.Drawing.Size(220, 24);
            this.TextBoxClaveA.TabIndex = 3;
            this.TextBoxClaveA.WordWrap = false;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DimGray;
            this.label2.Location = new System.Drawing.Point(20, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 25);
            this.label2.TabIndex = 17;
            this.label2.Text = "Clave: ";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.DimGray;
            this.label3.Location = new System.Drawing.Point(20, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 25);
            this.label3.TabIndex = 15;
            this.label3.Text = "Descripción:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.DimGray;
            this.label5.Location = new System.Drawing.Point(20, 231);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(129, 25);
            this.label5.TabIndex = 21;
            this.label5.Text = "Stock:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TextBoxStockA
            // 
            this.TextBoxStockA.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.TextBoxStockA.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextBoxStockA.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxStockA.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.TextBoxStockA.HideSelection = false;
            this.TextBoxStockA.Location = new System.Drawing.Point(26, 265);
            this.TextBoxStockA.MaxLength = 100;
            this.TextBoxStockA.Name = "TextBoxStockA";
            this.TextBoxStockA.Size = new System.Drawing.Size(220, 24);
            this.TextBoxStockA.TabIndex = 6;
            this.TextBoxStockA.WordWrap = false;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.DimGray;
            this.label6.Location = new System.Drawing.Point(20, 303);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 46);
            this.label6.TabIndex = 24;
            this.label6.Text = "Precio\r\nCompra";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.DimGray;
            this.label7.Location = new System.Drawing.Point(145, 303);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(105, 46);
            this.label7.TabIndex = 25;
            this.label7.Text = "Precio\r\nVenta";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TextBoxPCA
            // 
            this.TextBoxPCA.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.TextBoxPCA.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextBoxPCA.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxPCA.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.TextBoxPCA.HideSelection = false;
            this.TextBoxPCA.Location = new System.Drawing.Point(26, 361);
            this.TextBoxPCA.MaxLength = 100;
            this.TextBoxPCA.Name = "TextBoxPCA";
            this.TextBoxPCA.Size = new System.Drawing.Size(105, 24);
            this.TextBoxPCA.TabIndex = 7;
            this.TextBoxPCA.WordWrap = false;
            // 
            // TextBoxPVA
            // 
            this.TextBoxPVA.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.TextBoxPVA.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextBoxPVA.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxPVA.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.TextBoxPVA.HideSelection = false;
            this.TextBoxPVA.Location = new System.Drawing.Point(149, 361);
            this.TextBoxPVA.MaxLength = 100;
            this.TextBoxPVA.Name = "TextBoxPVA";
            this.TextBoxPVA.Size = new System.Drawing.Size(105, 24);
            this.TextBoxPVA.TabIndex = 8;
            this.TextBoxPVA.WordWrap = false;
            // 
            // ContenedorControles
            // 
            this.ContenedorControles.BackColor = System.Drawing.Color.White;
            this.ContenedorControles.Controls.Add(this.ContedorControles);
            this.ContenedorControles.Controls.Add(this.TextBoxDescripcion);
            this.ContenedorControles.Controls.Add(this.ContenedorDescripcion);
            this.ContenedorControles.Controls.Add(this.LabelDescripcion);
            this.ContenedorControles.Controls.Add(this.TextBoxClave);
            this.ContenedorControles.Controls.Add(this.ContenedorClave);
            this.ContenedorControles.Controls.Add(this.LabelClave);
            this.ContenedorControles.Controls.Add(this.ImagenProducto);
            this.ContenedorControles.Location = new System.Drawing.Point(0, 50);
            this.ContenedorControles.Name = "ContenedorControles";
            this.ContenedorControles.Size = new System.Drawing.Size(1210, 140);
            this.ContenedorControles.TabIndex = 30;
            this.ContenedorControles.Paint += new System.Windows.Forms.PaintEventHandler(this.ContenedorControles_Paint);
            // 
            // ContedorControles
            // 
            this.ContedorControles.Controls.Add(this.Editarbutton);
            this.ContedorControles.Controls.Add(this.Cancelar);
            this.ContedorControles.Controls.Add(this.Remover);
            this.ContedorControles.Controls.Add(this.Buscar);
            this.ContedorControles.Location = new System.Drawing.Point(725, 2);
            this.ContedorControles.Name = "ContedorControles";
            this.ContedorControles.Size = new System.Drawing.Size(481, 135);
            this.ContedorControles.TabIndex = 13;
            // 
            // Editarbutton
            // 
            this.Editarbutton.BackColor = System.Drawing.Color.Transparent;
            this.Editarbutton.FlatAppearance.BorderSize = 0;
            this.Editarbutton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.Editarbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Editarbutton.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Editarbutton.ForeColor = System.Drawing.Color.DimGray;
            this.Editarbutton.Image = global::Reef___Punto_de_Venta.Properties.Resources.ProductosEditar1;
            this.Editarbutton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Editarbutton.Location = new System.Drawing.Point(108, 33);
            this.Editarbutton.Name = "Editarbutton";
            this.Editarbutton.Size = new System.Drawing.Size(90, 70);
            this.Editarbutton.TabIndex = 11;
            this.Editarbutton.TabStop = false;
            this.Editarbutton.Text = "Editar";
            this.Editarbutton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Editarbutton.UseVisualStyleBackColor = false;
            this.Editarbutton.Click += new System.EventHandler(this.Editar_Click);
            // 
            // Cancelar
            // 
            this.Cancelar.BackColor = System.Drawing.Color.Transparent;
            this.Cancelar.FlatAppearance.BorderSize = 0;
            this.Cancelar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.Cancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cancelar.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cancelar.ForeColor = System.Drawing.Color.DimGray;
            this.Cancelar.Image = global::Reef___Punto_de_Venta.Properties.Resources.ProductosCancelar;
            this.Cancelar.Location = new System.Drawing.Point(300, 33);
            this.Cancelar.Name = "Cancelar";
            this.Cancelar.Size = new System.Drawing.Size(90, 70);
            this.Cancelar.TabIndex = 10;
            this.Cancelar.TabStop = false;
            this.Cancelar.Text = "Cancelar";
            this.Cancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Cancelar.UseVisualStyleBackColor = false;
            this.Cancelar.Click += new System.EventHandler(this.Cancelar_Click);
            // 
            // Remover
            // 
            this.Remover.BackColor = System.Drawing.Color.Transparent;
            this.Remover.FlatAppearance.BorderSize = 0;
            this.Remover.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.Remover.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Remover.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Remover.ForeColor = System.Drawing.Color.DimGray;
            this.Remover.Image = global::Reef___Punto_de_Venta.Properties.Resources.ProductosRemover;
            this.Remover.Location = new System.Drawing.Point(204, 33);
            this.Remover.Name = "Remover";
            this.Remover.Size = new System.Drawing.Size(90, 70);
            this.Remover.TabIndex = 6;
            this.Remover.TabStop = false;
            this.Remover.Text = "Remover";
            this.Remover.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Remover.UseVisualStyleBackColor = false;
            this.Remover.Click += new System.EventHandler(this.Remover_Click);
            // 
            // Buscar
            // 
            this.Buscar.BackColor = System.Drawing.Color.Transparent;
            this.Buscar.FlatAppearance.BorderSize = 0;
            this.Buscar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.Buscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Buscar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Buscar.ForeColor = System.Drawing.Color.DimGray;
            this.Buscar.Image = global::Reef___Punto_de_Venta.Properties.Resources.ProductosBuscar;
            this.Buscar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Buscar.Location = new System.Drawing.Point(12, 33);
            this.Buscar.Name = "Buscar";
            this.Buscar.Size = new System.Drawing.Size(90, 70);
            this.Buscar.TabIndex = 4;
            this.Buscar.TabStop = false;
            this.Buscar.Text = "Buscar";
            this.Buscar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.Buscar.UseVisualStyleBackColor = false;
            this.Buscar.Click += new System.EventHandler(this.Buscar_Click);
            // 
            // TextBoxDescripcion
            // 
            this.TextBoxDescripcion.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.TextBoxDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextBoxDescripcion.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxDescripcion.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.TextBoxDescripcion.HideSelection = false;
            this.TextBoxDescripcion.Location = new System.Drawing.Point(136, 100);
            this.TextBoxDescripcion.MaxLength = 100;
            this.TextBoxDescripcion.Name = "TextBoxDescripcion";
            this.TextBoxDescripcion.Size = new System.Drawing.Size(326, 20);
            this.TextBoxDescripcion.TabIndex = 2;
            this.TextBoxDescripcion.WordWrap = false;
            // 
            // ContenedorDescripcion
            // 
            this.ContenedorDescripcion.Image = global::Reef___Punto_de_Venta.Properties.Resources.BuscarContenedor;
            this.ContenedorDescripcion.Location = new System.Drawing.Point(133, 94);
            this.ContenedorDescripcion.Name = "ContenedorDescripcion";
            this.ContenedorDescripcion.Size = new System.Drawing.Size(329, 32);
            this.ContenedorDescripcion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ContenedorDescripcion.TabIndex = 11;
            this.ContenedorDescripcion.TabStop = false;
            // 
            // LabelDescripcion
            // 
            this.LabelDescripcion.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelDescripcion.ForeColor = System.Drawing.Color.DimGray;
            this.LabelDescripcion.Location = new System.Drawing.Point(133, 66);
            this.LabelDescripcion.Name = "LabelDescripcion";
            this.LabelDescripcion.Size = new System.Drawing.Size(129, 25);
            this.LabelDescripcion.TabIndex = 10;
            this.LabelDescripcion.Text = "Descripción:";
            this.LabelDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TextBoxClave
            // 
            this.TextBoxClave.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.TextBoxClave.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextBoxClave.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxClave.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.TextBoxClave.HideSelection = false;
            this.TextBoxClave.Location = new System.Drawing.Point(136, 35);
            this.TextBoxClave.MaxLength = 100;
            this.TextBoxClave.Name = "TextBoxClave";
            this.TextBoxClave.Size = new System.Drawing.Size(215, 24);
            this.TextBoxClave.TabIndex = 1;
            this.TextBoxClave.WordWrap = false;
            // 
            // ContenedorClave
            // 
            this.ContenedorClave.Image = global::Reef___Punto_de_Venta.Properties.Resources.BuscarContenedor;
            this.ContenedorClave.Location = new System.Drawing.Point(133, 31);
            this.ContenedorClave.Name = "ContenedorClave";
            this.ContenedorClave.Size = new System.Drawing.Size(220, 32);
            this.ContenedorClave.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ContenedorClave.TabIndex = 8;
            this.ContenedorClave.TabStop = false;
            // 
            // LabelClave
            // 
            this.LabelClave.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelClave.ForeColor = System.Drawing.Color.DimGray;
            this.LabelClave.Location = new System.Drawing.Point(133, 3);
            this.LabelClave.Name = "LabelClave";
            this.LabelClave.Size = new System.Drawing.Size(98, 25);
            this.LabelClave.TabIndex = 4;
            this.LabelClave.Text = "Clave: ";
            this.LabelClave.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ImagenProducto
            // 
            this.ImagenProducto.Image = global::Reef___Punto_de_Venta.Properties.Resources.ImageNull;
            this.ImagenProducto.Location = new System.Drawing.Point(1, 3);
            this.ImagenProducto.Name = "ImagenProducto";
            this.ImagenProducto.Size = new System.Drawing.Size(130, 134);
            this.ImagenProducto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ImagenProducto.TabIndex = 0;
            this.ImagenProducto.TabStop = false;
            this.ImagenProducto.Click += new System.EventHandler(this.ImagenProducto_Click);
            this.ImagenProducto.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ImagenProducto_MouseClick);
            // 
            // PanelAgregar
            // 
            this.PanelAgregar.Controls.Add(this.TextBoxPCA);
            this.PanelAgregar.Controls.Add(this.ContenedorPrecioCA);
            this.PanelAgregar.Controls.Add(this.TextBoxPVA);
            this.PanelAgregar.Controls.Add(this.TextBoxStockA);
            this.PanelAgregar.Controls.Add(this.ContenedorPVA);
            this.PanelAgregar.Controls.Add(this.TextBoxCategoriaA);
            this.PanelAgregar.Controls.Add(this.label1);
            this.PanelAgregar.Controls.Add(this.Limpiar);
            this.PanelAgregar.Controls.Add(this.Agregar);
            this.PanelAgregar.Controls.Add(this.label6);
            this.PanelAgregar.Controls.Add(this.label7);
            this.PanelAgregar.Controls.Add(this.ContenedorCategoriaA);
            this.PanelAgregar.Controls.Add(this.TextBoxDescripcionA);
            this.PanelAgregar.Controls.Add(this.ContenedorStockA);
            this.PanelAgregar.Controls.Add(this.TextBoxClaveA);
            this.PanelAgregar.Controls.Add(this.label2);
            this.PanelAgregar.Controls.Add(this.ContenedorClaveA);
            this.PanelAgregar.Controls.Add(this.label5);
            this.PanelAgregar.Controls.Add(this.label3);
            this.PanelAgregar.Controls.Add(this.ContenedorDescripcionA);
            this.PanelAgregar.Location = new System.Drawing.Point(876, 192);
            this.PanelAgregar.Name = "PanelAgregar";
            this.PanelAgregar.Size = new System.Drawing.Size(335, 508);
            this.PanelAgregar.TabIndex = 31;
            // 
            // ContenedorPrecioCA
            // 
            this.ContenedorPrecioCA.Image = global::Reef___Punto_de_Venta.Properties.Resources.BuscarContenedor;
            this.ContenedorPrecioCA.Location = new System.Drawing.Point(22, 357);
            this.ContenedorPrecioCA.Name = "ContenedorPrecioCA";
            this.ContenedorPrecioCA.Size = new System.Drawing.Size(110, 32);
            this.ContenedorPrecioCA.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ContenedorPrecioCA.TabIndex = 26;
            this.ContenedorPrecioCA.TabStop = false;
            // 
            // ContenedorPVA
            // 
            this.ContenedorPVA.Image = global::Reef___Punto_de_Venta.Properties.Resources.BuscarContenedor;
            this.ContenedorPVA.Location = new System.Drawing.Point(147, 357);
            this.ContenedorPVA.Name = "ContenedorPVA";
            this.ContenedorPVA.Size = new System.Drawing.Size(110, 32);
            this.ContenedorPVA.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ContenedorPVA.TabIndex = 27;
            this.ContenedorPVA.TabStop = false;
            // 
            // TextBoxCategoriaA
            // 
            this.TextBoxCategoriaA.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.TextBoxCategoriaA.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextBoxCategoriaA.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxCategoriaA.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.TextBoxCategoriaA.HideSelection = false;
            this.TextBoxCategoriaA.Location = new System.Drawing.Point(26, 192);
            this.TextBoxCategoriaA.MaxLength = 100;
            this.TextBoxCategoriaA.Name = "TextBoxCategoriaA";
            this.TextBoxCategoriaA.Size = new System.Drawing.Size(220, 24);
            this.TextBoxCategoriaA.TabIndex = 5;
            this.TextBoxCategoriaA.WordWrap = false;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DimGray;
            this.label1.Location = new System.Drawing.Point(20, 160);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 25);
            this.label1.TabIndex = 32;
            this.label1.Text = "Categoría:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Limpiar
            // 
            this.Limpiar.BackColor = System.Drawing.Color.White;
            this.Limpiar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.Limpiar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Limpiar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Limpiar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(200)))), ((int)(((byte)(163)))));
            this.Limpiar.Location = new System.Drawing.Point(170, 430);
            this.Limpiar.Name = "Limpiar";
            this.Limpiar.Size = new System.Drawing.Size(161, 66);
            this.Limpiar.TabIndex = 21;
            this.Limpiar.TabStop = false;
            this.Limpiar.Text = "Limpiar";
            this.Limpiar.UseVisualStyleBackColor = false;
            this.Limpiar.Click += new System.EventHandler(this.Limpiar_Click);
            // 
            // ContenedorCategoriaA
            // 
            this.ContenedorCategoriaA.Image = global::Reef___Punto_de_Venta.Properties.Resources.BuscarContenedor;
            this.ContenedorCategoriaA.Location = new System.Drawing.Point(22, 189);
            this.ContenedorCategoriaA.Name = "ContenedorCategoriaA";
            this.ContenedorCategoriaA.Size = new System.Drawing.Size(230, 32);
            this.ContenedorCategoriaA.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ContenedorCategoriaA.TabIndex = 20;
            this.ContenedorCategoriaA.TabStop = false;
            // 
            // TextBoxDescripcionA
            // 
            this.TextBoxDescripcionA.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.TextBoxDescripcionA.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextBoxDescripcionA.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxDescripcionA.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.TextBoxDescripcionA.HideSelection = false;
            this.TextBoxDescripcionA.Location = new System.Drawing.Point(26, 122);
            this.TextBoxDescripcionA.MaxLength = 100;
            this.TextBoxDescripcionA.Name = "TextBoxDescripcionA";
            this.TextBoxDescripcionA.Size = new System.Drawing.Size(295, 24);
            this.TextBoxDescripcionA.TabIndex = 4;
            this.TextBoxDescripcionA.WordWrap = false;
            // 
            // ContenedorStockA
            // 
            this.ContenedorStockA.Image = global::Reef___Punto_de_Venta.Properties.Resources.BuscarContenedor;
            this.ContenedorStockA.Location = new System.Drawing.Point(22, 261);
            this.ContenedorStockA.Name = "ContenedorStockA";
            this.ContenedorStockA.Size = new System.Drawing.Size(230, 32);
            this.ContenedorStockA.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ContenedorStockA.TabIndex = 22;
            this.ContenedorStockA.TabStop = false;
            // 
            // ContenedorClaveA
            // 
            this.ContenedorClaveA.Image = global::Reef___Punto_de_Venta.Properties.Resources.BuscarContenedor;
            this.ContenedorClaveA.Location = new System.Drawing.Point(21, 49);
            this.ContenedorClaveA.Name = "ContenedorClaveA";
            this.ContenedorClaveA.Size = new System.Drawing.Size(230, 32);
            this.ContenedorClaveA.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ContenedorClaveA.TabIndex = 15;
            this.ContenedorClaveA.TabStop = false;
            // 
            // ContenedorDescripcionA
            // 
            this.ContenedorDescripcionA.Image = global::Reef___Punto_de_Venta.Properties.Resources.BuscarContenedor;
            this.ContenedorDescripcionA.Location = new System.Drawing.Point(22, 118);
            this.ContenedorDescripcionA.Name = "ContenedorDescripcionA";
            this.ContenedorDescripcionA.Size = new System.Drawing.Size(305, 32);
            this.ContenedorDescripcionA.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ContenedorDescripcionA.TabIndex = 15;
            this.ContenedorDescripcionA.TabStop = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(200)))), ((int)(((byte)(163)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(200)))), ((int)(((byte)(163)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Location = new System.Drawing.Point(3, 193);
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(200)))), ((int)(((byte)(163)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.Size = new System.Drawing.Size(872, 508);
            this.dataGridView1.TabIndex = 3;
            // 
            // Productos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1210, 700);
            this.Controls.Add(this.ContenedorControles);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.HeaderVender);
            this.Controls.Add(this.PanelAgregar);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(156, 60);
            this.Name = "Productos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Productos";
            this.Load += new System.EventHandler(this.Productos_Load);
            this.HeaderVender.ResumeLayout(false);
            this.HeaderVender.PerformLayout();
            this.ContenedorControles.ResumeLayout(false);
            this.ContenedorControles.PerformLayout();
            this.ContedorControles.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorDescripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorClave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImagenProducto)).EndInit();
            this.PanelAgregar.ResumeLayout(false);
            this.PanelAgregar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorPrecioCA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorPVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorCategoriaA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorStockA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorClaveA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorDescripcionA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel HeaderVender;
        private System.Windows.Forms.Button Minimizar;
        private System.Windows.Forms.Label LabelResumen;
        private System.Windows.Forms.Button Agregar;
        private System.Windows.Forms.TextBox TextBoxClaveA;
        private System.Windows.Forms.PictureBox ContenedorClaveA;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox ContenedorDescripcionA;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TextBoxStockA;
        private System.Windows.Forms.PictureBox ContenedorStockA;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox ContenedorPrecioCA;
        private System.Windows.Forms.PictureBox ContenedorPVA;
        private System.Windows.Forms.TextBox TextBoxPCA;
        private System.Windows.Forms.TextBox TextBoxPVA;
        private System.Windows.Forms.Panel ContenedorControles;
        private System.Windows.Forms.Panel ContedorControles;
        public System.Windows.Forms.Button Cancelar;
        private System.Windows.Forms.Button Remover;
        private System.Windows.Forms.Button Buscar;
        private System.Windows.Forms.TextBox TextBoxDescripcion;
        private System.Windows.Forms.PictureBox ContenedorDescripcion;
        private System.Windows.Forms.Label LabelDescripcion;
        private System.Windows.Forms.TextBox TextBoxClave;
        private System.Windows.Forms.PictureBox ContenedorClave;
        private System.Windows.Forms.Label LabelClave;
        private System.Windows.Forms.PictureBox ImagenProducto;
        private System.Windows.Forms.Panel PanelAgregar;
        private System.Windows.Forms.TextBox TextBoxDescripcionA;
        private System.Windows.Forms.Button Limpiar;
        private System.Windows.Forms.PictureBox ContenedorCategoriaA;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TextBoxCategoriaA;
        private System.Windows.Forms.Button Editarbutton;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}