﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reef___Punto_de_Venta.Vistas
{
    public partial class Election : Form
    {
        int id;
        Productos P;
        public Election(int i,Productos p)
        {
            InitializeComponent();
            id = i;
            P = p;
        }

        MyConnection con = new MyConnection();
        Image im;               
       
        private void Election_FormClosed(object sender, FormClosedEventArgs e)
        {
            P.llenar();
        }

        private void Minimizar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ofd.Filter = "Archivos de Imagen JPG|*.jpg|Archivos PNG|*.png|Todos los archivos|*.*";
            DialogResult resultado = ofd.ShowDialog();

            if (resultado == DialogResult.OK)
            {
                im = Image.FromFile(ofd.FileName);
                Helper.SaveImageCapture(Imagen.ResizeImage(im), id);

                this.Close();
            }
            else
            {
                MessageBox.Show("No has elegido Imagen");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
            Cam showFrameCam = new Cam(id,P);
            showFrameCam.Show();
        }
    }
}
