﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reef___Punto_de_Venta.Vistas
{
    public partial class Ofertas : Form
    {
        public Ofertas()
        {
            InitializeComponent();
        }

        private void Minimiza_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void Minimizar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Ofertas_Load(object sender, EventArgs e)
        {
            llenad();
            int[] f = new int[dataGridView1.RowCount];
            int[] c = new int[dataGridView2.RowCount];
            int[] s = new int[dataGridView1.RowCount];
            double[] p = new double[dataGridView1.RowCount];

            for (int i = 0; i < dataGridView1.RowCount - 1; i++)
            {
                f[i] = Convert.ToInt32(dataGridView1.Rows[i].Cells[0].Value.ToString());
                //listBox1.Items.Add(f[i].ToString());
                //c[i] = int.Parse(dataGridView1.Rows[i].Cells[0].Value.ToString());
                //listBox2.Items.Add(c[i].ToString());
                p[i] = Convert.ToDouble(dataGridView1.Rows[i].Cells[1].Value.ToString());
                //listBox3.Items.Add(p[i].ToString());
                //s[i] = Convert.ToInt32(dataGridView1.Rows[i].Cells[2].Value.ToString());
            }

            for (int l = 0; l < dataGridView2.RowCount - 1; l++)
            {
                c[l] = int.Parse(dataGridView2.Rows[l].Cells[0].Value.ToString());
                //listBox2.Items.Add(c[l].ToString());
            }

            int[] promos = new int[dataGridView1.RowCount * 2];
            checa(f, c, p, promos);

            //string[] b={"id_producto","descripcion","img"};
            //Sentencias.llenar(dataGridView1, "productos", b);
        }

        private void creaimg(int[] pr, Label[] l, PictureBox[] pi)
        {
            string[] campos = { "descripcion", "img" };
            StringBuilder condision = new StringBuilder();
            for (int i = 0; i < pi.Length; i++)
            {
                if (pr[i] == 0)
                {
                    condision.Append("productos where productos.id_producto=" + pr[i + 1]);
                    Sentencias.llenar(dataGridView1, condision.ToString(), campos);
                    byte[] imageBuffer = (byte[])dataGridView1.Rows[0].Cells["img"].Value;
                    System.IO.MemoryStream ms = new System.IO.MemoryStream(imageBuffer);
                    pi[i].Image = Image.FromStream(ms);
                    l[i].Text = dataGridView1.Rows[0].Cells["descripcion"].Value.ToString();
                    condision.Clear();
                }
                else
                {
                    condision.Append("productos where productos.id_producto=" + pr[i]);
                    Sentencias.llenar(dataGridView1, condision.ToString(), campos);
                    byte[] imageBuffer = (byte[])dataGridView1.Rows[0].Cells["img"].Value;
                    System.IO.MemoryStream ms = new System.IO.MemoryStream(imageBuffer);
                    pi[i].Image = Image.FromStream(ms);
                    l[i].Text = dataGridView1.Rows[0].Cells["descripcion"].Value.ToString();
                    condision.Clear();
                }
            }
        }

        private void llenad()
        {
            string[] busca = { "id_producto", "productos.precio_venta-productos.precio_compra as ganacia" };
            Sentencias.llenar(dataGridView1, "productos", busca);

            string[] cam = { "productos.id_producto", "productos.descripcion" };
            string[] v = { "ventas", "detalle_venta", "productos" };
            string condition = "and (productos.id_producto=detalle_venta.id_producto) group by productos.id_producto";
            string g = "productos.id_producto order by productos.id_producto asc";
            string h = DateTime.Now.ToString("yyMMdd"), xc = DateTime.Now.AddDays(-0).ToString("yyMMdd");
            //            string h = "160707", xc = "160808";
            string u = "productos.precio_venta-productos.precio_compra";
            //Sentencias.grafica(xc, h, dataGridView2, "propductos", cam, u, "ganancia", v, condition, g);
            //Sentencias.llenar(dataGridView2, "detalle_venta", cam);
            Sentencias.sentenciax(h, xc, "productos", cam, v, condition, dataGridView2);
        }

        private void checa(int[] f, int[] c, double[] p, int[] pro)
        {
            try
            {
                int au = 0;
                for (int i = 0; i < f.Length; i++)
                {
                    StringBuilder pr = new StringBuilder();
                    int con = 0;
                    for (int j = 0; j < c.Length; j++)
                    {
                        if (f[i] == c[j])
                        {
                            con++;
                        }
                    }
                    if (con == 0)
                    {
                        double mg = 0;
                        int gu = 0;
                        for (int k = 0; k < c.Length; k++)
                        {
                            if (i != k)
                            {
                                if ((p[i] + p[k]) > mg)
                                {
                                    mg = p[i] + p[k];
                                    gu = k;
                                }
                            }
                            else
                            {
                                if ((p[i] + p[k + 1]) > mg)
                                {
                                    mg = (p[i] + p[k + 1]);
                                    gu = k;
                                }
                                k++;
                            }
                        }
                        if (gu == 0)
                        {
                            //listBox4.Items.Add("" + f[i] + " , " + c[gu] + " = " + mg);
                            pr.Append(f[i] + " , " + c[gu] + " = " + mg);
                            if (au == 0)
                            {
                                pro[0] = f[i];
                                pro[au++] = c[gu - 1];
                            }
                            else
                            {
                                pro[au++] = f[i];
                                pro[au++] = c[gu - 1];
                            }
                        }
                        else
                        {
                            //listBox4.Items.Add("" + f[i] + " , " + c[gu - 1] + " = " + mg);
                            pr.Append(f[i] + " , " + c[gu - 1] + " = " + mg);
                            if (au == 0)
                            {
                                pro[0] = f[i];
                                pro[1] = c[gu - 1];
                                au += 2;
                            }
                            else
                            {
                                pro[au] = f[i];
                                pro[au++] = c[gu - 1];
                                au++;
                            }
                        }

                    }
                }
                PictureBox[] pic = { pictureBox1, pictureBox2, pictureBox3, pictureBox4, pictureBox5, pictureBox6, pictureBox7, pictureBox8 };
                Label[] lab = { label1, label2, label3, label4, label5, label6, label7, label8 };
                creaimg(pro, lab, pic);
            }

            catch (Exception)
            {

            }
        }
    }
}
