﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Reef___Punto_de_Venta.Properties;

namespace Reef___Punto_de_Venta.Vistas
{
    public partial class Vender : Form
    {
        DataGridView dt = new DataGridView();//tabla con id e imagenes
        object s;
        EventArgs E;
        bool editar=false;

        public Vender()
        {
            InitializeComponent();
        }

        private void Minimizar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Vender_Load(object sender, EventArgs e)
        {
            s = sender;
            E = e;
            PanelBusqueda.Hide();
            PanelEditar.Hide();
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();
            dataGridView1.Columns.Add(new DataGridViewTextBoxColumn());
            dataGridView1.Columns.Add(new DataGridViewTextBoxColumn());
            dataGridView1.Columns.Add(new DataGridViewTextBoxColumn());
            dataGridView1.Columns.Add(new DataGridViewTextBoxColumn());
            dataGridView1.Columns.Add(new DataGridViewTextBoxColumn());
            dataGridView1.Columns.Add(new DataGridViewTextBoxColumn());
            dataGridView1.Columns[0].Name = "Clave";
            dataGridView1.Columns[0].Width = 100;
            dataGridView1.Columns[1].Name = "Descripcion";
            dataGridView1.Columns[1].Width = 500;
            dataGridView1.Columns[2].Name = "Precio";
            dataGridView1.Columns[2].Width = 125;
            dataGridView1.Columns[3].Name = "Cantidad";
            dataGridView1.Columns[3].Width = 125;
            dataGridView1.Columns[4].Name = "Descuento";
            dataGridView1.Columns[4].Width = 110;
            dataGridView1.Columns[5].Name = "Importe";
            dataGridView1.Columns[5].Width = 200;
            dataGridView1.BorderStyle = BorderStyle.Fixed3D;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.Controls.Add(dt);
            LabelTotalPrecio.Text = "0.00";
            LabelUSDPrecio.Text = "0.00";
            label7.Text = (Convert.ToInt32(Sentencias.max("ventas", "id_venta")) + 1).ToString("000000");
        }

        private void ContenedorControles_Paint(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics, this.ContenedorControles.ClientRectangle, Color.FromArgb(165, 182, 195), ButtonBorderStyle.Solid);
        }

        private void Buscar_Click(object sender, EventArgs e)
        {
            mostrarbuscar();
            llenardataimg(dt, dataGridView2);
            Labelbusquedaeditar.Hide();
            TextBoxCantidad.Focus();
        }

        private void CerrarBusqueda_Click(object sender, EventArgs e)
        {
            PanelBusqueda.Hide();
        }

        private void Descuento_Click(object sender, EventArgs e)
        {
            mostrareditar();
        }

        private void BotonModificarCanelar_Click(object sender, EventArgs e)
        {
            TextBoxCantidad.Clear();
            TextBoxPrecio.Clear();
            TextBoxDescuento.Clear();
            PanelEditar.Hide();
        }

        private void BotonVender_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count > 1)
            {
                for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
                {
                    string[] valores = { i.ToString(), label7.Text,dataGridView1.Rows[i].Cells[0].Value.ToString(),dataGridView1.Rows[i].Cells[2].Value.ToString(),dataGridView1.Rows[i].Cells[3].Value.ToString(), dataGridView1.Rows[i].Cells[4].Value.ToString() , dataGridView1.Rows[i].Cells[5].Value.ToString() };
                    Sentencias.insertar("detalle_venta", Sentencias.detalle_ventas, valores);
                    Sentencias.Vender("productos", "stock", dataGridView1.Rows[i].Cells[3].Value.ToString(), "id_producto", dataGridView1.Rows[i].Cells[0].Value.ToString()); 
                }
                string[] valores1 ={ label7.Text,Login.ini.IDuser.ToString(), DateTime.Now.ToString("yyMMdd"), LabelTotalPrecio.Text.Replace(",","") };
                Sentencias.insertar("ventas", Sentencias.ventas, valores1);
                Vender_Load(s, E);
            }
        }
        private void mostrarbuscar()
        {
            if (PanelBusqueda.Visible == false)
            {
                PanelBusqueda.Visible = true;
            }
        }
        private void mostrareditar()
        {
            if (PanelEditar.Visible == false)
            {
                PanelEditar.Visible = true;
            }
        }

        private void Editar_Click(object sender, EventArgs e)
        {
            if (PanelBusqueda.Visible != true)
            {
                if (dataGridView1.SelectedRows.Count > 0 && dataGridView1.CurrentRow.Cells[0].Value != null)
                {
                    mostrareditar();
                    TextBoxCantidad.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
                    TextBoxPrecio.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
                    TextBoxDescuento.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();
                    ImagenSeleccionada.Image = Imagen.CargarImagen("id_producto", dataGridView1.CurrentRow.Cells[0].Value.ToString());
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BusquedaCategoria.Clear();
            BusquedaClave.Clear();
            BusquedaDescripcion.Clear();
        }
        private void llenardataimg(DataGridView dt, DataGridView dm)
        {
            dm.Columns.Clear();
            dm.Rows.Clear();
            string[] campos = new string[Sentencias.productos.Length - 1];
            Array.Copy(Sentencias.productos, campos, campos.Length);
            string[] camposbusca = { "id_producto", "descripcion" };
            string[] valores = { TextBoxClave.Text, TextBoxDescripcion.Text };
            Sentencias.buscar("productos", dt, Sentencias.productos, camposbusca, valores);
            dm.BorderStyle = BorderStyle.Fixed3D;
            dm.CellBorderStyle = DataGridViewCellBorderStyle.None;
            string[] images = new string[dt.Rows.Count - 1];
            for (int i = 0; i < dt.Rows.Count - 1; i++)
            {
                images[i] = dt.Rows[i].Cells[0].Value.ToString();
            }
            dm.Columns.Add(new DataGridViewImageColumn());
            dm.Columns[0].Width = 229;
            dm.Columns.Add(new DataGridViewImageColumn());
            dm.Columns[1].Width = 228;
            dm.Columns.Add(new DataGridViewImageColumn());
            dm.Columns[2].Width = 229;
            dm.Columns.Add(new DataGridViewImageColumn());
            dm.Columns[3].Width = 229;
            dm.Columns[0].Name = ""; dm.Columns[1].Name = ""; dm.Columns[2].Name = ""; dm.Columns[3].Name = "";
            string cmp = "id_producto";
            for (int i = 0; i < images.Length / 4; i++)
            {
                dm.Rows.Add(Imagen.CargarImagen(cmp, images[4 * i]), Imagen.CargarImagen(cmp, images[4 * i + 1]), Imagen.CargarImagen(cmp, images[4 * i + 2]), Imagen.CargarImagen(cmp, images[4 * i + 3]));
                dm.Rows[i].Height = 242;
            }
            DataGridViewRow r = new DataGridViewRow();
            r.Height = 242;
            for (int i = 0; i < images.Length % 4; i++)
            {
                r.Cells.Add(new DataGridViewImageCell { Value = Imagen.CargarImagen(cmp, images[4 * (images.Length / 4) + i]) });
            }
            for (int i = 0; i < 4 - images.Length % 4 && images.Length % 4 > 0; i++)
            {
                r.Cells.Add(new DataGridViewImageCell { Value = Resources.ImageNull });
            }
            dm.Rows.Add(r);
        }

        private void Aceptar_Click(object sender, EventArgs e)
        {
            TextBoxCantidad.Text = "0";
            TextBoxPrecio.Text = dt.CurrentRow.Cells["precio_venta"].Value.ToString();
            TextBoxDescuento.Text = "0";
            PanelEditar.Show();
            ImagenSeleccionada.Image = Imagen.CargarImagen("id_producto", dt.CurrentRow.Cells["id_producto"].Value.ToString());
            TextBoxCantidad.Clear();
            TextBoxCantidad.Focus();
        }

        private void dataGridView2_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            int x = dataGridView2.CurrentCell.ColumnIndex;
            int y = dataGridView2.CurrentCell.RowIndex;
            if (y * 4 + x < dt.Rows.Count - 1)
            {
                dt.CurrentCell = dt[0, (y * 4) + x];
                int y1 = dt.CurrentCell.RowIndex;
                BusquedaClave.Text = dt.Rows[y1].Cells[0].Value.ToString();
                BusquedaDescripcion.Text = dt.Rows[y1].Cells[2].Value.ToString();
                BusquedaCategoria.Text = dt.Rows[y1].Cells[1].Value.ToString();
            }
            else
            {
                BusquedaClave.Text = "Clave no existente";
                BusquedaDescripcion.Text = "Descripcion no existente";
                BusquedaCategoria.Text = "Categoria no existente";
            }
        }

        private void EditAceButton_Click(object sender, EventArgs e)
        {
            if (PanelBusqueda.Visible == true)
            {
                if (TextBoxCantidad.Text != "" && TextBoxPrecio.Text != "" && TextBoxDescuento.Text != "")
                {
                    string[] textos = { BusquedaClave.Text, BusquedaDescripcion.Text, TextBoxPrecio.Text, TextBoxCantidad.Text, TextBoxDescuento.Text, importe(TextBoxPrecio.Text, TextBoxCantidad.Text, TextBoxDescuento.Text) };
                    if (editar != true)
                    {
                        dataGridView1.Rows.Add(textos[0], textos[1], textos[2], textos[3], textos[4], textos[5]);

                    }
                    else
                    {
                        for (int i = 0; i < dataGridView1.CurrentRow.Cells.Count; i++)
                        {
                            dataGridView1.CurrentRow.Cells[i].Value = textos[i];
                            editar = false;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Deben estar llenos todos los campos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

            }
            else
            {
                dataGridView1.CurrentRow.Cells[3].Value = TextBoxCantidad.Text;
                dataGridView1.CurrentRow.Cells[2].Value = TextBoxPrecio.Text;
                dataGridView1.CurrentRow.Cells[4].Value = TextBoxDescuento.Text;
                dataGridView1.CurrentRow.Cells[5].Value = importe(TextBoxPrecio.Text,TextBoxCantidad.Text, TextBoxDescuento.Text);
                
            }
            PanelBusqueda.Hide();
            PanelEditar.Hide();
            LabelTotalPrecio.Text = suma();
            LabelUSDPrecio.Text = dollar(LabelTotalPrecio.Text);
            TextBoxClave.Clear();
            TextBoxClave.Focus();
        }
        private string importe(string precio, string cantidad, string descuento)
        {
            double prec = Convert.ToDouble(precio);
            double cant = Convert.ToDouble(cantidad);
            double desc = Convert.ToDouble(descuento);
            desc = desc / 100 * prec*cant;
            double imp = prec * cant;
            imp = imp - desc;
            return imp.ToString("N2").Replace(",","");
        }

        private string suma()
        {
            double res = 0;
            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                string valors = dataGridView1.Rows[i].Cells["importe"].Value.ToString();
                if (valors != "")
                {
                    res += double.Parse(valors);
                }
            }
            return res.ToString("N2")/*.Replace(",", "")*/;
        }
        private string dollar(string importe)
        {
            double Dollar = Convert.ToDouble(importe);
            Dollar = Dollar * 0.05390;
            return Dollar.ToString("N2").Replace(",", "");//CODIGO CHIDORI CONTEMPLAR GUARDAR
        }

        private void Remover_Click(object sender, EventArgs e)
        {
            if (this.dataGridView1.SelectedRows.Count > 0&&dataGridView1.CurrentRow.Cells[0].Value!=null)
            {
                dataGridView1.Rows.RemoveAt(this.dataGridView1.CurrentRow.Index);
            }
        }

        private void BotonModificar_Click(object sender, EventArgs e)
        {
            
            if (PanelBusqueda.Visible == true)
            {
                PanelEditar.Hide();
            }
            else
            {
                PanelBusqueda.Hide();
                limpiardata(dataGridView2);
                Labelbusquedaeditar.Show();
                PanelBusqueda.Show();
                PanelEditar.Hide();
                editar = true;
            }

            editar = true;
        }
        private void limpiardata(DataGridView d)
        {
            d.Rows.Clear();
        }

        private void Cancelar_Click(object sender, EventArgs e)
        {
            if (PanelEditar.Visible != true)
            {
                if (PanelBusqueda.Visible == true)
                {
                    PanelBusqueda.Hide();
                }
                else
                {
                    if (MessageBox.Show("¿Desea cancelar la venta?", "Cancelar Venta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        this.Close();
                    }
                }
            }
        }

        private void TextBoxClave_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                //Realizar una venta chidori
                Buscar.PerformClick();
            }
        }

        private void TextBoxDescripcion_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                //Realizar una venta chidori
            }
        }

        private void TextBoxCantidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                EditAceButton.PerformClick();
            }
        }
    }
}
