﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reef___Punto_de_Venta.Vistas
{
    public partial class Reportes : Form
    {
        public Reportes()
        {
            InitializeComponent();
        }

        private void Reportes_Load(object sender, EventArgs e)
        {
            
        }

        private void HeaderVender_Paint(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics, this.HeaderVender.ClientRectangle, Color.FromArgb(165, 182, 195), ButtonBorderStyle.Solid);
        }

        private void BotonGenerar_Click(object sender, EventArgs e)
        {
            string s = "";
            if (comboBoxSeleccion.SelectedItem != null)
            {
                s = comboBoxSeleccion.SelectedItem.ToString();
            }
            else
            {
                MessageBox.Show("Primero seleccione un reporte para usar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if ( s!= "")
            {
                string txt="";
                switch (s)
                {
                    case "Ventas - Diario":
                        txt = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                        txt = txt.Remove(0, 6);//Remueve la parte "file:\"
                        txt = txt.Remove(txt.Length - 9, 9);
                        txt += "VentaDiaria.rpt";
                        crystalReportViewer1.ReportSource = txt;
                        break;
                    case "Ventas - Mensual":
                        txt = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                        txt = txt.Remove(0, 6);//Remueve la parte "file:\"
                        txt = txt.Remove(txt.Length - 9, 9);
                        txt += "VentaMensual.rpt";
                        crystalReportViewer1.ReportSource = txt;
                        break;
                }
            }
            else
            {
                MessageBox.Show("Primero seleccione un reporte para usar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void Minimizar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
