﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reef___Punto_de_Venta.Vistas
{
    public partial class Inventario : Form
    {
        public Inventario()
        {
            InitializeComponent();
        }

        private void Minimiza_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void HeaderVender_Paint(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics, this.HeaderVender.ClientRectangle, Color.FromArgb(165, 182, 195), ButtonBorderStyle.Solid);
            string[] campos = { "id_producto", "descripcion", "stock" };
            string[] AS = { "Clave", "Producto", "Existencias" };
            Sentencias.llenar(dataGridView1,"productos",campos,AS);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
