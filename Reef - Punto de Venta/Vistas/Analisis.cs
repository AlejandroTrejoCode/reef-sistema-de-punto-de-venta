﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reef___Punto_de_Venta.Vistas
{
    public partial class Analisis : Form
    {
        public Analisis()
        {
            InitializeComponent();
        }

        private void Minimiza_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void HeaderVender_Paint(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics, this.HeaderVender.ClientRectangle, Color.FromArgb(165, 182, 195), ButtonBorderStyle.Solid);
        }

        private void Analisis_Load(object sender, EventArgs e)
        {
            vgrafica(0, 1);
        }

        private void vgrafica(int dias, int s)
        {
            string[] c = { "detalle_venta.id_producto", "productos.descripcion", "ventas.fecha_ven" };
            string[] v = { "ventas", "detalle_venta", "productos" };
            string condition = "and (detalle_venta.id_venta=ventas.id_venta) and detalle_venta.id_producto=productos.id_producto";
            string g = "detalle_venta.id_producto";
            string f = DateTime.Now.ToString("yyMMdd"), i = DateTime.Now.AddDays(-dias).ToString("yyMMdd");
            if (s == 1)
            {
                Sentencias.grafica(i, f, dataGridView1, "ventas", c, "detalle_venta.cantidad", "total", v, condition, g);
            }
            else if (s == 2)
            {
                Sentencias.grafica(i, f, dataGridView2, "ventas1", c, "detalle_venta.cantidad", "total", v, condition, g);
            }
            else if (s == 3)
            {
                Sentencias.grafica(i, f, dataGridView3, "ventas", c, "detalle_venta.cantidad", "total", v, condition, g);
            }
            sgrafica(s);
        }

        private void pgrafica(int dias, int s)
        {
            //select ventas.fecha_ven as fecha, count(detalle_venta.id_venta) as ventasm, sum(productos.precio_venta*detalle_venta.cantidad) as ganancia from ventas, productos, detalle_venta where (ventas.fecha_ven between 160706 and 160806) and (detalle_venta.id_venta=ventas.id_venta) and detalle_venta.id_producto=productos.id_producto group by ventas.fecha_ven;
            string[] c = { "ventas.fecha_ven as fecha", "count(detalle_venta.id_venta) as ventasm" };
            string[] v = { "ventas", "detalle_venta", "productos" };
            string condition = "and (detalle_venta.id_venta=ventas.id_venta) and detalle_venta.id_producto=productos.id_producto";
            string g = "ventas.fecha_ven";
            string sum = "detalle_venta.importe";
            string f = DateTime.Now.ToString("yyMMdd"), i = DateTime.Now.AddDays(-dias).ToString("yyMMdd");
            if (s == 4)
            {
                Sentencias.grafica(i, f, dataGridView4, "ventas", c, sum, "ganancia", v, condition, g);
            }
            else if (s == 5)
            {
                Sentencias.grafica(i, f, dataGridView5, "ventas1", c, sum, "ganancia", v, condition, g);
            }
            else if (s == 6)
            {
                Sentencias.grafica(i, f, dataGridView6, "ventas", c, sum, "ganancia", v, condition, g);
            }
            sgrafica(s);
        }

        //n
        private void sgrafica(int s)
        {
            switch (s)
            {
                case 1:
                    Vdia.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
                    if (dataGridView1.RowCount > 0)
                    {
                        int c = 0;
                        while (c < 10 & c < dataGridView1.RowCount - 1)
                        {
                            string x = dataGridView1.Rows[c].Cells["descripcion"].Value.ToString();
                            string y = dataGridView1.Rows[c].Cells["total"].Value.ToString();
                            Vdia.Series[0].Points.AddXY(x, y);
                            c++;
                        }
                        //for (int p = 0; p < dataGridView1.RowCount - 1; p++)
                        //{
                        //    string x = dataGridView1.Rows[p].Cells["descripcion"].Value.ToString();
                        //    string y = dataGridView1.Rows[p].Cells["total"].Value.ToString();
                        //    Vdia.Series[0].Points.AddXY(x, y);
                        //}   
                    }
                    vgrafica(7, 2);
                    break;
                case 2:
                    VSemanal.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
                    if (dataGridView2.RowCount > 0)
                    {
                        int c = 0;
                        while (c < 10 & c < dataGridView2.RowCount - 1)
                        {
                            string x = dataGridView2.Rows[c].Cells["descripcion"].Value.ToString();
                            string y = dataGridView2.Rows[c].Cells["total"].Value.ToString();
                            VSemanal.Series[0].Points.AddXY(x, y);
                            c++;
                        }
                        //for (int p = 0; p < dataGridView2.RowCount - 1; p++)
                        //{
                        //    string x1 = dataGridView2.Rows[p].Cells["descripcion"].Value.ToString();
                        //    string y1 = dataGridView2.Rows[p].Cells["total"].Value.ToString();
                        //    VSemanal.Series[0].Points.AddXY(x1, y1);
                        //}   
                    }
                    vgrafica(30, 3);
                    break;
                case 3:
                    VMensual.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
                    if (dataGridView3.RowCount > 0)
                    {
                        int c = 0;
                        while (c < 10 & c < dataGridView3.RowCount - 1)
                        {
                            string x = dataGridView3.Rows[c].Cells["descripcion"].Value.ToString();
                            string y = dataGridView3.Rows[c].Cells["total"].Value.ToString();
                            VMensual.Series[0].Points.AddXY(x, y);
                            c++;
                        }
                        //for (int p = 0; p < dataGridView3.RowCount - 1; p++)
                        //{
                        //    string x2 = dataGridView3.Rows[p].Cells["descripcion"].Value.ToString();
                        //    string y2 = dataGridView3.Rows[p].Cells["total"].Value.ToString();
                        //    VMensual.Series[0].Points.AddXY(x2, y2);
                        //}   
                    }
                    pgrafica(1, 4);
                    break;
                case 4:
                    Pdiario.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
                    if (dataGridView4.RowCount > 0)
                    {
                        int c = 0;
                        while (c < 10 & c < dataGridView4.RowCount - 1)
                        {
                            string x = dataGridView4.Rows[c].Cells["fecha"].Value.ToString();
                            string y = dataGridView4.Rows[c].Cells["ganancia"].Value.ToString();
                            Pdiario.Series[0].Points.AddXY(x, y);
                            c++;
                        }
                        //for (int p = 0; p < dataGridView4.RowCount - 1; p++)
                        //{
                        //    string x3 = dataGridView4.Rows[p].Cells["fecha"].Value.ToString();
                        //    string y3 = dataGridView4.Rows[p].Cells["ganancia"].Value.ToString();
                        //    Pdiario.Series[0].Points.AddXY(x3, y3);
                        //}   
                    }
                    pgrafica(7, 5);
                    break;
                case 5:
                    Psemanal.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Range;
                    if (dataGridView5.RowCount > 0)
                    {
                        int c = 0;
                        while (c < 10 & c < dataGridView5.RowCount - 1)
                        {
                            string x = dataGridView5.Rows[c].Cells["fecha"].Value.ToString();
                            string y = dataGridView5.Rows[c].Cells["ganancia"].Value.ToString();
                            Psemanal.Series[0].Points.AddXY(x, y);
                            c++;
                        }
                        //for (int p = 0; p < dataGridView5.RowCount - 1; p++)
                        //{
                        //    string x4 = dataGridView5.Rows[p].Cells["fecha"].Value.ToString();
                        //    string y4 = dataGridView5.Rows[p].Cells["ganancia"].Value.ToString();
                        //    Psemanal.Series[0].Points.AddXY(x4, y4);
                        //}   
                    }
                    pgrafica(30, 6);
                    break;
                case 6:
                    PMensual.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Range;
                    if (dataGridView6.RowCount > 0)
                    {
                        int c = 0;
                        while (c < 10 & c < dataGridView6.RowCount - 1)
                        {
                            string x = dataGridView6.Rows[c].Cells["fecha"].Value.ToString();
                            string y = dataGridView6.Rows[c].Cells["ganancia"].Value.ToString();
                            PMensual.Series[0].Points.AddXY(x, y);
                            c++;
                        }
                        //for (int p = 0; p < dataGridView6.RowCount - 1; p++)
                        //{
                        //    string x5 = dataGridView6.Rows[p].Cells["fecha"].Value.ToString();
                        //    string y5 = dataGridView6.Rows[p].Cells["ganancia"].Value.ToString();
                        //    PMensual.Series[0].Points.AddXY(x5, y5);
                        //}   
                    }
                    break;
            }
        }
    }
}
