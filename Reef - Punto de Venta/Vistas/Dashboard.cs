﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Reef___Punto_de_Venta;
using System.IO;


namespace Reef___Punto_de_Venta
{
    public partial class Inicio : Form
    {
        public bool Estado = true;
        int desplz_menu = 4;
        Vistas.Productos ProductosForm = new Vistas.Productos();
        Vistas.Vender VenderForm = new Vistas.Vender();
        Vistas.Ajustes AjustesForm = new Vistas.Ajustes();
        Vistas.Reportes ReportesForm = new Vistas.Reportes();
        Vistas.Inventario InventarioForm = new Vistas.Inventario();
        Vistas.Corte CorteForm = new Vistas.Corte();
        Vistas.Analisis AnalisisForm = new Vistas.Analisis();
        Vistas.Ofertas OfertasForm = new Vistas.Ofertas();
        
        public static controlDashboard vistaDashboard;

        public controlDashboard VistaDashboard
        {
            get
            {
                if (vistaDashboard == null)
                    vistaDashboard = new controlDashboard();
                return vistaDashboard;
            }
        }
        string userType, userName;

        public Inicio(string ut, string un,string ui)
        {
            InitializeComponent();
            panelWorkSpace.Controls.Add(VistaDashboard);//Al iniciar el form poondrá la vista del dashboard en el WorkSpace
            //MMG//Por fines practicos dejé el controlDashboard (para no tener que reintegrarlo)

            userType = ut;
            userName = un;
            IDuser = Convert.ToInt32(ui);
        }

        private void Inicio_Load(object sender, EventArgs e)
        {
            labelType.Text = userName;
            labelUser.Text = userType;
            Cargar_InfoTienda();
            UserImg.SizeMode = PictureBoxSizeMode.StretchImage;
            UserImg.Image = Properties.Resources.REEF_LOGO_ALV;    
        }

        #region Cargar Nombre y Direccion desde TXT
        public void Cargar_InfoTienda()
        {
            string txt = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            txt = txt.Remove(0, 6);//Remueve la parte "file:\"
            txt = txt.Remove(txt.Length - 9, 9);
            txt += "InfoTienda\\";

            FileStream stream = new FileStream(@txt + "direccion_tienda.txt", FileMode.Open, FileAccess.Read);
            StreamReader lee = new StreamReader(stream);
            string Linea = "";
            ArrayList arrText = new ArrayList();
            vistaDashboard.LabelDireccion.Text = "";
            do
            {
                Linea = lee.ReadLine();
                if (Linea != null)
                {
                    arrText.Add(Linea);
                    vistaDashboard.LabelDireccion.Text += Linea + "\r\n";
                }
            } while (Linea != null);
            lee.Close();

            stream = new FileStream(@txt + "nombre_tienda.txt", FileMode.Open, FileAccess.Read);
            lee = new StreamReader(stream);
            Linea = "";
            arrText = new ArrayList();
            vistaDashboard.LabelNombreTienda.Text = "";
            do
            {
                Linea = lee.ReadLine();
                if (Linea != null)
                {
                    arrText.Add(Linea);
                    vistaDashboard.LabelNombreTienda.Text += Linea + "\n";
                }
            } while (Linea != null);
            lee.Close();
        }//Carga la direccion de la Tienda y su Nombre
        #endregion      

        private void MenuVender_MouseEnter(object sender, EventArgs e)
        {
            MenuVender.BackColor = Color.FromArgb(49, 71, 90);            
            MenuSelector.Location = new Point(0, 0);
            LabelVender.Left += desplz_menu;
            pictureBox1.Left += desplz_menu;
        }

        private void MenuProductos_MouseEnter(object sender, EventArgs e)
        {
            MenuProductos.BackColor = Color.FromArgb(49,71,90);
            MenuSelector.Location = new Point(0,65);
            LabelProductos.Left += desplz_menu;
            pictureBox2.Left += desplz_menu;
        }
        
        private void MenuReportes_MouseEnter(object sender, EventArgs e)
        {
            MenuReportes.BackColor = Color.FromArgb(49, 71, 90);
            MenuSelector.Location = new Point(0, 130);
            LabelReportes.Left += desplz_menu;
            pictureBox3.Left += desplz_menu;
        }

        private void MenuInventario_MouseEnter(object sender, EventArgs e)
        {
            MenuInventario.BackColor = Color.FromArgb(49, 71, 90);
            MenuSelector.Location = new Point(0, 195);
            LabelInventario.Left += desplz_menu;
            pictureBox4.Left += desplz_menu;
        }

        private void MenuCorte_MouseEnter(object sender, EventArgs e)
        {
            MenuCorte.BackColor = Color.FromArgb(49, 71, 90);
            MenuSelector.Location = new Point(0, 260);
            LblCorte.Left += desplz_menu;
            pictureBox5.Left += desplz_menu;
        }

        private void MenuAnalisis_MouseEnter(object sender, EventArgs e)
        {
            MenuAnalisis.BackColor = Color.FromArgb(49, 71, 90);
            MenuSelector.Location = new Point(0, 325);
            LabelAnalisis.Left += desplz_menu;
            pictureBox6.Left += desplz_menu;
        }

        private void MenuOfertas_MouseEnter(object sender, EventArgs e)
        {
            MenuOfertas.BackColor = Color.FromArgb(49, 71, 90);
            MenuSelector.Location = new Point(0, 389);
            LabelOfertas.Left += desplz_menu;
            pictureBox7.Left += desplz_menu;
        }

        private void MenuAjustes_MouseEnter(object sender, EventArgs e)
        {
            MenuAjustes.BackColor = Color.FromArgb(49, 71, 90);
            MenuSelector.Location = new Point(0, 453);
            LabelAjustes.Left += desplz_menu;
            pictureBox8.Left += desplz_menu;
        }

        private void MenuVender_MouseLeave(object sender, EventArgs e)
        {
            MenuVender.BackColor = Color.FromArgb(49, 49, 52);
            LabelVender.Left -= desplz_menu;
            pictureBox1.Left -= desplz_menu;
        }

        private void MenuProductos_MouseLeave(object sender, EventArgs e)
        {
            MenuProductos.BackColor = Color.FromArgb(49, 49, 52);
            LabelProductos.Left -= desplz_menu;
            pictureBox2.Left -= desplz_menu;
        }

        private void MenuReportes_MouseLeave(object sender, EventArgs e)
        {
            MenuReportes.BackColor = Color.FromArgb(49, 49, 52);
            LabelReportes.Left -= desplz_menu;
            pictureBox3.Left -= desplz_menu;
        }

        private void MenuInventario_MouseLeave(object sender, EventArgs e)
        {
            MenuInventario.BackColor = Color.FromArgb(49, 49, 52);
            LabelInventario.Left -= desplz_menu;
            pictureBox4.Left -= desplz_menu;
        }

        private void MenuCorte_MouseLeave(object sender, EventArgs e)
        {
            MenuCorte.BackColor = Color.FromArgb(49, 49, 52);
            LblCorte.Left -= desplz_menu;
            pictureBox5.Left -= desplz_menu;
        }

        private void MenuAnalisis_MouseLeave(object sender, EventArgs e)
        {
            MenuAnalisis.BackColor = Color.FromArgb(49, 49, 52);
            LabelAnalisis.Left -= desplz_menu;
            pictureBox6.Left -= desplz_menu;
        }

        private void MenuOfertas_MouseLeave(object sender, EventArgs e)
        {
            MenuOfertas.BackColor = Color.FromArgb(49, 49, 52);
            LabelOfertas.Left -= desplz_menu;
            pictureBox7.Left -= desplz_menu;
        }

        private void MenuAjustes_MouseLeave(object sender, EventArgs e)
        {
            MenuAjustes.BackColor = Color.FromArgb(49, 49, 52);
            LabelAjustes.Left -= desplz_menu;
            pictureBox8.Left -= desplz_menu;
        }
                                   
        private void Buscar_MouseEnter(object sender, EventArgs e)
        {
            Buscar.BackColor = Color.LightGray;
        }

        private void Buscar_MouseLeave(object sender, EventArgs e)
        {
            Buscar.BackColor = Color.White;
        }

        private void LabelVender_Click(object sender, EventArgs e)
        {
            if (Estado)
            {
                if (VenderForm.IsDisposed == true)
                {
                    VenderForm = new Vistas.Vender();
                    VenderForm.Show();
                }
                else
                {
                    VenderForm.Hide();
                    VenderForm.Show();
                }
            }
            else
            {
                MessageBox.Show("Funcion no disponible despues de cerrar la caja");
            }
        }

        private void LabelProductos_Click(object sender, EventArgs e)
        {
                if (ProductosForm.IsDisposed == true)
                {
                    ProductosForm = new Vistas.Productos();
                    ProductosForm.Show();
                }
                else
                {
                    ProductosForm.Hide();
                    ProductosForm.Show();
                }     
        }

        private void LabelAjustes_Click(object sender, EventArgs e)
        {
                if (AjustesForm.IsDisposed == true)
                {
                    AjustesForm = new Vistas.Ajustes();
                    AjustesForm.Show();
                }
                else
                {
                    AjustesForm.Hide();
                    AjustesForm.Show();
                }
        }

        private void LabelReportes_Click(object sender, EventArgs e)
        {
                if (ReportesForm.IsDisposed == true)
                {
                    ReportesForm = new Vistas.Reportes();
                    ReportesForm.Show();
                }
                else
                {
                    ReportesForm.Hide();
                    ReportesForm.Show();
                }
        }

        private void LabelInventario_Click(object sender, EventArgs e)
        {
                if (InventarioForm.IsDisposed == true)
                {
                    InventarioForm = new Vistas.Inventario();
                    InventarioForm.Show();
                }
                else
                {
                    InventarioForm.Hide();
                    InventarioForm.Show();
                }
        }

        private void LblCorte_Click(object sender, EventArgs e)
        {
                if (CorteForm.IsDisposed == true)
                {
                    CorteForm = new Vistas.Corte();
                    CorteForm.Show();
                }
                else
                {
                    CorteForm.Hide();
                    CorteForm.Show();
                }
        }

        private void LabelAnalisis_Click(object sender, EventArgs e)
        {
                if (AnalisisForm.IsDisposed == true)
                {
                    AnalisisForm = new Vistas.Analisis();
                    AnalisisForm.Show();
                }
                else
                {
                    AnalisisForm.Hide();
                    AnalisisForm.Show();
                }
        }

        private void LabelOfertas_Click(object sender, EventArgs e)
        {
                if (OfertasForm.IsDisposed == true)
                {
                    OfertasForm = new Vistas.Ofertas();
                    OfertasForm.Show();
                }
                else
                {
                    OfertasForm.Hide();
                    OfertasForm.Show();
                }
        }

        private void LabelCorte_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void AbiertoCerrado_Click(object sender, EventArgs e)
        {
            if (Estado == false)
            {
                Estado = true;                
                MessageBox.Show("Debe reiniciar el sistema");                
            }
            else
            {
                if(MessageBox.Show("¿Esta seguro de cerrar la caja?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Estado = false;
                    AbiertoCerrado.ForeColor = Color.Coral;
                    AbiertoCerrado.Text = "Cerrado";
                    if (VenderForm.IsDisposed == false)
                    {
                        VenderForm.Minimizar.PerformClick();
                    }
                }
            }
        }

        private void Header_Paint(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics, this.Header.ClientRectangle, Color.FromArgb(165, 182, 195), ButtonBorderStyle.Solid);
        }        
    }   
}
