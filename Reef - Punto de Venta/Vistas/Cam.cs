﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge;
using AForge.Video;
using AForge.Video.DirectShow;

namespace Reef___Punto_de_Venta.Vistas
{
    public partial class Cam : Form
    {
        int id;
        Productos P;
        public Cam(int i,Productos p)
        {
            InitializeComponent();
            id = i;
            P = p;
        }

        private FilterInfoCollection CaptureDevice;
        private VideoCaptureDevice FinalFrame;

        private void Cam_Load(object sender, EventArgs e)
        {
            try
            {
                CaptureDevice = new FilterInfoCollection(FilterCategory.VideoInputDevice);
                foreach (FilterInfo Device in CaptureDevice)
                {
                    comboBox1.Items.Add(Device.Name);
                }
                comboBox1.SelectedIndex = 0;
                FinalFrame = new VideoCaptureDevice();
            }
            catch
            {
                MessageBox.Show("No tienes ninguna Web Cam compatible");
                this.Close();
            }
        }

        private void FinalFrame_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            pictureBox1.Image = (Bitmap)eventArgs.Frame.Clone();
        }       

        private void Cam_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (FinalFrame.IsRunning == true)
            {
                FinalFrame.Stop();
            }
        }
        
        private void Minimizar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            FinalFrame = new VideoCaptureDevice(CaptureDevice[comboBox1.SelectedIndex].MonikerString);
            FinalFrame.NewFrame += new NewFrameEventHandler(FinalFrame_NewFrame);
            FinalFrame.Start();
            button2.Visible = true;
            button3.Visible = true;
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            pictureBox2.Image = (Bitmap)pictureBox1.Image.Clone();
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            try
            {
                Helper.SaveImageCapture(pictureBox2.Image, id);
                this.Close();
                P.Cancelar.PerformClick();
                
            }
            catch
            {
                MessageBox.Show("Debes Tomar una foto");
            }
        }
    }
}
