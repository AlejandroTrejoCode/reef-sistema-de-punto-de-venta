﻿namespace Reef___Punto_de_Venta.Vistas
{
    partial class Ajustes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Ajustes));
            this.HeaderVender = new System.Windows.Forms.Panel();
            this.Minimizar = new System.Windows.Forms.Button();
            this.LabelResumen = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TextBoxNombreT = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TextBoxUbicacion = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.CambiarDatos = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ContenedorClave = new System.Windows.Forms.PictureBox();
            this.texbId = new System.Windows.Forms.TextBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.pass = new System.Windows.Forms.TextBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.EliminarUsuario = new System.Windows.Forms.Button();
            this.ModificarUsuario = new System.Windows.Forms.Button();
            this.AgregarUsuario = new System.Windows.Forms.Button();
            this.HeaderVender.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorClave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // HeaderVender
            // 
            this.HeaderVender.BackColor = System.Drawing.Color.White;
            this.HeaderVender.Controls.Add(this.Minimizar);
            this.HeaderVender.Controls.Add(this.LabelResumen);
            this.HeaderVender.Location = new System.Drawing.Point(0, 0);
            this.HeaderVender.Name = "HeaderVender";
            this.HeaderVender.Size = new System.Drawing.Size(1210, 50);
            this.HeaderVender.TabIndex = 2;
            this.HeaderVender.Paint += new System.Windows.Forms.PaintEventHandler(this.HeaderVender_Paint);
            // 
            // Minimizar
            // 
            this.Minimizar.BackgroundImage = global::Reef___Punto_de_Venta.Properties.Resources.Cerrar1;
            this.Minimizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Minimizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Minimizar.ForeColor = System.Drawing.Color.PowderBlue;
            this.Minimizar.Location = new System.Drawing.Point(1190, 0);
            this.Minimizar.Name = "Minimizar";
            this.Minimizar.Size = new System.Drawing.Size(20, 20);
            this.Minimizar.TabIndex = 2;
            this.Minimizar.UseVisualStyleBackColor = true;
            this.Minimizar.Click += new System.EventHandler(this.Minimizar_Click);
            // 
            // LabelResumen
            // 
            this.LabelResumen.AutoSize = true;
            this.LabelResumen.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelResumen.ForeColor = System.Drawing.Color.DimGray;
            this.LabelResumen.Location = new System.Drawing.Point(12, 6);
            this.LabelResumen.Name = "LabelResumen";
            this.LabelResumen.Size = new System.Drawing.Size(247, 38);
            this.LabelResumen.TabIndex = 1;
            this.LabelResumen.Text = "Inicio > Ajustes";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DimGray;
            this.label2.Location = new System.Drawing.Point(15, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(206, 25);
            this.label2.TabIndex = 18;
            this.label2.Text = "Nombre de la tienda:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TextBoxNombreT
            // 
            this.TextBoxNombreT.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.TextBoxNombreT.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextBoxNombreT.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxNombreT.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.TextBoxNombreT.HideSelection = false;
            this.TextBoxNombreT.Location = new System.Drawing.Point(15, 96);
            this.TextBoxNombreT.MaxLength = 100;
            this.TextBoxNombreT.Name = "TextBoxNombreT";
            this.TextBoxNombreT.Size = new System.Drawing.Size(537, 24);
            this.TextBoxNombreT.TabIndex = 20;
            this.TextBoxNombreT.TabStop = false;
            this.TextBoxNombreT.WordWrap = false;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DimGray;
            this.label1.Location = new System.Drawing.Point(15, 136);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(206, 25);
            this.label1.TabIndex = 21;
            this.label1.Text = "Ubicación";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TextBoxUbicacion
            // 
            this.TextBoxUbicacion.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.TextBoxUbicacion.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextBoxUbicacion.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxUbicacion.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.TextBoxUbicacion.HideSelection = false;
            this.TextBoxUbicacion.Location = new System.Drawing.Point(17, 168);
            this.TextBoxUbicacion.MaxLength = 100;
            this.TextBoxUbicacion.Multiline = true;
            this.TextBoxUbicacion.Name = "TextBoxUbicacion";
            this.TextBoxUbicacion.Size = new System.Drawing.Size(535, 50);
            this.TextBoxUbicacion.TabIndex = 23;
            this.TextBoxUbicacion.TabStop = false;
            this.TextBoxUbicacion.WordWrap = false;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.DimGray;
            this.label3.Location = new System.Drawing.Point(595, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(206, 38);
            this.label3.TabIndex = 24;
            this.label3.Text = "Usuarios";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(200)))), ((int)(((byte)(163)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(200)))), ((int)(((byte)(163)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Location = new System.Drawing.Point(600, 92);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(495, 255);
            this.dataGridView1.TabIndex = 27;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dataGridView1_KeyPress_1);
            // 
            // CambiarDatos
            // 
            this.CambiarDatos.BackColor = System.Drawing.Color.White;
            this.CambiarDatos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.CambiarDatos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CambiarDatos.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CambiarDatos.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(200)))), ((int)(((byte)(163)))));
            this.CambiarDatos.Location = new System.Drawing.Point(12, 242);
            this.CambiarDatos.Name = "CambiarDatos";
            this.CambiarDatos.Size = new System.Drawing.Size(161, 66);
            this.CambiarDatos.TabIndex = 26;
            this.CambiarDatos.Text = "Cambiar Datos";
            this.CambiarDatos.UseVisualStyleBackColor = false;
            this.CambiarDatos.Click += new System.EventHandler(this.CambiarDatos_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Reef___Punto_de_Venta.Properties.Resources.BuscarContenedor;
            this.pictureBox1.Location = new System.Drawing.Point(12, 164);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(550, 62);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            // 
            // ContenedorClave
            // 
            this.ContenedorClave.Image = global::Reef___Punto_de_Venta.Properties.Resources.BuscarContenedor;
            this.ContenedorClave.Location = new System.Drawing.Point(12, 92);
            this.ContenedorClave.Name = "ContenedorClave";
            this.ContenedorClave.Size = new System.Drawing.Size(550, 32);
            this.ContenedorClave.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ContenedorClave.TabIndex = 19;
            this.ContenedorClave.TabStop = false;
            // 
            // texbId
            // 
            this.texbId.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.texbId.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.texbId.Enabled = false;
            this.texbId.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.texbId.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.texbId.HideSelection = false;
            this.texbId.Location = new System.Drawing.Point(605, 382);
            this.texbId.MaxLength = 100;
            this.texbId.Name = "texbId";
            this.texbId.Size = new System.Drawing.Size(116, 24);
            this.texbId.TabIndex = 66;
            this.texbId.TabStop = false;
            this.texbId.WordWrap = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::Reef___Punto_de_Venta.Properties.Resources.BuscarContenedor;
            this.pictureBox6.Location = new System.Drawing.Point(602, 378);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(129, 32);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 65;
            this.pictureBox6.TabStop = false;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.DimGray;
            this.label8.Location = new System.Drawing.Point(601, 354);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 25);
            this.label8.TabIndex = 64;
            this.label8.Text = "id";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBox1
            // 
            this.comboBox1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.comboBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.comboBox1.Font = new System.Drawing.Font("Century Gothic", 14.25F);
            this.comboBox1.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Admin",
            "Gerente",
            "Cajero"});
            this.comboBox1.Location = new System.Drawing.Point(600, 515);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(235, 30);
            this.comboBox1.TabIndex = 60;
            this.comboBox1.Text = "Admin";
            // 
            // pass
            // 
            this.pass.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pass.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.pass.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pass.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.pass.HideSelection = false;
            this.pass.Location = new System.Drawing.Point(605, 587);
            this.pass.MaxLength = 100;
            this.pass.Name = "pass";
            this.pass.Size = new System.Drawing.Size(220, 24);
            this.pass.TabIndex = 59;
            this.pass.TabStop = false;
            this.pass.WordWrap = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Reef___Punto_de_Venta.Properties.Resources.BuscarContenedor;
            this.pictureBox4.Location = new System.Drawing.Point(602, 583);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(233, 32);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 58;
            this.pictureBox4.TabStop = false;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.textBox1.HideSelection = false;
            this.textBox1.Location = new System.Drawing.Point(605, 443);
            this.textBox1.MaxLength = 100;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(220, 24);
            this.textBox1.TabIndex = 57;
            this.textBox1.TabStop = false;
            this.textBox1.WordWrap = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Reef___Punto_de_Venta.Properties.Resources.BuscarContenedor;
            this.pictureBox2.Location = new System.Drawing.Point(602, 439);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(233, 32);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 56;
            this.pictureBox2.TabStop = false;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.DimGray;
            this.label6.Location = new System.Drawing.Point(601, 559);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(206, 25);
            this.label6.TabIndex = 55;
            this.label6.Text = "Contraseña";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.DimGray;
            this.label5.Location = new System.Drawing.Point(601, 487);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(206, 25);
            this.label5.TabIndex = 54;
            this.label5.Text = "Nivel de usuario";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.DimGray;
            this.label4.Location = new System.Drawing.Point(596, 415);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(206, 25);
            this.label4.TabIndex = 53;
            this.label4.Text = "Nombre de usuario";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // EliminarUsuario
            // 
            this.EliminarUsuario.BackColor = System.Drawing.Color.White;
            this.EliminarUsuario.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.EliminarUsuario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.EliminarUsuario.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EliminarUsuario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(200)))), ((int)(((byte)(163)))));
            this.EliminarUsuario.Location = new System.Drawing.Point(939, 621);
            this.EliminarUsuario.Name = "EliminarUsuario";
            this.EliminarUsuario.Size = new System.Drawing.Size(161, 66);
            this.EliminarUsuario.TabIndex = 52;
            this.EliminarUsuario.Text = "Eliminar Usuario";
            this.EliminarUsuario.UseVisualStyleBackColor = false;
            this.EliminarUsuario.Click += new System.EventHandler(this.EliminarUsuario_Click_1);
            // 
            // ModificarUsuario
            // 
            this.ModificarUsuario.BackColor = System.Drawing.Color.White;
            this.ModificarUsuario.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.ModificarUsuario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ModificarUsuario.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ModificarUsuario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(200)))), ((int)(((byte)(163)))));
            this.ModificarUsuario.Location = new System.Drawing.Point(772, 621);
            this.ModificarUsuario.Name = "ModificarUsuario";
            this.ModificarUsuario.Size = new System.Drawing.Size(161, 66);
            this.ModificarUsuario.TabIndex = 51;
            this.ModificarUsuario.Text = "Modificar Usuario";
            this.ModificarUsuario.UseVisualStyleBackColor = false;
            this.ModificarUsuario.Click += new System.EventHandler(this.ModificarUsuario_Click_1);
            // 
            // AgregarUsuario
            // 
            this.AgregarUsuario.BackColor = System.Drawing.Color.White;
            this.AgregarUsuario.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.AgregarUsuario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AgregarUsuario.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AgregarUsuario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(200)))), ((int)(((byte)(163)))));
            this.AgregarUsuario.Location = new System.Drawing.Point(605, 621);
            this.AgregarUsuario.Name = "AgregarUsuario";
            this.AgregarUsuario.Size = new System.Drawing.Size(161, 66);
            this.AgregarUsuario.TabIndex = 50;
            this.AgregarUsuario.Text = "Agregar Usuario";
            this.AgregarUsuario.UseVisualStyleBackColor = false;
            this.AgregarUsuario.Click += new System.EventHandler(this.AgregarUsuario_Click);
            // 
            // Ajustes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1210, 700);
            this.Controls.Add(this.texbId);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.pass);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.EliminarUsuario);
            this.Controls.Add(this.ModificarUsuario);
            this.Controls.Add(this.AgregarUsuario);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.CambiarDatos);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TextBoxUbicacion);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TextBoxNombreT);
            this.Controls.Add(this.ContenedorClave);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.HeaderVender);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(156, 60);
            this.Name = "Ajustes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Ajustes";
            this.Load += new System.EventHandler(this.Ajustes_Load);
            this.HeaderVender.ResumeLayout(false);
            this.HeaderVender.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorClave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel HeaderVender;
        private System.Windows.Forms.Button Minimizar;
        private System.Windows.Forms.Label LabelResumen;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox ContenedorClave;
        private System.Windows.Forms.TextBox TextBoxNombreT;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox TextBoxUbicacion;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button CambiarDatos;
        private System.Windows.Forms.TextBox texbId;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox pass;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button EliminarUsuario;
        private System.Windows.Forms.Button ModificarUsuario;
        private System.Windows.Forms.Button AgregarUsuario;
    }
}