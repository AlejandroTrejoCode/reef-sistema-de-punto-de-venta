﻿namespace Reef___Punto_de_Venta.Vistas
{
    partial class Reportes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Reportes));
            this.HeaderVender = new System.Windows.Forms.Panel();
            this.Minimizar = new System.Windows.Forms.Button();
            this.LabelResumen = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxSeleccion = new System.Windows.Forms.ComboBox();
            this.BotonGenerar = new System.Windows.Forms.Button();
            this.ContenedorBotonAbierto = new System.Windows.Forms.PictureBox();
            this.crystalReportViewer1 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.VentaMensual1 = new Reef___Punto_de_Venta.VentaMensual();
            this.VentaDiaria1 = new Reef___Punto_de_Venta.VentaDiaria();
            this.HeaderVender.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorBotonAbierto)).BeginInit();
            this.SuspendLayout();
            // 
            // HeaderVender
            // 
            this.HeaderVender.BackColor = System.Drawing.Color.White;
            this.HeaderVender.Controls.Add(this.Minimizar);
            this.HeaderVender.Controls.Add(this.LabelResumen);
            this.HeaderVender.Location = new System.Drawing.Point(0, 0);
            this.HeaderVender.Name = "HeaderVender";
            this.HeaderVender.Size = new System.Drawing.Size(1210, 50);
            this.HeaderVender.TabIndex = 3;
            this.HeaderVender.Paint += new System.Windows.Forms.PaintEventHandler(this.HeaderVender_Paint);
            // 
            // Minimizar
            // 
            this.Minimizar.BackgroundImage = global::Reef___Punto_de_Venta.Properties.Resources.Cerrar1;
            this.Minimizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Minimizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Minimizar.ForeColor = System.Drawing.Color.PowderBlue;
            this.Minimizar.Location = new System.Drawing.Point(1190, 0);
            this.Minimizar.Name = "Minimizar";
            this.Minimizar.Size = new System.Drawing.Size(20, 20);
            this.Minimizar.TabIndex = 2;
            this.Minimizar.UseVisualStyleBackColor = true;
            this.Minimizar.Click += new System.EventHandler(this.Minimizar_Click);
            // 
            // LabelResumen
            // 
            this.LabelResumen.AutoSize = true;
            this.LabelResumen.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelResumen.ForeColor = System.Drawing.Color.DimGray;
            this.LabelResumen.Location = new System.Drawing.Point(12, 6);
            this.LabelResumen.Name = "LabelResumen";
            this.LabelResumen.Size = new System.Drawing.Size(272, 38);
            this.LabelResumen.TabIndex = 1;
            this.LabelResumen.Text = "Inicio > Reportes";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.DimGray;
            this.label3.Location = new System.Drawing.Point(14, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(409, 38);
            this.label3.TabIndex = 25;
            this.label3.Text = "Seleccione su reporte a generar:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBoxSeleccion
            // 
            this.comboBoxSeleccion.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxSeleccion.ForeColor = System.Drawing.Color.DimGray;
            this.comboBoxSeleccion.FormattingEnabled = true;
            this.comboBoxSeleccion.Items.AddRange(new object[] {
            "Ventas - Diario",
            "Ventas - Mensual"});
            this.comboBoxSeleccion.Location = new System.Drawing.Point(429, 59);
            this.comboBoxSeleccion.Name = "comboBoxSeleccion";
            this.comboBoxSeleccion.Size = new System.Drawing.Size(265, 32);
            this.comboBoxSeleccion.TabIndex = 26;
            // 
            // BotonGenerar
            // 
            this.BotonGenerar.BackColor = System.Drawing.Color.White;
            this.BotonGenerar.FlatAppearance.BorderSize = 0;
            this.BotonGenerar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.BotonGenerar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BotonGenerar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotonGenerar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(200)))), ((int)(((byte)(163)))));
            this.BotonGenerar.Location = new System.Drawing.Point(741, 60);
            this.BotonGenerar.Name = "BotonGenerar";
            this.BotonGenerar.Size = new System.Drawing.Size(144, 30);
            this.BotonGenerar.TabIndex = 27;
            this.BotonGenerar.Text = "Generar Reporte";
            this.BotonGenerar.UseVisualStyleBackColor = false;
            this.BotonGenerar.Click += new System.EventHandler(this.BotonGenerar_Click);
            // 
            // ContenedorBotonAbierto
            // 
            this.ContenedorBotonAbierto.Image = global::Reef___Punto_de_Venta.Properties.Resources.ButtonAbierto_fw;
            this.ContenedorBotonAbierto.Location = new System.Drawing.Point(733, 57);
            this.ContenedorBotonAbierto.Name = "ContenedorBotonAbierto";
            this.ContenedorBotonAbierto.Size = new System.Drawing.Size(158, 36);
            this.ContenedorBotonAbierto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ContenedorBotonAbierto.TabIndex = 28;
            this.ContenedorBotonAbierto.TabStop = false;
            // 
            // crystalReportViewer1
            // 
            this.crystalReportViewer1.ActiveViewIndex = -1;
            this.crystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalReportViewer1.Cursor = System.Windows.Forms.Cursors.Default;
            this.crystalReportViewer1.Location = new System.Drawing.Point(12, 103);
            this.crystalReportViewer1.Name = "crystalReportViewer1";
            this.crystalReportViewer1.Size = new System.Drawing.Size(1186, 585);
            this.crystalReportViewer1.TabIndex = 29;
            // 
            // Reportes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1210, 700);
            this.Controls.Add(this.crystalReportViewer1);
            this.Controls.Add(this.BotonGenerar);
            this.Controls.Add(this.ContenedorBotonAbierto);
            this.Controls.Add(this.comboBoxSeleccion);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.HeaderVender);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(156, 60);
            this.Name = "Reportes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Reportes";
            this.Load += new System.EventHandler(this.Reportes_Load);
            this.HeaderVender.ResumeLayout(false);
            this.HeaderVender.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorBotonAbierto)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel HeaderVender;
        private System.Windows.Forms.Button Minimizar;
        private System.Windows.Forms.Label LabelResumen;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxSeleccion;
        private System.Windows.Forms.Button BotonGenerar;
        private System.Windows.Forms.PictureBox ContenedorBotonAbierto;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportViewer1;
        private VentaDiaria VentaDiaria1;
        private VentaMensual VentaMensual1;
    }
}