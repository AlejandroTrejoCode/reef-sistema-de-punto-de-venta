﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Reef___Punto_de_Venta.Vistas
{
    class Helper
    {

        public static void SaveImageCapture(Image image, int i)
        {
            GuardarImagen(image, i);
        }
        private static void GuardarImagen(Image imagen, int id)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                imagen.Save(ms, ImageFormat.Gif);
                byte[] imgArr = ms.ToArray();
                MyConnection m = new MyConnection();
                m.Crear_conexion();
                using (MySqlConnection conn = m.getConexion())
                {
                    using (MySqlCommand cmd = new MySqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandText = "UPDATE productos set img = (@imgArr) where id_producto = @id";
                        //cmd.Parameters.AddWithValue("@Nombre", Nombre);
                        cmd.Parameters.AddWithValue("@imgArr", imgArr);
                        cmd.Parameters.AddWithValue("@id", id);
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("insersion exitosa");
                    }
                }
            }
        }

        public static Image CargarImagen(string id)
        {
            MyConnection m = new MyConnection();
            m.Crear_conexion();
            using (MySqlConnection conn = m.getConexion())
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    Image i = Image.FromFile(@"C:\Users\alex\Pictures\tachita.png");
                    try
                    {
                        cmd.Connection = conn;
                        cmd.CommandText = "SELECT img FROM productos WHERE descripcion = @id";
                        cmd.Parameters.AddWithValue("@id", id);
                        byte[] imgArr = (byte[])cmd.ExecuteScalar();

                        imgArr = (byte[])cmd.ExecuteScalar();
                        if (imgArr.Length > 0)
                        {
                            using (var stream = new MemoryStream(imgArr))
                            {
                                Image img = Image.FromStream(stream);
                                return img;
                            }
                        }
                        return i;
                    }
                    catch
                    {
                        return i;
                    }
                }
            }
        }
    }
}
