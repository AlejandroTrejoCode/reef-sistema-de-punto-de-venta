﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reef___Punto_de_Venta.Vistas
{
    public partial class Corte : Form
    {
        public Corte()
        {
            InitializeComponent();
        }

        private void Minimiza_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void HeaderVender_Paint(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics, this.HeaderVender.ClientRectangle, Color.FromArgb(165, 182, 195), ButtonBorderStyle.Solid);
        }

        private void Corte_Load(object sender, EventArgs e)
        {            
            string Fecha = DateTime.Now.ToString("dd, MM, yy").ToUpper();
            LabelDia.Text = Fecha;
            string i = DateTime.Now.ToString("yyMMdd");
            //string i = "160810";
            string[] cam = { "productos.id_producto", "productos.descripcion", "detalle_venta.importe", "ventas.fecha_ven" };
            string[] ta = { "productos", "ventas", "detalle_venta" };
            string condi = "and detalle_venta.id_producto=productos.id_producto";
            StringBuilder se = new StringBuilder();
            Sentencias.grafica(i, i, dataGridView1, "ventas", cam, "cantidad", "cantidad", ta, condi, "productos.id_producto");
            for (int j = 0; j < dataGridView1.RowCount - 1; j++)
            {                
                string n = dataGridView1.Rows[j].Cells[1].Value.ToString();
                se.Append(n.Substring(0, 6) + "                                                                               " + dataGridView1.Rows[j].Cells[4].Value.ToString());                
                listBox1.Items.Add(se.ToString());
                se.Clear();
            }
            //select detalle_venta.id_venta, sum(productos.precio_venta-productos.precio_compra) as ganancia, sum(importe) as total from ventas, productos, detalle_venta where (ventas.fecha_ven between 160810 and 160810) and detalle_venta.id_producto=productos.id_producto;
            string[] c = { "sum(d2.total) as Total", "Sum((d3.precio_venta - d3.precio_compra) * d1.cantidad) as Ganancias" };            
            string t = "detalle_venta as d1, ventas as d2, productos as d3 where d1.id_venta=d2.id_venta&&d1.id_producto=d3.id_producto&& month(fecha_ven)=month(current_date())&&date(d2.fecha_ven)=curdate() group by d2.fecha_ven";
            //Sentencias.sentenciax(i, i, "ventas", c, ta, condicion, dataGridView2);
            Sentencias.llenar(dataGridView2, t, c);
            LabelGanacias.Text = dataGridView2.Rows[0].Cells["Total"].Value.ToString();
            LabelGanancias.Text = dataGridView2.Rows[0].Cells["Ganancias"].Value.ToString();
        }

        private void LabelDia_Click(object sender, EventArgs e)
        {

        }

        private void PanelCorte_Paint(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics, this.PanelCorte.ClientRectangle, Color.FromArgb(165, 182, 195), ButtonBorderStyle.Solid);
        }
    }
}
