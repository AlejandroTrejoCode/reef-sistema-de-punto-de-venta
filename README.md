# *-- REEF PUNTO DE VENTA --* #
Creado para la asignatura de Programación Orientada a Eventos.
Se encuentra en la versión más estable, puedes hacer uso de ella.
No olvides agradecer.

## *-- TEAM --* ##
Sergio Alejandro Trejo Cuxim -Lider de proyecto, UX, UI.
alejandrotrejocode@gmail.com

Pedro Pablo Romero Martinez - Programador estrella, modelador de base de datos.
KabutoYamato

Mateo Morales Garcia - Tester, UX.
MMGUP

Luis Ángel Novelo Caamal - Programador.
luisanc@gmail.com


Alexis Calderón Muñoz - Programador.
Alexfate